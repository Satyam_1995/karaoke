//
//  SelectMusicCell.swift
//  Karaoke
//
//  Created by Ankur  on 05/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SelectMusicCell: UICollectionViewCell {
    
    @IBOutlet weak var img_musicType:UIImageView!
    @IBOutlet weak var img_select:UIImageView!
    @IBOutlet weak var lbl_type:UILabel!
    @IBOutlet weak var back_view:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
