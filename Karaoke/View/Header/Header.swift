//
//  Header.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class Header: UICollectionReusableView {

    static let identifier = "Header"
    private let label: UILabel = {
       let lbl = UILabel()
//        lbl.text = "Tell us what you like to sing!"
//        lbl.textAlignment = .center
//        lbl.textColor = .white
        return lbl
    }()
    
    public func Configure(str:String,txtColor:UIColor,align:NSTextAlignment){
        backgroundColor = .clear
        label.text = str
        label.textColor = txtColor
        label.textAlignment = align
        addSubview(label)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        label.frame = bounds
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
