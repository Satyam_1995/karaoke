//
//  TextCommectCell.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 04/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit

class TextCommectCell: UITableViewCell {

    @IBOutlet weak var txtCommect : UITextField!
    @IBOutlet weak var btnSend : UIButton!
    typealias btnPress = (String) -> Void
    var delegate : btnPress?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func btn(_ sender:UIButton) {
        if txtCommect.text != "" {
            delegate?(txtCommect.text!)
            txtCommect.text = ""
        }
    }
}
