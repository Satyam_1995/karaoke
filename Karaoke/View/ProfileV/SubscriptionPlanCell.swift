//
//  SubscriptionPlanCell.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SubscriptionPlanCell: UITableViewCell {
    
    @IBOutlet weak var imgBack:UIImageView!
    @IBOutlet weak var lblPlanType:UILabel!
    @IBOutlet weak var lblPlanName:UILabel!
    @IBOutlet weak var lblPlanPrice:UILabel!
    @IBOutlet weak var lblPlanDetail:UILabel!
    @IBOutlet weak var btnBuy:UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
