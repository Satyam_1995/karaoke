//
//  BlockListCell.swift
//  Karaoke
//
//  Created by Ankur  on 19/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class BlockListCell: UITableViewCell {
    
    @IBOutlet weak var imgUSer:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var lbltotalFollowers:UILabel!
    @IBOutlet weak var btnUnblock:UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
