//
//  VideoCell.swift
//  Karaoke
//
//  Created by Ankur  on 17/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class VideoCell: UICollectionViewCell {

    @IBOutlet weak var imgSong:UIImageView!
    @IBOutlet weak var btnComment:UIButton!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var lblLike:UILabel!
    @IBOutlet weak var lblComment:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
