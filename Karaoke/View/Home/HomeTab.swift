//
//  HomeTab.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class HomeTab: UIView {
    
    @IBOutlet weak var containerView:UIView!
    @IBOutlet weak var btnComment:UIButton!
    @IBOutlet weak var btnLike:UIButton!
    @IBOutlet weak var btnSing:UIButton!
    @IBOutlet weak var btnFavorite:UIButton!
    @IBOutlet weak var lblComment:UILabel!
    @IBOutlet weak var lblLike:UILabel!
    @IBOutlet weak var imgLike:UIImageView!
    @IBOutlet weak var imgFavorite:UIImageView!
    @IBOutlet weak var lblSing:UILabel!
    @IBOutlet weak var lblFavorite:UILabel!
    @IBOutlet weak var favoriteView:UIView!
    
    typealias selectAction = (HomeTabAction,UIButton) -> Void
    var tabAction : selectAction?
    
    var updateLike = "0" {
        didSet {
            lblLike.text = updateLike.formatPoints()
        }
    }
    var updateComment = "0" {
        didSet {
            lblComment.text = updateComment.formatPoints()
        }
    }
    func AddLike() {
        guard var newl = Int(updateLike) else {
            return
        }
        newl = newl + 1
        updateLike = "\(newl)"
        lblLike.text = updateLike.formatPoints()
    }
    func removeLike() {
        guard var newl = Int(updateLike) else {
            return
        }
        newl = newl - 1
        updateLike = "\(newl)"
        lblLike.text = updateLike.formatPoints()
    }
    static let identifier = "HomeTab"
    override init(frame: CGRect) {
        super.init(frame: frame)
        commitInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commitInit()
    }
    private func commitInit(){
        Bundle.main.loadNibNamed("HomeTab", owner: self, options: nil)
        addSubview(containerView)
        containerView.frame = self.bounds
        containerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    @IBAction func btnFavorite(_ sender:UIButton) {
        self.tabAction?(.homeTab_favouriteBtnAction,sender)
    }
    
    @IBAction func btnComment(_ sender:UIButton) {
        self.tabAction?(.homeTab_CommentBtnAction,sender)
    }
    
    @IBAction func btnSingIt(_ sender:UIButton) {
        self.tabAction?(.homeTab_singItBtnAction,sender)
    }
    
    @IBAction func btnLike(_ sender:UIButton) {
        self.tabAction?(.homeTab_likeBtnAction,sender)
    }
}

extension UIViewController {
    func formatPoints(num: Double) ->String{
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))k")
            }
            return("\(thousandNum.roundToPlaces(places: 1))k")
        }
        if num > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))k")
            }
            return ("\(millionNum.roundToPlaces(places: 1))M")
        }
        else{
            if(floor(num) == num){
                return ("\(Int(num))")
            }
            return ("\(num)")
        }
    }
}
extension String {
    func formatPoints() ->String{
        guard let num = Double(self), self != "" else {
            return "0"
        }
        let thousandNum = num/1000
        let millionNum = num/1000000
        if num >= 1000 && num < 1000000{
            if(floor(thousandNum) == thousandNum){
                return("\(Int(thousandNum))k")
            }
            return("\(thousandNum.roundToPlaces(places: 1))k")
        }
        if num > 1000000{
            if(floor(millionNum) == millionNum){
                return("\(Int(thousandNum))k")
            }
            return ("\(millionNum.roundToPlaces(places: 1))M")
        }
        else{
            if(floor(num) == num){
                return ("\(Int(num))")
            }
            return ("\(num)")
        }
    }
}
extension Double {
    func roundToPlaces(places:Int) -> Double {
        
        let multiplier = pow(10, Double(places))
        let newDecimal = multiplier * self // move the decimal right
        let truncated = Double(Int(newDecimal)) // drop the fraction
        let originalDecimal = truncated / multiplier // move the decimal back
        return originalDecimal
    }
}

enum HomeTabAction {
    case homeTab_likeBtnAction
    case homeTab_CommentBtnAction
    case homeTab_singItBtnAction
    case homeTab_favouriteBtnAction
}
