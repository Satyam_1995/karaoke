//
//  HomeDataCell.swift
//  Karaoke
//
//  Created by Ankur  on 24/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class HomeDataCell: UICollectionViewCell {
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var thumbnil:UIImageView!
    @IBOutlet weak var btn_follow:UIButton!
    @IBOutlet weak var btn_Menu:UIButton!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var bottomView:HomeTab!
    @IBOutlet weak var playVideo:VideoView!
    
    @IBOutlet weak var pauseImgView: UIImageView!{
        didSet{
            pauseImgView.alpha = 0
        }
    }
    var playerView: VideoPlayerView!
    private(set) var isPlaying = false
    private(set) var liked = false
    
    var cellData : Video_list? {
        didSet {
            updateDat()
        }
    }
    // MARK: LIfecycles
    override func prepareForReuse() {
        super.prepareForReuse()
        playerView.cancelAllLoadingRequest()
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let s = UIViewController().topbarHeight
        bottomCons.constant = s + s + 43
        self.btn_follow.setTitle("Follow", for: .normal)
        self.btn_follow.setTitle("Following", for: .selected)
        self.btn_follow.setTitleColor(.white, for: .selected)
        
        playerView = VideoPlayerView(frame: self.contentView.frame)
        
        playVideo.addSubview(playerView)
        
//        contentView.addSubview(playerView)
//        contentView.sendSubviewToBack(playerView)
        
//        let pauseGesture = UITapGestureRecognizer(target: self, action: #selector(handlePause))
//        self.contentView.addGestureRecognizer(pauseGesture)
//
        let likeDoubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleLikeGesture(sender:)))
        likeDoubleTapGesture.numberOfTapsRequired = 2
        self.contentView.addGestureRecognizer(likeDoubleTapGesture)
//        pauseGesture.require(toFail: likeDoubleTapGesture)
    }
    func configure(post: Video_list){
        self.cellData = post
        let ext = post.video_file?.fileExtension() ?? "mp4"
        playerView.configure(url: post.video_file!.convert_to_url!, fileExtension: ext, size: (Int(self.frame.size.width), Int(self.frame.size.height)))
    }
    
    @objc func handlePause(){
        if isPlaying {
            // Pause video and show pause sign
            UIView.animate(withDuration: 0.075, delay: 0, options: .curveEaseIn, animations: { [weak self] in
                guard let self = self else { return }
                self.pauseImgView.alpha = 0.35
                self.pauseImgView.transform = CGAffineTransform.init(scaleX: 0.45, y: 0.45)
            }, completion: { [weak self] _ in
                self?.pause()
            })
        } else {
            // Start video and remove pause sign
            UIView.animate(withDuration: 0.075, delay: 0, options: .curveEaseInOut, animations: { [weak self] in
                guard let self = self else { return }
                self.pauseImgView.alpha = 0
            }, completion: { [weak self] _ in
                self?.play()
                self?.pauseImgView.transform = .identity
            })
        }
    }
    
    func replay(function : String = #function, // debugging purposes
                line : Int = #line,
                file : String = #file){
        if !isPlaying {
//            print(function, line, file)
            playerView.replay()
            play()
        }
    }
    
    func play(function : String = #function, // debugging purposes
              line : Int = #line,
              file : String = #file) {
        if !isPlaying {
//            print(function, line, file)
            playerView.play()
            isPlaying = true
        }
    }
    
    func pause(function : String = #function, // debugging purposes
               line : Int = #line,
               file : String = #file){
        
        if isPlaying {
//            print(function, line, file)
            playerView.pause()
            isPlaying = false
        }
    }
    
    // MARK: - Actions
    // Like Video Actions
    @IBAction func like(_ sender: Any) {
        if !liked {
            likeVideo()
        } else {
            liked = false
            bottomView.imgLike.tintColor = .white
        }
        
    }
    @objc func likeVideo(){
        if !liked {
            liked = true
            bottomView.imgLike.tintColor = .red
        }
    }
    
    // Heart Animation with random angle
    @objc func handleLikeGesture(sender: UITapGestureRecognizer){
        let location = sender.location(in: self)
//        let heartView = UIImageView(image: UIImage(systemName: "heart.fill"))
        let heartView = UIImageView(image: #imageLiteral(resourceName: "heartR"))
        heartView.tintColor = .red
        let width : CGFloat = 110
        heartView.contentMode = .scaleAspectFit
        heartView.frame = CGRect(x: location.x - width / 2, y: location.y - width / 2, width: width, height: width)
        heartView.transform = CGAffineTransform(rotationAngle: CGFloat.random(in: -CGFloat.pi * 0.2...CGFloat.pi * 0.2))
        self.contentView.addSubview(heartView)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 3, options: [.curveEaseInOut], animations: {
            heartView.transform = heartView.transform.scaledBy(x: 0.85, y: 0.85)
        }, completion: { _ in
            UIView.animate(withDuration: 0.4, delay: 0.1, usingSpringWithDamping: 0.8, initialSpringVelocity: 3, options: [.curveEaseInOut], animations: {
                heartView.transform = heartView.transform.scaledBy(x: 2.3, y: 2.3)
                heartView.alpha = 0
            }, completion: { _ in
                heartView.removeFromSuperview()
            })
        })
        likeVideo()
    }
    
    
    
    
    
    func updateDat()  {
        guard let dict = cellData else {
            return
        }
        if dict.user_like_status != 1 {
            bottomView.imgLike.image = #imageLiteral(resourceName: "heartW")
        }else{
            bottomView.imgLike.image = #imageLiteral(resourceName: "heartR")
        }
        if dict.user_fav_status != 1 {
            bottomView.imgFavorite.image = #imageLiteral(resourceName: "fav")
        }else{
            bottomView.imgFavorite.image = #imageLiteral(resourceName: "favR")
        }
        self.lblUserName.text = dict.user_name ?? ""
        if let url = dict.user_profile?.convert_to_url {
            self.imgUser.af.setImage(withURL: url)
        }
        if let url = dict.video_thumb?.convert_to_url {
            self.thumbnil.af.setImage(withURL: url)
        }
        self.bottomView.updateLike = dict.total_likes ?? ""
        self.bottomView.updateComment = dict.total_comments ?? ""
        if dict.user_follow_status != 0 {
            self.btn_follow.isHidden = true
        }else{
            self.btn_follow.isHidden = false
        }
    }
}
