//
//  SongBookPageVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 28/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class SongBookPageVM {
    
    weak var vc : SongBookPageVC?
//    var search_result : [Search_result] = []
//    var search_result1 : [Search_result] = []
    var search_result : [Popular_videos] = []
    var search_result1 : [Popular_videos] = []
    

    func search_song(txt:String,page:Int,min_range:Int, max_range:Int)  {
//        Para:user_id, txt
//        URL - http://karaoke.yilstaging.com/api/search_song
        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.txt] = txt
        parameters[ApiKey.min_range] = "\(min_range)"
        parameters[ApiKey.max_range] = "\(max_range)"
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .search_song, loader: true) { (json) in
            print(json)
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(SongBookModel.self, from: data)
//                self.top_artist_videos = d.video_list ?? []
                if min_range == 0 {
                    self.search_result.removeAll()
                }
                self.search_result.append(contentsOf: d.search_result ?? [])
                
                if self.search_result1.count == self.search_result.count {
                    self.vc?.shouldShowLoadingCell = false
                }else{
                    self.vc?.shouldShowLoadingCell = self.search_result.count%5 == 0
                    self.search_result1 = self.search_result
                }
                
                self.vc?.updatedata(new: true)
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
