//
//  ExploreVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 15/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class ExploreVM {
    
    weak var vc : ExploreVC?
    var explore_screenData : Explore_screenVM?
    var top_artist_videos : [Popular_videos] = []
    var recomended_videos : [Popular_videos] = []
    var popular_videos : [Popular_videos] = []
    var top_singer_videos : [Popular_videos] = []
    var trending_videos : [Popular_videos] = []

    func explore_screen()  {
//        Para:user_id
//        URL - http://karaoke.yilstaging.com/api/explore_screen
//        let loader = ProfileResponse == nil ? true : false
        let user_id = UserDetail.shared.getUserId()
        
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .explore_screen, loader: true) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(Explore_screenVM.self, from: data)
                
                self.top_artist_videos = d.top_artist_videos ?? []
                self.recomended_videos = d.recomended_videos ?? []
                self.popular_videos = d.popular_videos ?? []
                self.top_singer_videos = d.top_singer_videos ?? []
                self.trending_videos = d.trending_videos ?? []
                self.vc?.updatedata(new: true)
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
//    func video_views(video_id:String,complition:@escaping(Bool)->Void)  {
////        Para:user_id,video_id
////        URL -  http://karaoke.yilstaging.com/api/video_views
////        let loader = ProfileResponse == nil ? true : false
//        let user_id = UserDetail.shared.getUserId()
//
//        var parameters : [String:Any] = [:]
//        parameters[ApiKey.user_id] = user_id
//        parameters[ApiKey.video_id] = video_id
//
//        WebServices.commonPostAPI(parameters: parameters, endPoint: .video_views, loader: false) { (json) in
//            complition(true)
//        } successData: { (data) in
//        } failure: { (err) -> (Void) in
//            print(err.localizedDescription)
//            complition(false)
//        }
//    }
}
