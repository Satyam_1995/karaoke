//
//  CommentVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 04/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class CommentVM {
    
    weak var vc : CommentVC?
    var result : CommentModel?

    func get_comment_list(video_id:String)  {
//        Para:user_id,followers_id,video_id,comment
//        URL - http://karaoke.yilstaging.com/api/get_comment_list
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
//        parameters[ApiKey.user_id] = "83"
        parameters[ApiKey.video_id] = video_id
        
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_comment_list, loader: true) { (json) in
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(CommentModel.self, from: data)
                if d.status == "success" {
                    self.result = d
                    self.vc?.updateData()
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    func post_comment(video_id:String,comment:String)  {
//        Para:user_id, video_id, comment
//        URL - http://karaoke.yilstaging.com/api/post_comment
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
//        parameters[ApiKey.user_id] = "83"
        parameters[ApiKey.video_id] = video_id
        parameters[ApiKey.comment] = comment
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .post_comment, loader: true) { (json) in
            self.vc?.updateComment()
        } successData: { (data) in
           
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func post_comment_reply(video_id:String,comment:String,comment_id:String)  {
//        Para:user_id,video_id, comment_id,comment
//        URL - http://karaoke.yilstaging.com/api/post_comment_reply
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
//        parameters[ApiKey.user_id] = "83"
        parameters[ApiKey.video_id] = video_id
        parameters[ApiKey.comment] = comment
        parameters[ApiKey.comment_id] = comment_id
        
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .post_comment_reply, loader: true) { (json) in
            self.vc?.updateComment()
        } successData: { (data) in
           
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    func comment_likes_unlike(like:like_status,comment_id:String)  {
//        Para:user_id,comment_id,like_status
//        URL -  http://karaoke.yilstaging.com/api/comment_likes_unlike
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.like_status] = like.rawValue
        parameters[ApiKey.comment_id] = comment_id
        WebServices.commonPostAPI(parameters: parameters, endPoint: .comment_likes_unlike, loader: true) { (json) in
            self.vc?.updateLike()
        } successData: { (data) in
           
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func comment_reply_likes_unlike(like:like_status,reply_id:String)  {
//        Para:user_id,reply_id,like_status (0-unlike,1-like)
//        URL -  http://karaoke.yilstaging.com/api/comment_reply_likes_unlike
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.like_status] = like.rawValue
        parameters[ApiKey.reply_id] = reply_id
        WebServices.commonPostAPI(parameters: parameters, endPoint: .comment_reply_likes_unlike, loader: true) { (json) in
            self.vc?.updateReplayLike()
        } successData: { (data) in
           
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
