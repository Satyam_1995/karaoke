//
//  FollowerVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 22/12/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
class FollowerVM {
    
    weak var vc : FollowerVC?
    var followData : FollowModel?
    var follower_list_all : [Follower] = []
    var following_list_all : [Follower] = []
    var follower_list : [Follower] = []
    var following_list : [Follower] = []
    
    func get_follower_following(user_id:String)  {
//        http://karaoke.yilstaging.com/api/get_follower_following
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_follower_following, loader: true) { (json) in
            
        } successData: { (data) in
            do {
                self.followData = try JSONDecoder().decode(FollowModel.self, from: data)
                self.follower_list = self.followData?.follower ?? []
                self.follower_list = self.follower_list.uniques(by: \.friend_id)
                
                self.following_list = self.followData?.following ?? []
                self.following_list = self.following_list.uniques(by: \.friend_id)
                
                self.follower_list_all = self.follower_list
                self.following_list_all = self.following_list
                self.vc?.updateData()
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    func get_friend_follower_following(user_id:String,friend_id:String)  {
//        Para:user_id,friend_id
//        Task->(check block status, follow status,following status)
//        URL -  http://karaoke.yilstaging.com/api/get_friend_follower_following
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        parameters[ApiKey.friend_id] = friend_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_friend_follower_following, loader: true) { (json) in
        } successData: { (data) in
            do {
                self.followData = try JSONDecoder().decode(FollowModel.self, from: data)
                self.follower_list = self.followData?.follower ?? []
                self.follower_list = self.follower_list.uniques(by: \.friend_id)
                
                self.following_list = self.followData?.following ?? []
                self.following_list = self.following_list.uniques(by: \.friend_id)
                
                self.follower_list_all = self.follower_list
                self.following_list_all = self.following_list
                self.vc?.updateData()
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    
}
