//
//  EditProfileVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 22/12/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
class EditProfileVM {
    weak var vc : EditProfileVC?
    var Profiledetail : ProfileDetailModel?
    func getData()  {
//        let loader = ProfileResponse == nil ? true : false
        WebServices.get_user_profile(loader: true, success: { (json) in
        }, successData: { (data) in
            do {
                self.Profiledetail = try JSONDecoder().decode(ProfileDetailModel.self, from: data)
                let sing = singletonClass.createsingleton()
                sing?.userProfileData = self.Profiledetail
                if self.Profiledetail?.status == "success" {
                    self.vc?.setData(new:true)
                }
            }
            catch {
              print(error)
            }
        }) { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func createProfile() {
        
//        name, user_name, mobile, email, password, fcm_token, bio, user_image
//        URL- http://karaoke.yilstaging.com/api/register

        let para : JSONDictionary = [ApiKey.name:self.vc!.txt_name.text!,
                                     ApiKey.user_name:self.vc!.txt_username.text!,
                                     ApiKey.mobile:"",
                                     ApiKey.email:self.vc!.txt_email.text!,
                                     ApiKey.bio:self.vc!.txt_bio.text!]
        var pmgPar :[UploadFileParameter] = []
        let name = Date.getCurrentDateForName() + ".png"
        
        pmgPar.append(UploadFileParameter(fileName: name, key: ApiKey.user_image, data: self.vc!.imageaAtt!.data!, mimeType: ".png"))
        
        WebServices.commonPostAPI_data(parameters: para, files: pmgPar, endPoint: .register_user, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            if let status = dict["status"] as? String, status == "success" {

            }else {
                let message = dict["message"] as? String ?? ""
                self.vc?.AlertControllerCuston(title: alertTitle.alert_alert, message: message, BtnTitle: ["OK"]) { (str) in
                    
                }
            }
        } successData: { (data) in
            
        } progress: {(pr) in
            
        } failure: { (error) -> (Void) in
            print(error)
            self.vc?.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
        }
    }
    func updateProfileDat(para:JSONDictionary, imgPara : [UploadFileParameter], complation: @escaping (Bool) -> Void) {
//        Para: user_id, name, user_name, mobile, email, password, bio, user_image
//        URL - http://karaoke.yilstaging.com/api/update_user_profile
        WebServices.commonPostAPI_data(parameters: para, files: imgPara, endPoint: .update_user_profile, loader: true) { (json) in
            
        } successData: { (data) in
            complation(true)
            
        } progress: {(pr) in
            
        } failure: { (error) -> (Void) in
            print(error)
            complation(false)
        }

    }
    func check_username() {
        if vc?.txt_username.text == "" {
            return
        }
        let para : JSONDictionary = [ApiKey.username:vc!.txt_username.text! as String]
        WebServices.commonPostAPI(parameters: para, endPoint: .check_username_is_valid, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            let status = dict["status"] as? String ?? ""
            if status == "success" {
                self.vc?.txt_username.tag = 1
            }else if let message = dict["message"] as? String, message == "Username exists!" {
                if self.vc!.txt_username.text! == self.Profiledetail?.data?.first?.user_name {
                    self.vc?.txt_username.tag = 1
                }else{
                    if let suggested = dict["suggested"] as? String {
                        let msg = MessageString.username_already + "\n\nsuggested username is \"\(suggested)\""
                        self.vc?.AlertControllerCuston(title: alertTitle.alert_alert, message: msg, BtnTitle: ["Use suggested","Use another"]) { (str) in
                            if str == "Use suggested" {
                                self.vc?.txt_username.text = suggested
                                self.vc?.txt_username.tag = 1
                            }else{
        //                        self.txt_username.becomeFirstResponder()
                                self.vc?.txt_username.tag = 0
                            }
                        }
                    }
                    self.vc?.txt_username.tag = 0
//                    self.vc?.AlertControllerCuston(title: alertTitle.alert_error, message: MessageString.username_already, BtnTitle: ["OK"], completion: { (_) in
//                    })
                }
            }else {
                self.vc?.txt_username.tag = 0
                self.vc?.AlertControllerCuston(title: alertTitle.alert_error, message: "Something went wrong please login again", BtnTitle: ["OK"], completion: { (_) in
                })
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func check_email() {
        if vc?.txt_email.text == "" {
            return
        }
        let para : JSONDictionary = [ApiKey.email:vc!.txt_email.text! as Any]
        WebServices.commonPostAPI(parameters: para, endPoint: .check_email, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            let message = dict["message"] as? String ?? ""
            if message == "Email not registered!" {
                self.vc?.txt_email.tag = 1
            }else {
                self.vc?.txt_email.tag = 0
                self.vc?.AlertControllerCuston(title: alertTitle.alert_alert, message: MessageString.Email_already_exists, BtnTitle: ["OK"]) { (str) in
                    self.vc?.txt_email.becomeFirstResponder()
                }
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}


//let alert = UIAlertController(title: nil, message: NSLocalizedString("Please wait", comment: ""), preferredStyle: .alert)
//
//        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
//        loadingIndicator.hidesWhenStopped = true
//        loadingIndicator.activityIndicatorViewStyle = UIActivityIndicatorView.Style.gray
//        loadingIndicator.startAnimating();
//
//        alert.view.addSubview(loadingIndicator)
//        ViewController.present(alert, animated: true, completion: nil)
