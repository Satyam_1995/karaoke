//
//  SubscriptionPlanVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 22/12/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
class SubscriptionPlanVM {
    
    weak var vc : SubscriptionPlanVC?
    var Profiledetail : ProfileDetailModel?
    var myplan_details : [Myplan_details] = []
    var subscription_plan : [Subscription_plan] = []
    var currentDeviceDate = Date()
    func user_subscription_plan()  {
//        http://karaoke.yilstaging.com/api/user_subscription_plan
        // user_id
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as! String
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .user_subscription_plan, loader: true) { (json) in
            print(json)
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(SubscriptionModel.self, from: data)
                if d.status == "success" {
                    self.myplan_details = d.myplan_details ?? []
                    self.subscription_plan = d.subscription_plan ?? []
                    if let da = d.current_date {
                        self.currentDeviceDate = self.vc?.convertDateFormat(inputDate:da) ?? Date()
                    }
//                    self.subscription_plan.append(contentsOf: self.subscription_plan)
//                    self.subscription_plan.append(contentsOf: self.subscription_plan)
//                    self.subscription_plan.append(contentsOf: self.subscription_plan)
                    self.vc?.updateData()
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    func set_subscription_plan(planid:String, tx_id:String,check:String,expire_on:String)  {
//        http://karaoke.yilstaging.com/api/set_subscription_plan
        // user_id,planid, tx_id
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.planid] = planid
        parameters[ApiKey.tx_id] = tx_id
        parameters[ApiKey.check] = check
        parameters[ApiKey.expire_on] = expire_on
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .set_subscription_plan, loader: true) { (json) in
            guard let data = json.dictionaryObject else{
                return
            }
            if let state = data["status"] as? String {
                
                self.vc?.updateTranstion(check:check)
            }else{
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
