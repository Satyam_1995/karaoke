//
//  FavoriteVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 05/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class FavoriteVM {
    
    weak var vc : FavoriteVC?
    var favouriteData : FavouriteModel?
    var userVideoList : [Video_list] = []

    func get_fav_list(user_id:String = UserDetail.shared.getUserId())  {
//        Para:user_id,
//        URL - http://karaoke.yilstaging.com/api/get_fav_list
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_fav_list, loader: true) { (json) in
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(FavouriteModel.self, from: data)
                if dict.status == "success" {
                    self.userVideoList = dict.fav_list ?? []
                    self.vc?.updateData(new:true)
                    self.vc?.collect.reloadData()
                }else{
                    self.vc?.AlertControllerOnr(title: alertTitle.alert_error, message: "No favorites video found.")
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
