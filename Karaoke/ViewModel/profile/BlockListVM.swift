//
//  BlockListVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 08/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class BlockListVM {
    
    weak var vc : BlockListVC?
    var favouriteData : BlockListModel?
    var blockList : [BlockListUser] = []

    func get_block_list(user_id:String = UserDetail.shared.getUserId())  {
//        Para: user_id
//        URL - http://karaoke.yilstaging.com/api/get_block_list
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_block_list, loader: true) { (json) in
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(BlockListModel.self, from: data)
                if dict.status == "success" {
                    self.blockList = dict.data ?? []
                    self.vc?.updateData(new:true)
                    self.vc?.table.reloadData()
                }else{
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func block_unblock(friend_id:String)  {
//        Para:  user_id, followerid
//        URL - http://karaoke.yilstaging.com/api/block_unblock
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = UserDetail.shared.getUserId()
        parameters[ApiKey.followerid] = friend_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .block_unblock, loader: true) { (json) in
            
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(userVideoListModel.self, from: data)
                if dict.status == "success" {
                    self.vc?.updateBlockList(new: true)
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
