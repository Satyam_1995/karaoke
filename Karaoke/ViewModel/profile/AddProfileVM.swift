//
//  AddProfileVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 21/12/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation

class AddProfileVM {
    
    weak var vc : AddProfileVC?
    var Profiledetail : ProfileDetailModel?
    var userVideoList : [User_videos] = []
    func getData(UserId:String = UserDetail.shared.getUserId())  {
        let loader = Profiledetail == nil ? true : false
        WebServices.get_user_profile(user_id:UserId, loader: loader, success: { (json) in
        }, successData: { (data) in
            do {
                self.Profiledetail = try JSONDecoder().decode(ProfileDetailModel.self, from: data)
                let sing = singletonClass.createsingleton()
                sing?.userProfileData = self.Profiledetail
                if self.Profiledetail?.status == "success" {
                    self.vc?.setData(new:true)
                }
            }
            catch {
              print(error)
            }
        }) { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
//    func get_follower_profile(user_id:String = UserDetail.shared.getUserId())  {
////        http://karaoke.yilstaging.com/api/get_follower_profile
////        let loader = ProfileResponse == nil ? true : false
//        var parameters : [String:Any] = [:]
//        parameters[ApiKey.user_id] = user_id
//        
//        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_follower_profile, loader: true) { (json) in
//        } successData: { (data) in
//            do {
//                self.Profiledetail = try JSONDecoder().decode(ProfileDetailModel.self, from: data)
////                let sing = singletonClass.createsingleton()
////                sing?.userProfileData = self.Profiledetail
//                if self.Profiledetail?.status == "success" {
//                    self.vc?.setData(new:true)
//                }
//            }
//            catch {
//              print(error)
//            }
//        } failure: { (err) -> (Void) in
//            print(err.localizedDescription)
//        }
//    }
    func get_user_videos(user_id:String = UserDetail.shared.getUserId())  {
//        http://karaoke.yilstaging.com/api/get_user_videos
        let loader = self.userVideoList.count == 0 ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_user_videos, loader: loader) { (json) in
            print(json)
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(userVideoListModel.self, from: data)
                
                if dict.status == "success" {
                    self.userVideoList = dict.user_videos ?? []
                    if dict.user_videos?.count ?? 0 > 0 {
                        self.vc?.pStatus = 1
                        self.vc?.updateUI()
                    }else{
                        self.vc?.pStatus = 2
                        self.vc?.updateUI()
                    }
                    self.vc?.collect.reloadData()
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
