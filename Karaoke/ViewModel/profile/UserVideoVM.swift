//
//  UserVideoVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 29/12/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
class UserVideoVM {
    
    var vc : UserVideoVC?
    var videoDetail : Video_detail?
    
    func get_video_details(video_d:String)  {
//        Para:video_id,user_id
//        URL - http://karaoke.yilstaging.com/api/get_video_details
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as! String
        parameters[ApiKey.user_id] = user_id
        
        WebServices.get_video_details(video_id: video_d) { (json) in
            
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(SingleVideoDetail.self, from: data)
                if d.status == "success" {
                    self.videoDetail = d.video_detail?.first
                    videoDetailCache[video_d] = self.videoDetail
                    self.vc?.upNewData(new: true)
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
}
