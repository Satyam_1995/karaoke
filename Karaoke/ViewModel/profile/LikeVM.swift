//
//  LikeVM.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 28/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
class LikeVM {
    
    weak var vc : LikeVC?
    var like_list : [Like_list] = []

    func get_like_list(user_id:String = UserDetail.shared.getUserId(),video_id:String)  {
//        Para:user_id,video_id
//        URL - http://karaoke.yilstaging.com/api/get_like_list
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        parameters[ApiKey.user_id] = user_id
        parameters[ApiKey.video_id] = video_id
        
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .get_like_list, loader: true) { (json) in
        } successData: { (data) in
            do {
                let dict = try JSONDecoder().decode(LikeModel.self, from: data)
                if dict.status == "success" {
                    self.like_list = dict.like_list ?? []
                    self.vc?.updateData()
                }else{
                    self.vc?.AlertControllerOnr(title: alertTitle.alert_error, message: "No like found.")
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}
