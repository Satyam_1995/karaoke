//
//  PreviewVideoVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import MiniPlayer
import AVKit
import Photos

class PreviewVideoVC: UIViewController {
    
    @IBOutlet weak var waveform: FDWaveformView!
    @IBOutlet weak var collect:UICollectionView!
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var viewLayer:UIView!
    @IBOutlet weak var lblTimer:UILabel!
    
    @IBOutlet weak var audioView:UIView!
    @IBOutlet weak var btnAudioApply:UIButton!
    @IBOutlet weak var btnAudioCancel:UIButton!
    @IBOutlet weak var miniPlayer: MiniPlayer!
    var timeObserverToken: Any?
    var lblArr : [UIView] = []
    
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    var player : AVPlayer?
    
    var lyrics : String = ""
    var song_name : String = ""
    var music_cat_id : String = ""
    var artist_name : String = ""
    var old_video_id : String = ""
    
    var selectedIndex = IndexPath(item: -1, section: 0)
    var soundMurgeArray : [soundMergeList] = []
    var videoMurgeArray : [URL] = []
    var sound_list : [Sound_list] = []
    var _filename = "kar"
    var _filenameCount = 0
    
    
    var videoURL : URL!
    var video_thumb : Data!
    deinit {
        NotificationCenter.default.removeObserver(self)
//        player?.removeTimeObserver(self)
      }
    override func viewDidLoad() {
        super.viewDidLoad()
        if videoMurgeArray.count > 0 {
            videoURL = videoMurgeArray.last
        }
        collect.delegate = self
        collect.dataSource = self
        
        self.setUpVideo()
        self.btnPlay.setImage(#imageLiteral(resourceName: "loginPlay"), for: .selected)
        NotificationCenter.default.addObserver(self, selector: #selector(playerItemDidReachEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: nil)
        
        waveform.waveformType = .linear
        waveform.audioURL = videoURL
        waveform.delegate = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Timer.scheduledTimer(withTimeInterval: 2, repeats: false) { (_) in
            DispatchQueue.main.async {
//                self.createLbl()
            }
        }
    }
    func setUpVideo() {
        audioView.frame = self.view.frame
        audioView.center = self.view.center
        audioView.isHidden = true
        self.view.addSubview(audioView)
        
        DispatchQueue.main.async {
            if nil != self.videoURL {
//                self.playVideo.configure(url: self.videoURL!)
                self.showPlayer(item: self.videoURL.path)
                self.addPeriodicTimeObserver()
            }
        }
    }
    @objc func playerItemDidReachEnd(_ notification: Notification) {
        player!.seek(to: CMTime.zero)
        player!.play()
        
    }
    func showPlayer(item: String) {
        player = AVPlayer(url: self.videoURL!)
        let avPlayerLayer = AVPlayerLayer(player: player)
        avPlayerLayer.videoGravity = .resizeAspect
        avPlayerLayer.masksToBounds = true
        avPlayerLayer.cornerRadius = 5
        avPlayerLayer.videoGravity = .resizeAspectFill
        avPlayerLayer.frame = imgProfile.layer.bounds
        self.imgProfile.layer.addSublayer(avPlayerLayer)
        if (player != nil) {
            player!.play()
        }
        DispatchQueue.main.async {
            avPlayerLayer.frame = self.view.frame
//            avPlayerLayer.center = self.view.center
        }
    }
    func playItem(at itemURL: URL) {
        let item = AVPlayerItem(url: itemURL)
        player?.pause()
        player?.seek(to: .zero)
        player?.replaceCurrentItem(with: item)
        player?.play()
        waveform.waveformType = .linear
        waveform.audioURL = itemURL
        addPeriodicTimeObserver()
    }
    @IBAction func btnPlayAction(_ sender:UIButton) {
        if sender.isSelected == true {
            player?.play()
        }else{
            player?.pause()
        }
        sender.isSelected = !sender.isSelected
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.pause()
        btnPlay.isSelected = true
        removePeriodicTimeObserver()
    }
    @IBAction func btnBack(_ sender:UIButton) {
        removePeriodicTimeObserver()
        self.navigationController?.popViewController(animated: true)
        self.select?("",2)
    }
    @IBAction func btnDownload(_ sender:UIButton) {
        if videoMurgeArray.count > 0 {
            self.saveToCameraRoll(url: videoMurgeArray.last ?? videoURL)
        }else{
            self.saveToCameraRoll(url: videoURL)
        }
    }
    func saveToCameraRoll(url: URL) {
        
        if PHPhotoLibrary.authorizationStatus() == .authorized {
            let load = LoadingView()
            load.showActivityLoading(uiView: self.view)
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
            }) { (saved, error) in
                DispatchQueue.main.async {
                    load.hideActivityLoading(uiView: self.view)
                    if !saved {
                        self.showToast(message: "Could not save!! \n\(String(describing: error?.localizedDescription))", fontSize: 12)
                    } else {
                        self.showToast(message: "Video downloaded successfully", fontSize: 12.0)
                    }
                }
            }
        }else if PHPhotoLibrary.authorizationStatus() == .denied {
            let alertController = UIAlertController(title: nil, message: NSLocalizedString("photo_library_permission", comment: ""), preferredStyle: .alert)
            let settingsAction = UIAlertAction(title: NSLocalizedString("settings", comment: ""), style: .default) { (_) -> Void in
                guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                    return
                }
                if UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)")
                    })
                }
            }
            let cancelAction = UIAlertAction(title: NSLocalizedString("close", comment: ""), style: .cancel, handler: nil)
            alertController.addAction(cancelAction)
            alertController.addAction(settingsAction)
            DispatchQueue.main.async {
                self.present(alertController, animated: true)
            }
        }else {
            PHPhotoLibrary.requestAuthorization { status in
                if #available(iOS 14, *) {
                    if status == .authorized || status == .limited {
                        DispatchQueue.main.async {
                            self.saveToCameraRoll(url: url)
                        }
                    }
                } else {
                    if status == .authorized {
                        DispatchQueue.main.async {
                            self.saveToCameraRoll(url: url)
                        }
                    }
                }
            }
        }
    }
    @objc func video(_ video: String, didFinishSavingWithError error: NSError?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            showToast(message: "Could not save!! \n\(error)", fontSize: 12)
        } else {
            showToast(message: "Video downloaded successfully", fontSize: 12.0)
        }
        print(video)
    }
    func showToast(message:String,fontSize:Float)  {
        CommonFunctions.showToastWithMessage(message, displayTime: 2) { }
    }
    @IBAction func btnPublish(_ sender:UIButton) {
        let vc = UploadedVC.instantiate(fromAppStoryboard: .Home)
        var url = videoURL
        if videoMurgeArray.count > 0 {
            url = videoMurgeArray.last
        }
        vc.videoURL = url
        vc.lyrics = self.lyrics
        vc.song_name = self.song_name
        vc.music_cat_id = self.music_cat_id
        vc.artist_name = self.artist_name
        vc.old_video_id = self.old_video_id
        vc.video_thumb = video_thumb
        
        vc.hidesBottomBarWhenPushed = true
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.navigationController?.popViewController(animated: true)
                self?.select?("",1)
//                self?.tabBarController?.selectedIndex = 0
//                let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
//                self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        self.present(vc, animated: true) {
            self.player?.pause()
            self.btnPlay.isSelected = true
        }
        player?.pause()
        btnPlay.isSelected = true
    }
}


extension PreviewVideoVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.sound_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "SingItCell", for: indexPath) as! SingItCell
        cell.lbl_txt.text = sound_list[indexPath.item].effect_name
        cell.lbl_txt.textAlignment = .center
        cell.backgroundColor = UIColor(named: "audioFilterBackground") ?? #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3)
        if self.selectedIndex == indexPath {
            cell.backgroundColor = UIColor(named: "audioFilterBackgroundSelected") ?? #colorLiteral(red: 0.9294117647, green: 0.2784313725, blue: 0.3411764706, alpha: 1)
        }
        cell.layer.cornerRadius = 10
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath
        collect.reloadData()
        let dict = sound_list[indexPath.item]
        if let url = self.read_url(fromDocumentsWithFileName: dict.sound_id! + ".mp3"){
            self.displayView(url: url)
        }else{
            AppNetworking.DOWNLOAD(endPoint: dict.effect_url!, fileName: dict.sound_id!, parameters: [:], mediaType: ".mp3", loader: false) { (status) in
            } successData: { (url) in
                if let url = url {
                    self.displayView(url: url)
                }
            } failure: { (error) in
                print(error)
            }
        }
    }
    func displayView(url:URL){
        self.player?.pause()
        self.audioView.isHidden = false
        let song = AVPlayerItem(asset: AVAsset(url: url), automaticallyLoadedAssetKeys: ["playable"])
        self.miniPlayer.soundTrack = song
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tt = self.sound_list[indexPath.item]
        let lbl = UILabel()
        lbl.text = tt.effect_name ?? ""
        let pp = lbl.textWidth() + 50
//        let w = collect.frame.size.width/3
        let h = collect.frame.size.height
        return CGSize(width: pp , height: h)
    }
    func read_url(fromDocumentsWithFileName fileName: String) -> URL? {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
                return pathComponent
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
}
extension PreviewVideoVC: FDWaveformViewDelegate {
    func waveformViewWillRender(_ waveformView: FDWaveformView) {
    }
    func waveformViewDidRender(_ waveformView: FDWaveformView) {
        UIView.animate(withDuration: 0.25, animations: {() -> Void in
            waveformView.alpha = 1.0
        })
    }
    func waveformViewWillLoad(_ waveformView: FDWaveformView) {
    }
    func waveformViewDidLoad(_ waveformView: FDWaveformView) {
    }
    func addPeriodicTimeObserver() {
        removePeriodicTimeObserver()
        let timeScale = CMTimeScale(NSEC_PER_SEC)
        let time = CMTime(seconds: 0.1, preferredTimescale: timeScale)
        var total = -1.0
        timeObserverToken = player?.addPeriodicTimeObserver(forInterval: time,
                                                          queue: .main) {
            [weak self] time in
            if total == -1 || total.isNaN {
                if let tot = self?.player?.currentItem?.duration.seconds, tot != 0  {
                   let totalSamples = self!.waveform.totalSamples
                    total = Double(totalSamples) / tot
                    print("total",total)
                }
            }
            if !total.isNaN && total != -1 {
                let rem = self?.player?.currentItem?.currentTime().seconds ?? 1
                self?.lblTimer.text = "\(Int(rem))"
                let new  = rem * total
                let n = Int(new)
                if n > 0 {
                    self?.waveform.highlightedSamples = 0 ..< Int(n)
                }
            }
        }
    }
    func removePeriodicTimeObserver() {
        if let timeObserverToken = timeObserverToken {
            if player?.rate == 1.0{
            player?.removeTimeObserver(timeObserverToken)
            self.timeObserverToken = nil
            }
        }
    }
    func createLbl() {

        for item in lblArr {
            item.removeFromSuperview()
        }
        lblArr.removeAll()
        let d_wirth = self.view.frame.size.width
        var tag = 0
        for item in soundMurgeArray {
            DispatchQueue.main.async {
                if let tot = self.player?.currentItem?.duration.seconds {
                    let lable = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: self.viewLayer.frame.size.height))
                    lable.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0.3003042936, alpha: 0.65)
                    let total = Double(d_wirth) / tot
                    lable.frame.origin.x = CGFloat(total * item.time)
                    let audioAsset = AVURLAsset(url: item.url, options: nil)
                    let audioDuration = audioAsset.duration
                    let audioDurationSeconds = Double(CMTimeGetSeconds(audioDuration))
                    lable.frame.size.width = CGFloat(total * audioDurationSeconds)
                    self.viewLayer.addSubview(lable)
                    self.lblArr.append(lable)
                    let btn = UIButton(frame: CGRect(x: lable.frame.size.width-20, y: 0, width: 20, height: 20))
                    btn.setImage(#imageLiteral(resourceName: "cross2"), for: .normal)
                    btn.tag = tag
                    btn.addTarget(self, action: #selector(self.actionDeleteEffect(_:)), for: .touchUpInside)
                    tag = tag + 1
//                    lable.addSubview(btn)
                }
            }
        }
    }
    @IBAction func actionDeleteEffect(_ sender:UIButton) {
        soundMurgeArray.remove(at: sender.tag)
        self.mergeSegmentsAndUpload()
    }
}
extension PreviewVideoVC : AVCaptureVideoDataOutputSampleBufferDelegate {

    func mergeSegmentsAndUpload() {
        DispatchQueue.main.async { [self] in
            if let urlV = self.videoURL {
                self._filenameCount = self._filenameCount + 1
                let name = self._filename + "\(self._filenameCount)"
                let lo = LoadingView()
                lo.showActivityLoading(uiView: self.view)
                VideoCompositionWriter().pavan_mergeVideoAndAudio(videoUrl: urlV, audioArrayList: soundMurgeArray, shouldFlipHorizontally: false, filename: name) { (error, url) in
                    DispatchQueue.main.async {
                        lo.hideActivityLoading(uiView: self.view)
                    }
                    guard let url = url else {
                        return
                    }
                    print(url)
                    
                    self.videoMurgeArray.append(url)
                    DispatchQueue.main.async {
                        self.playItem(at: url)
//                        self.createLbl()
                    }
                }
            }
        }
    }
    @IBAction func btnAudioApplyAction(_ sender: Any) {
        self.miniPlayer.stop()
        self.audioView.isHidden = true
        guard let time = self.player?.currentTime().seconds else {
            return
        }
        let dict = sound_list[selectedIndex.item]
        if let url = self.read_url(fromDocumentsWithFileName: dict.sound_id! + ".mp3"){
            soundMurgeArray.append(soundMergeList(url: url, time: time, id: dict.sound_id!))
            self.mergeSegmentsAndUpload()
        }
    }
    @IBAction func btnAudioCancelAction(_ sender: Any) {
        self.miniPlayer.stop()
        self.audioView.isHidden = true
        player?.play()
    }
    
}
