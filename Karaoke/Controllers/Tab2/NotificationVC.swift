//
//  NotificationVC.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class NotificationVC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    let viewModel = NotificationVM()
    let user_id = UserDetail.shared.getUserId()
    var shouldShowLoadingCell = false
    var refreshControl : UIRefreshControl?
    var page = 0
    var loadAgain  = false
    var recorder : KVideoRecorder?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.vc = self
        
        self.table.register(UINib(nibName: "NotificationCell", bundle: nil), forCellReuseIdentifier: "NotificationCell")
        
        table.delegate = self
        table.dataSource = self
//        tabBarController?.delegate = self
        refreshControl = UIRefreshControl()
        refreshControl?.addTarget(self, action: #selector(refreshBeers), for: .valueChanged)

        refreshControl?.beginRefreshing()
        table.addSubview(refreshControl!)

        viewModel.get_notification_list(last_id: "0")
//        recorder = KVideoRecorder(to: self.table)
//        recorder!.setup(.video)
//        recorder?.delegate = self
//        recorder?.toggleCamera()
        if UIDevice.current.hasNotch {
            let ss = self.iScreenSizes()
            tabBarController!.tabBar.frame = CGRect(x: 10, y: ss - 30, width: tabBarController!.tabBar.frame.size.width, height: 80.0)
        }
    }
    
    @objc private func refreshBeers() {
        page = 0
        viewModel.get_notification_list(last_id: "0")
    }
    var timrNew : Timer?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.post(name: NSNotification.Name("newNotificaton"), object: false)
        self.navigationController?.viewControllers = [self]
        self.tabBarController?.tabBar.isHidden = false
//        self.navigationController?.isNavigationBarHidden = true
        if self.loadAgain {
            self.viewModel.get_notification_listNew(last_id: "0")
        }
        timrNew?.invalidate()
        timrNew = nil
        timrNew = Timer.scheduledTimer(withTimeInterval: 5, repeats: true, block: { (t) in
            if self.loadAgain {
                self.viewModel.get_notification_listNew(last_id: "0")
                NotificationCenter.default.post(name: NSNotification.Name("newNotificaton"), object: false)
            }
        })
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timrNew?.invalidate()
        timrNew = nil
    }
    @IBAction func btn_Back(_ sender: UIButton){
        tabBarController?.selectedIndex = 0
    }
    var ss = true
    @IBAction func btn_Search(_ sender: UIButton){
//        if ss {
//            recorder?.record(name: "test")
//        }else{
//            recorder?.stop()
//
//        }
//        ss = !ss
        let vc = SongBookPageVC.instantiate(fromAppStoryboard: .Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func updateData(new:Bool) {
        self.table.reloadData()
        refreshControl?.endRefreshing()
        loadAgain = true
    }
    func updateMayBeLater(index:Int) {
        self.viewModel.notification_list[index].maybelater = "1"
        self.table.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
//        let friend_id = self.viewModel.notification_list[index].friendid ?? ""
//        for item in 0..<self.viewModel.notification_list.count {
//            if (self.viewModel.notification_list[item].friendid ?? "") == friend_id && index != item {
//                self.viewModel.notification_list[item].maybelater = "1"
//            }
//        }
//        self.table.reloadData()
    }
}


extension NotificationVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = viewModel.notification_list.count
        return shouldShowLoadingCell ? count + 1 : count
//        return viewModel.notification_list.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 108
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoadingIndexPath(indexPath) {
            return LoadingCell(style: .default, reuseIdentifier: "loading")
        } else {
            let cell = table.dequeueReusableCell(withIdentifier: "NotificationCell", for: indexPath) as! NotificationCell
            let dict  = viewModel.notification_list[indexPath.row]
            if dict.type == "Follow" {
                return self.cellFollow(cell: cell, indexPath: indexPath)
            }else if dict.type == "Comment" {
                return self.cellCommen(cell: cell, indexPath: indexPath)
            }else{
                return self.cellLike(cell: cell, indexPath: indexPath)
            }
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        getMoreImages(page: page)
        
//        if viewModel.notification_list.count == indexPath.row && viewModel.notification_list.count%5 == 0{
//           getMoreImages(page: page)
//        }
    }
    func getMoreImages(page:Int){
        let last = viewModel.notification_list.last?.notification_id ?? ""
        self.viewModel.get_notification_list(last_id: last, refresh: false)
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.viewModel.notification_list.count
    }
    func cellCommen(cell: NotificationCell, indexPath:IndexPath) -> NotificationCell {
        let dict = viewModel.notification_list[indexPath.row]
        if let url = dict.user_profile?.convert_to_url {
            cell.img_user.af.setImage(withURL: url)
        }
        cell.img_video.isHidden = false
        cell.img_video1.isHidden = false
        cell.btn_View.isHidden = true
        cell.heightConstraints.constant = 0
        if dict.user_follow_status == 0 && dict.maybelater == "0" {
            if dict.friend_follow_status == 1 {
            }else{
            }
            cell.btnLater.tag = indexPath.row
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action: #selector(btn_Follow(_:)), for: .touchUpInside)
            cell.btnLater.addTarget(self, action: #selector(btn_Later(_:)), for: .touchUpInside)
            cell.heightConstraints.constant = 0
            cell.btn_View.isHidden = false
            cell.heightConstraints.constant = 25
        }
        let name = dict.name ?? ""
        let message = dict.message ?? ""
        let txt1 = customizeFont(string: "\(name) ", font: .boldSystemFont(ofSize: 12), color: .black)
        let txt2 = customizeFont(string: "Commented: ", font: .systemFont(ofSize: 12), color: UIColor.red)
        let txt3 = customizeFont(string: "\"\(message)\"", font: .boldSystemFont(ofSize: 12), color: .black)
        txt1.append(txt2)
        txt1.append(txt3)
        cell.lblComment.attributedText = txt1
        cell.lblTime.text = self.stringToDate(date: dict.created_at ?? "")
        if let url = dict.video_details?.video_thumb?.convert_to_url {
            cell.img_video.af.setImage(withURL: url)
        }
        return cell
    }
    func cellFollow(cell: NotificationCell, indexPath:IndexPath) -> NotificationCell {
        let dict = viewModel.notification_list[indexPath.row]
        if let url = dict.user_profile?.convert_to_url {
            cell.img_user.af.setImage(withURL: url)
        }
        cell.img_video.isHidden = true
        cell.img_video1.isHidden = true
        cell.btn_View.isHidden = true
        cell.heightConstraints.constant = 0
        if dict.user_follow_status == 0 && dict.maybelater == "0" {
            if dict.friend_follow_status == 1 {
            }else{
            }
            cell.btnLater.tag = indexPath.row
            cell.btnFollow.tag = indexPath.row
            cell.btnFollow.addTarget(self, action: #selector(btn_Follow(_:)), for: .touchUpInside)
            cell.btnLater.addTarget(self, action: #selector(btn_Later(_:)), for: .touchUpInside)
            cell.heightConstraints.constant = 0
            cell.btn_View.isHidden = false
            cell.heightConstraints.constant = 25
        }
        let name = dict.name ?? ""
        let txt1 = customizeFont(string: "\(name) ", font: .boldSystemFont(ofSize: 12), color: .black)
        let txt4 = customizeFont(string: "started following you.", font: .systemFont(ofSize: 12), color: UIColor.red)
        txt1.append(txt4)
        cell.lblComment.attributedText = txt1
        cell.lblTime.text = self.stringToDate(date: dict.created_at ?? "")
        return cell
    }
    func cellLike(cell: NotificationCell, indexPath:IndexPath) -> NotificationCell {
        let dict = viewModel.notification_list[indexPath.row]
        if let url = dict.user_profile?.convert_to_url {
            cell.img_user.af.setImage(withURL: url)
        }
        cell.img_video.isHidden = false
        cell.img_video1.isHidden = false
        cell.btn_View.isHidden = true
        cell.heightConstraints.constant = 0
        
        let fname = dict.user_likes.last?.name ?? ""
        let txt1 = customizeFont(string: "\(fname) ", font: .boldSystemFont(ofSize: 12), color: .black)
        if dict.user_likes.count > 2 {
            let txt2 = customizeFont(string: "and ", font: .systemFont(ofSize: 12), color: UIColor.red)
            let txt3 = customizeFont(string: "\(dict.user_likes.count - 1) Others", font: .boldSystemFont(ofSize: 12), color: .black)
            txt1.append(txt2)
            txt1.append(txt3)
        }else if dict.user_likes.count == 2 {
            let txt2 = customizeFont(string: "and ", font: .systemFont(ofSize: 12), color: UIColor.red)
            let txt3 = customizeFont(string: dict.user_likes.first?.name ?? "", font: .boldSystemFont(ofSize: 12), color: .black)
            txt1.append(txt2)
            txt1.append(txt3)
        }
        let txt4 = customizeFont(string: " liked your video.", font: .systemFont(ofSize: 12), color: UIColor.red)
        txt1.append(txt4)
        cell.lblComment.attributedText = txt1
        cell.lblTime.text = self.stringToDate(date: dict.created_at ?? "")
        if let url = dict.video_details?.video_thumb?.convert_to_url {
            cell.img_video.af.setImage(withURL: url)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict  = viewModel.notification_list[indexPath.row]
        if dict.type == "Follow" {
            guard let friend_id = dict.friendid else { return }
            if user_id == friend_id {
                self.tabBarController?.selectedIndex = 4
                return
            }
            let vc = OtherUserProfileVC.instantiate(fromAppStoryboard: .Home)
            vc.friend_id = friend_id
            vc.hidesBottomBarWhenPushed = false
            self.navigationController?.pushViewController(vc, animated: true)
        }else {
            guard let detail = dict.video_details else { return }
            
            let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
            vc.hidesBottomBarWhenPushed = true
            if let videoURL =  detail.video_file?.convert_to_string, let v_id = dict.videoid {
                vc.videoURL = videoURL
                vc.video_id = v_id
                if dict.type == "Comment" {
                    vc.openCommentView = true
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                self.AlertControllerOnr(title: alertTitle.alert_error, message: "Something went wrong with this video.")
            }
        }
    }
    
    @IBAction func btn_Follow(_ sender: UIButton){
        sender.isHidden = true
        let dict = viewModel.notification_list[sender.tag]
        guard let friend_id = dict.friendid else {
            return
        }
        if user_id == friend_id {
            CommonFunctions.showToastWithMessage("you can't follow to yourself", displayTime: 2, completion: nil)
            return
        }
        WebServices.follow_unfollow(friend_id: friend_id, follow_type: .follow, loader: true) { (json) in
        } successData: { (data) in
            self.viewModel.notification_list[sender.tag].user_follow_status = 1
            for item in 0..<self.viewModel.notification_list.count {
                if (self.viewModel.notification_list[item].friendid ?? "") == friend_id && sender.tag != item {
                    self.viewModel.notification_list[item].user_follow_status = 1
                }
            }
            self.table.reloadData()
        } failure: { (err) -> (Void) in
            print(err)
            sender.isHidden = false
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
    @IBAction func btn_Later(_ sender: UIButton){
        guard let dict = viewModel.notification_list[sender.tag].notification_id else {
            return
        }
        viewModel.may_be_later(notification_id: dict, index: sender.tag)
//        table.reloadData()
    }
}

extension NotificationVC:UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index = tabBarController.selectedIndex
        if index == 1 {
//            for i in imgArr1 {
//                imgArr.append(i)
//            }
            table.reloadData()
        }
        
    }
}
extension NotificationVC: KVideoRecorderDelegate {
    
    func outputURLURL(outputURL: URL) {
        print(outputURL)
        PreviewVideoVC().saveToCameraRoll(url: outputURL)
    }
    
    func timer(second: Int) {
        print(second)
    }
    
}
