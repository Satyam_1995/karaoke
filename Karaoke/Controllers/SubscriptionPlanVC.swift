//
//  SubscriptionPlanVC.swift
//  Karaoke
//
//  Created by Ankur  on 19/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit



class SubscriptionPlanVC: UIViewController {
    #if DEBUG
        let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
    #else
        let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
    #endif
    
    fileprivate var transactionHandler: InAppPurchaseTransactionHandler!
    
    @IBOutlet weak var table:UITableView!
//    var list : [strPlanList] = []
    let addProAPI = SubscriptionPlanVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        Setup()
        addProAPI.vc = self
        addProAPI.user_subscription_plan()
        let previous = UserDefaults.standard.object(forKey: "previous_arr") as? [String] ?? []
//        previous.append(dict)
        print("previous",previous)
    }
    func updateData()  {
        self.table.reloadData()
    }
    func updateTranstion(check:String)  {
        if check == "1" {
            self.AlertControllerOnr(title: alertTitle.alert_success, message: "Subscription plan purchased successfully.")
            addProAPI.user_subscription_plan()
        }
    }
    
    func Setup(){
        
        self.table.register(UINib(nibName: "SubscriptionPlanCell", bundle: nil), forCellReuseIdentifier: "SubscriptionPlanCell")
        
        table.delegate = self
        table.dataSource = self
        
//        list.append(strPlanList(strPlanPrice: "$8", strPlanName: "Monthly Plan", imgBack: #imageLiteral(resourceName: "sp1"), strPlanType: "Per Month"))
//        list.append(strPlanList(strPlanPrice: "$12", strPlanName: "3 Months Plan", imgBack: #imageLiteral(resourceName: "sp2"), strPlanType: "On every 3 Months"))
//        list.append(strPlanList(strPlanPrice: "$22", strPlanName: "6 Months Plan", imgBack: #imageLiteral(resourceName: "sp3"), strPlanType: "On every 6 Months"))
//        list.append(strPlanList(strPlanPrice: "$45", strPlanName: "1 Year Plan", imgBack: #imageLiteral(resourceName: "sp4"), strPlanType: "Per Year"))
        
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SubscriptionPlanVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return addProAPI.subscription_plan.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "SubscriptionPlanCell", for: indexPath) as! SubscriptionPlanCell
        
        let dict = addProAPI.subscription_plan[indexPath.row]
        cell.lblPlanDetail.text = dict.plan_details ?? ""
        cell.lblPlanPrice.text = "$" + (dict.plan_amount ?? "$0")
        cell.lblPlanName.text = dict.plan_name ?? ""
        cell.lblPlanType.text = dict.plan_name ?? ""
        cell.btnBuy.tag = indexPath.item
        if !self.addProAPI.myplan_details.contains(where: {$0.planid == dict.plan_id!}) {
            cell.btnBuy.addTarget(self, action: #selector(self.btnbuy(_:)), for: .touchUpInside)
        }else{
            cell.btnBuy.addTarget(self, action: #selector(self.btnbuy(_:)), for: .touchUpInside)
//            cell.btnBuy.removeTarget(self, action: #selector(self.btnbuy(_:)), for: .touchUpInside)
        }

        
        if indexPath.row%4 == 0{
            cell.imgBack.image = #imageLiteral(resourceName: "sp1")
        }else if indexPath.row%4 == 1 {
            cell.imgBack.image = #imageLiteral(resourceName: "sp2")
        }else if indexPath.row%4 == 2 {
            cell.imgBack.image = #imageLiteral(resourceName: "sp3")
        }else if indexPath.row%4 == 3 {
            cell.imgBack.image = #imageLiteral(resourceName: "sp4")
        }else{
//            cell.imgBack.image = #imageLiteral(resourceName: "sp1")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0 {
//            let dict = addProAPI.subscription_plan[indexPath.row].plan_id ?? ""
//            self.addProAPI.set_subscription_plan(planid: dict, tx_id: "1")
            guard let dict = addProAPI.subscription_plan[indexPath.row].plan_id else {
                return
            }
//            self.addProAPI.set_subscription_plan(planid: dict, tx_id: "", check: "0")
//        }
    }
    @IBAction func btnbuy(_ sender:UIButton) {
        guard let dict = addProAPI.subscription_plan[sender.tag].plan_id else {
            return
        }
        
        let chec = checkFor(planid: dict)
        if chec == dict {
            self.AlertControllerCuston(title: alertTitle.alert_success, message: "You've already purchased this.", BtnTitle: ["OK"]) { (s) in
            }
            return
        }
        
        let checlToAll = checkFor()
        if checlToAll != "" {
            self.AlertControllerCuston(title: alertTitle.alert_success, message: "You've already purchased \(checlToAll) plan. Would you like to purchase another plan?", BtnTitle: ["Cancel","Buy"]) { (s) in
                if s == "Buy" {
                    self.buy(index: sender.tag)
                }
            }
            return
        }
        self.buy(index: sender.tag)
    }
    func checkFor(planid:String) -> String {
        var returntype = ""
        for item in self.addProAPI.myplan_details {
            if item.planid == planid {
                let s = convertDateFormat(inputDate: item.expire_on)
                if s >= self.addProAPI.currentDeviceDate {
                    returntype = planid
                    break
                }
            }
        }
        return returntype
    }
    func checkFor() -> String {
        var returntype = ""
        for item in self.addProAPI.myplan_details {
            let s = convertDateFormat(inputDate: item.expire_on)
            if s >= self.addProAPI.currentDeviceDate {
                returntype = item.planid
                for sub in addProAPI.subscription_plan {
                    if item.planid == sub.plan_id {
                        returntype = sub.plan_name ?? ""
                    }
                }
                break
            }
        }
        return returntype
    }
    
    
    func checkPlanLastDate() -> Date {
        var planLastDate = self.addProAPI.currentDeviceDate
        for item in self.addProAPI.myplan_details {
            let s = convertDateFormat(inputDate: item.expire_on)
            if s > planLastDate {
                planLastDate = s
            }
        }
        return planLastDate
    }
    
    func buy(index:Int)  {
        guard let dict = addProAPI.subscription_plan[index].plan_id else {
            return
        }
        
        guard let apple_product_id = addProAPI.subscription_plan[index].apple_product_id else {
            return
        }
        if dict != "1" && dict != "2" && dict != "3" && dict != "4" {
            self.AlertControllerOnr(title: alertTitle.alert_alert, message: "Invalid plan, Please contact to admin")
            return
        }
        
        
        
        let x = InAppPurchase.sharedInstance
        x.selectionActionPurchased = { [unowned self] (message, item) in
            print(item as Any,item?.payment.productIdentifier as Any)
            let name = item?.transactionIdentifier
            let productIdenti = item?.payment.productIdentifier
            let data = item?.payment
            print("jhhhhhhhhhh \(String(describing: name))    \(String(describing: productIdenti)) " ,data as Any)
            DispatchQueue.main.async {
                CommonFunctions.hideActivityLoader()
            }
            var previous = UserDefaults.standard.object(forKey: "previous_arr") as? [String] ?? []
            previous.append(dict)
            UserDefaults.standard.set(previous, forKey: "previous_arr")
            let lastDate = checkPlanLastDate()
            print(lastDate)
            if dict == "1" {
                let modifiedDate = Calendar.current.date(byAdding: .month, value: 1, to: lastDate)!
                let lastD = self.convertDateToString(inputDate: modifiedDate)
                self.addProAPI.set_subscription_plan(planid: dict, tx_id: productIdenti ?? name ?? "", check: "1", expire_on: lastD)
            }
            if dict == "2" {
                let modifiedDate = Calendar.current.date(byAdding: .month, value: 3, to: lastDate)!
                let lastD = self.convertDateToString(inputDate: modifiedDate)
                self.addProAPI.set_subscription_plan(planid: dict, tx_id: productIdenti ?? name ?? "", check: "1", expire_on: lastD)
            }
            if dict == "3" {
                let modifiedDate = Calendar.current.date(byAdding: .month, value: 6, to: lastDate)!
                let lastD = self.convertDateToString(inputDate: modifiedDate)
                self.addProAPI.set_subscription_plan(planid: dict, tx_id: productIdenti ?? name ?? "", check: "1", expire_on: lastD)
            }
            if dict == "4" {
                let modifiedDate = Calendar.current.date(byAdding: .year, value: 1, to: lastDate)!
                let lastD = self.convertDateToString(inputDate: modifiedDate)
                self.addProAPI.set_subscription_plan(planid: dict, tx_id: productIdenti ?? name ?? "", check: "1", expire_on: lastD)
            }
//            self.addProAPI.set_subscription_plan(planid: dict, tx_id: productIdenti ?? name ?? "", check: "1", expire_on: "")
//            self.receiptValidation()
        }
        
        x.selectionActionFailed = { [unowned self] (message, item) in
            print(item as Any,item?.payment.productIdentifier as Any)
            DispatchQueue.main.async {
//                self.loading.hideActivityLoading(uiView: self.view)
                CommonFunctions.hideActivityLoader()
//                self.AlertControllerOnr(title: "Error", message: message, BtnTitle: "OK")
            }
        }
        x.selectionActionRestored = { [unowned self] (message, item) in
            print(item as Any,item?.payment.productIdentifier as Any)
            DispatchQueue.main.async {
//                self.loading.hideActivityLoading(uiView: self.view)
                CommonFunctions.hideActivityLoader()
            }
        }
        x.selectionActionError = { [unowned self] (message, item) in
            print(item as Any,item?.payment.productIdentifier as Any)
            DispatchQueue.main.async {
//                self.loading.hideActivityLoading(uiView: self.view)
                CommonFunctions.hideActivityLoader()
                self.AlertControllerOnr(title: alertTitle.alert_error, message: message, BtnTitle: "OK")
            }
        }
        x.selectionActionNoProduct = { [unowned self] (message, item) in
            print(item as Any,item?.payment.productIdentifier as Any)
//            self.loading.hideActivityLoading(uiView: self.view)
            CommonFunctions.hideActivityLoader()
            self.AlertControllerOnr(title: "Error", message: message, BtnTitle: "OK")
        }
        
        InAppPurchase.sharedInstance.buyPlayCoines(ProductId:apple_product_id)
//        InAppPurchase.sharedInstance.buyPlayCoines(ProductId: "Yesitlabs.Monthly")
        
        CommonFunctions.showActivityLoader()
        return
        if index%4 == 0 {
            x.buyUnlockTestInAppPurchase1()
//            x.buyTestInAppPurchasetestRestore()
//            InAppPurchase.sharedInstance.buyPlayCoines(ProductId:x.InAppPurchase1Monthly)
            CommonFunctions.showActivityLoader()
        }
        if index%4 == 1 {
            x.buyUnlockTestInAppPurchase2()
            CommonFunctions.showActivityLoader()
        }
        if index%4 == 2 {
            x.buyUnlockTestInAppPurchase3()
            CommonFunctions.showActivityLoader()
        }
        if index%4 == 3 {
            x.buyUnlockTestInAppPurchase4()
            CommonFunctions.showActivityLoader()
        }
    }
    func receiptValidation() {

        let receiptFileURL = Bundle.main.appStoreReceiptURL
        let receiptData = try? Data(contentsOf: receiptFileURL!)
        let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "d8a21a2efe2e4d6e9482c7be996a79d0" as AnyObject]
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "ab59642069a0471bacf8ab1363f76bd6" as AnyObject]

        do {
            let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
            let storeURL = URL(string: verifyReceiptURL)!
            var storeRequest = URLRequest(url: storeURL)
            storeRequest.httpMethod = "POST"
            storeRequest.httpBody = requestData

            let session = URLSession(configuration: URLSessionConfiguration.default)
            let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
                guard let data = data else {
                    return
                }
                do {
                    let jsonResponse = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers)
                    print(jsonResponse)
                    if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
                        print(date)
                        let d = self?.convertDateToString(inputDate: date)
                        
                    }else{
                        print("Sorry server not respond.")
                    }
                } catch let parseError {
                    print(parseError)
                }
            })
            task.resume()
        } catch let parseError {
            print(parseError)
        }
    }
    func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
        if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
            let lastReceipt = receiptInfo.lastObject as! NSDictionary
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
            if let expiresDate = lastReceipt["expires_date"] as? String {
                return formatter.date(from: expiresDate)
            }
            return nil
        }
        else {
            return nil
        }
    }
}

struct strPlanList {
    var strPlanPrice : String?
    var strPlanName : String?
    var imgBack : UIImage?
    var strPlanType : String?
}
