//
//  ReportPopupVC.swift
//  Karaoke
//
//  Created by Ankur  on 17/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class ReportPopupVC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    @IBOutlet weak var txtCustom:UITextView!
    @IBOutlet weak var lbl_title:UILabel!
    
    var status:Int = 0
    var followers_id = ""
    var video_id = ""
    let arr = ["Inappropriate Content","CopyrightInfringement","Mislabeled Content","Custom"]
    var selectedIndex = IndexPath(item: -1, section: 0)
    let addRepory = ReportPopupVM()
    typealias updateStatus = (Bool,String) -> Void
    var action:updateStatus?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if status == 0 {
            lbl_title.text = "Report Song"
        }else if status == 1{
            lbl_title.text = "Report User"
        }else{
            lbl_title.text = "Report Post"
        }
        addRepory.vc = self
        setUp()
    }
    
    func setUp()  {
        
        table.delegate = self
        table.dataSource = self
//        table.isScrollEnabled = false
        table.allowsSelection = true
        
    }
    
    @IBAction func btn_Submit(_ sender: UIButton){
        if selectedIndex != IndexPath(item: -1, section: 0) {
            if status == 0 {
                self.dismiss(animated: true) {
                    self.action?(true,"")
                }
            }else if status == 1{
                if selectedIndex.item == 3 {
                    addRepory.report_user(comment: txtCustom.text!, reported_userid: followers_id)
                }else {
                    addRepory.report_user(comment: arr[selectedIndex.row], reported_userid: followers_id)
                }
            }else{
                if selectedIndex.item == 3 {
                    addRepory.report_post(comment: txtCustom.text!, followers_id: followers_id, video_id: video_id)
                }else {
                    addRepory.report_post(comment: arr[selectedIndex.row], followers_id: followers_id, video_id: video_id)
                }
            }
        }
    }
    func updateViewData()  {
        
        self.dismiss(animated: true) {
            self.action?(true,"")
        }
    }
    @IBAction func btn_Close(_ sender: UIButton){
        self.dismiss(animated: true) {
            self.action?(true,"")
        }
    }
}

extension ReportPopupVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "ReportCell", for: indexPath) as! ReportCell
        cell.lblTxt.text = arr[indexPath.row]
        cell.img_tick.image = #imageLiteral(resourceName: "unchecked")
        if selectedIndex == indexPath {
            cell.img_tick.image = #imageLiteral(resourceName: "checked")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndex = indexPath
        table.reloadData()
        if (indexPath.row + 1) == arr.count {
            txtCustom.text = ""
            txtCustom.isEditable = true
        }else{
            txtCustom.isEditable = false
        }
    }
}
