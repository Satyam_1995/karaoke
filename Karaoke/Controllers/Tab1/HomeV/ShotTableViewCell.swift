//
//  ShotTableViewCell.swift
//  AutoPlayVideo
//
//  Created by Ashish Singh on 7/21/17.
//  Copyright © 2017 Ashish. All rights reserved.
//

import UIKit
import AVFoundation
class ShotTableViewCell: UITableViewCell, ASAutoPlayVideoLayerContainer {
    @IBOutlet var shotImageView: UIImageView!
    
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    @IBOutlet weak var btn_follow:UIButton!
    @IBOutlet weak var btn_Menu:UIButton!
    @IBOutlet weak var imgUser:UIImageView!
    @IBOutlet weak var lblUserName:UILabel!
    @IBOutlet weak var btnProfile:UIButton!
    @IBOutlet weak var bottomView:HomeTab!
    @IBOutlet weak var imgPlayPaush:UIImageView!
    
    typealias selectAction = (IndexPath?) -> Void
    var tabActionDetail : selectAction?
    var tabActionLike : selectAction?
    var liked = false
    
//    var playerController: ASVideoPlayerController?
    var videoLayer: AVPlayerLayer = AVPlayerLayer()
    var videoURL: String? {
        didSet {
            if let videoURL = videoURL {
                ASVideoPlayerController.sharedVideoPlayer.setupVideoFor(url: videoURL)
            }
            videoLayer.isHidden = videoURL == nil
            
        }
    }
    
    
    var indexPath : IndexPath?
    var cellData : Video_list? {
        didSet {
            updateDat()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        shotImageView.layer.cornerRadius = 5
        shotImageView.backgroundColor = UIColor.gray.withAlphaComponent(0.7)
        shotImageView.clipsToBounds = true
        shotImageView.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor
        shotImageView.layer.borderWidth = 0.5
//        videoLayer.backgroundColor = UIColor.clear.cgColor
        videoLayer.backgroundColor = #colorLiteral(red: 0.2605174184, green: 0.2605243921, blue: 0.260520637, alpha: 1)
        videoLayer.isHidden = true
//        videoLayer.videoGravity = AVLayerVideoGravity.resize
        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspect
//        videoLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        shotImageView.layer.addSublayer(videoLayer)
        selectionStyle = .none
        self.setUp()
    }
    
    func configureCell(imageUrl: String?,
                       description: String,
                       videoUrl: String?) {
//        self.shotImageView.imageURL = imageUrl
        if let img = imageUrl?.convert_to_url {
            self.shotImageView.af.setImage(withURL: img)
        }
        self.videoURL = videoUrl
        #if targetEnvironment(simulator)
//        self.videoURL = videoUrl
        #else
//        self.videoURL = videoUrl
        #endif
//        self.videoURL = videoUrl
    }

    override func prepareForReuse() {
        shotImageView.imageURL = nil
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let horizontalMargin: CGFloat = 20
        let width: CGFloat = bounds.size.width - horizontalMargin * 2
        let height: CGFloat = (width * 0.9).rounded(.up)
//        videoLayer.frame = CGRect(x: 0, y: 0, width: width, height: height)
        videoLayer.frame = CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height)
    }
    
    func visibleVideoHeight() -> CGFloat {
        let videoFrameInParentSuperView: CGRect? = self.superview?.superview?.convert(shotImageView.frame, from: shotImageView)
        guard let videoFrame = videoFrameInParentSuperView,
            let superViewFrame = superview?.frame else {
             return 0
        }
        let visibleVideoFrame = videoFrame.intersection(superViewFrame)
        return visibleVideoFrame.size.height
    }
}
extension ShotTableViewCell {
    
    func setUp()  {
        let s = UIViewController().topbarHeight2
//        bottomCons.constant = s + 20 + s
        bottomCons.constant = s * 1.4
        self.btn_follow.setTitle("Follow", for: .normal)
        self.btn_follow.setTitle("Following", for: .selected)
        self.btn_follow.setTitleColor(.white, for: .selected)
        
        let pauseGesture = UITapGestureRecognizer(target: self, action: #selector(handlePause))
        self.contentView.addGestureRecognizer(pauseGesture)

        let likeDoubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleLikeGesture(sender:)))
        likeDoubleTapGesture.numberOfTapsRequired = 2
        self.contentView.addGestureRecognizer(likeDoubleTapGesture)
        pauseGesture.require(toFail: likeDoubleTapGesture)
        
    }
    func updateDat()  {
        liked = false
        guard let dict = cellData else {
            return
        }
        if dict.user_like_status != 1 {
            bottomView.imgLike.image = #imageLiteral(resourceName: "heartW")
            liked = false
        }else{
            bottomView.imgLike.image = #imageLiteral(resourceName: "heartR")
            liked = true
        }
        if dict.user_fav_status != 1 {
            bottomView.imgFavorite.image = #imageLiteral(resourceName: "fav")
        }else{
            bottomView.imgFavorite.image = #imageLiteral(resourceName: "favR")
        }
        self.lblUserName.text = dict.user_name ?? ""
        if let url = dict.user_profile?.convert_to_url {
            self.imgUser.af.setImage(withURL: url)
        }
//        if let url = dict.video_thumb?.convert_to_url {
//            self.thumbnil.af.setImage(withURL: url)
//        }
        self.bottomView.updateLike = dict.total_likes ?? ""
        self.bottomView.updateComment = dict.total_comments ?? ""
        if dict.user_follow_status != 0 {
            self.btn_follow.isHidden = true
        }else{
            self.btn_follow.isHidden = false
        }
    }
    @objc func handlePause(){
        self.tabActionDetail?(indexPath)
        if let aPlayer = self.videoLayer.player {
            if aPlayer.timeControlStatus == .playing {
                self.videoLayer.player?.pause()
                self.imgPlayPaush.image = #imageLiteral(resourceName: "loginPlay")
                self.imgPlayPaush.alpha = 1
                UIView.animate(withDuration: 1) {
                    self.imgPlayPaush.alpha = 0
                }
            } else if aPlayer.timeControlStatus == .paused {
                self.videoLayer.player?.play()
                self.imgPlayPaush.image = #imageLiteral(resourceName: "play3")
                self.imgPlayPaush.alpha = 1
                UIView.animate(withDuration: 1) {
                    self.imgPlayPaush.alpha = 0
                }
            }
        }
    }
    // Heart Animation with random angle
    @objc func handleLikeGesture(sender: UITapGestureRecognizer){
        let location = sender.location(in: self)
//        let heartView = UIImageView(image: UIImage(systemName: "heart.fill"))
        let heartView = UIImageView(image: #imageLiteral(resourceName: "heartR"))
        heartView.tintColor = .red
        let width : CGFloat = 80
        heartView.contentMode = .scaleAspectFit
        heartView.frame = CGRect(x: location.x - width / 2, y: location.y - width / 2, width: width, height: width)
        heartView.transform = CGAffineTransform(rotationAngle: CGFloat.random(in: -CGFloat.pi * 0.2...CGFloat.pi * 0.2))
        self.contentView.addSubview(heartView)
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 3, options: [.curveEaseInOut], animations: {
            heartView.transform = heartView.transform.scaledBy(x: 0.85, y: 0.85)
        }, completion: { _ in
            UIView.animate(withDuration: 0.4, delay: 0.1, usingSpringWithDamping: 0.8, initialSpringVelocity: 3, options: [.curveEaseInOut], animations: {
                heartView.transform = heartView.transform.scaledBy(x: 2.3, y: 2.3)
                heartView.alpha = 0
            }, completion: { _ in
                heartView.removeFromSuperview()
            })
        })
        
        likeVideo()
    }
    @objc func likeVideo(){
        guard let dict = cellData else {
            return
        }
//        if dict.user_like_status != 1 {
            if !liked {
                liked = true
                self.tabActionLike?(indexPath)
            }
//        }
    }
}
