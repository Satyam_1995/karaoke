//
//  CommentVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class CommentVC: UIViewController {

    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var table: UITableView!
    @IBOutlet weak var txtCommect : UITextField!
    let identifire1 = "FirstConnectCell"
    let identifire2 = "ReplyCommentCell"
    let identifire3 = "TextCommectCell"
    
    var video_id = "3"
    var addCommect = -1
    let viewModel  = CommentVM()
    var final_comment : [All_comment] = []
    typealias updateStatus = (Bool,String) -> Void
    var status:updateStatus?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.vc = self
        viewModel.get_comment_list(video_id: video_id)
        swipe()
        self.setUp()
//        let l = [SubCommentList(name: "a1", msg: "hi"),
//                 SubCommentList(name: "a2", msg: "hi22"),
//                 SubCommentList(name: "a3", msg: "hi3333"),
//                 SubCommentList(name: "a44444", msg: "hi44444")]
    }
   
    @IBAction func btnTouchOutside(_ sender:UIButton) {
//       self.navigationController?.popViewController(animated: false)
        self.view.endEditing(true)
        self.dismiss(animated: true) {
            self.status?(true,"dismiss")
        }
    }
    func setUp()  {
        table.register(UINib(nibName: identifire1, bundle: nil), forCellReuseIdentifier: identifire1)
        table.register(UINib(nibName: identifire2, bundle: nil), forCellReuseIdentifier: identifire2)
        table.register(UINib(nibName: identifire3, bundle: nil), forCellReuseIdentifier: identifire3)
        table.delegate = self
        table.dataSource = self
    }
    @IBAction func btnNewCommect(_ sender:UIButton) {
        if txtCommect.text != "" {
            self.viewModel.post_comment(video_id: video_id, comment: txtCommect.text!)
            self.table.reloadData()
            txtCommect.text = ""
        }
    }
    func submitReplayComment(msg:String, indexPath:IndexPath)  {
        let dict = final_comment[indexPath.section].comments_id ?? ""
        self.viewModel.post_comment_reply(video_id: video_id, comment: msg, comment_id: dict)
        self.table.reloadData()
    }
    func updateData() {
        guard let result = viewModel.result else {
            return
        }
        let user_comment = result.user_comment ?? []
        let all_comment = result.all_comment ?? []
        var a : [All_comment] = []
        
  
        
        if user_comment.count > 0 {
            a.append(contentsOf: user_comment)
        }
        if all_comment.count > 0 {
            a.append(contentsOf: all_comment)
        }
        var s = a.uniques(by: \.comments_id!)
        for item in 0..<s.count {
            s[item].reply_on_comment = s[item].reply_on_comment.reversed()
        }
        final_comment = s
        self.table.reloadData()
    }
    func updateComment() {
        viewModel.get_comment_list(video_id: video_id)
    }
    func updateLike() {
        viewModel.get_comment_list(video_id: video_id)
    }
    func updateReplayLike() {
        viewModel.get_comment_list(video_id: video_id)
    }
}
extension CommentVC : UITableViewDelegate,UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return final_comment.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if addCommect == section {
            return final_comment[section].reply_on_comment.count + 1
        }
        return final_comment[section].reply_on_comment.count
        

    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if final_comment[indexPath.section].reply_on_comment.count <= indexPath.item {
            let cell = tableView.dequeueReusableCell(withIdentifier: identifire3) as! TextCommectCell
            cell.delegate = { [weak self] (msg) in
                print(msg,self as Any)
                self?.submitReplayComment(msg: msg, indexPath: indexPath)
            }
            return cell
        }else{
            let dict = final_comment[indexPath.section].reply_on_comment[indexPath.item]
            let cell = tableView.dequeueReusableCell(withIdentifier: identifire2, for: indexPath) as! ReplyCommentCell
            cell.btnLike.tag = indexPath.item
            cell.btnLike.imageView!.tag = indexPath.section
            cell.btnLike.addTarget(self, action: #selector(self.btnReplyLikeCommect), for: .touchUpInside)
            if dict.reply_like_status == 1 {
                cell.btnLike.setTitleColor(.red, for: .normal)
            }else{
                cell.btnLike.setTitleColor(.black, for: .normal)
            }
            let total_likes = dict.total_likes ?? "0"
            cell.btnLike.setTitle("\(total_likes) Liked", for: .normal)
            cell.lblName.text = dict.user_name
            cell.lblCommect.text = dict.comment ?? ""
            cell.lblTime.text = self.stringToDate(date: dict.created_on ?? "")
//            cell.lblTime.text = self.stringToDate(date: "2020-11-05 10:51:31")
            if let url = dict.user_profile?.convert_to_url {
                cell.img.af.setImage(withURL: url)
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let dict = final_comment[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: identifire1) as! FirstConnectCell
        cell.btnReply.tag = section
        cell.btnReply.addTarget(self, action: #selector(self.btnReply), for: .touchUpInside)
        if dict.reply_on_comment.count > 0 {
            cell.btnReply.setTitleColor(.red, for: .normal)
            cell.btnReply.setTitle("Replied", for: .normal)
        }else{
            cell.btnReply.setTitleColor(.black, for: .normal)
            cell.btnReply.setTitle("Reply", for: .normal)
        }
        
        cell.btnLike.tag = section
        cell.btnLike.addTarget(self, action: #selector(self.btnLikeCommect), for: .touchUpInside)
        if dict.comments_like_status == 1 {
            cell.btnLike.setTitleColor(.red, for: .normal)
        }else{
            cell.btnLike.setTitleColor(.black, for: .normal)
        }
        let total_likes = dict.total_likes ?? "0"
        cell.btnLike.setTitle("\(total_likes) Liked", for: .normal)
        
        cell.lblName.text = dict.user_name ?? ""
        cell.lblCommect.text = dict.comment ?? ""
        cell.lblTime.text = self.stringToDate(date: dict.created_on ?? "")
//        cell.lblTime.text = self.stringToDate(date: "2019-01-01 10:51:31")
        
        if let url = dict.user_profile?.convert_to_url {
            cell.img.af.setImage(withURL: url)
        }
        return cell
    }
    
    @IBAction func btnReply(_ sender:UIButton) {
        addCommect = sender.tag
        self.table.reloadData()
    }
    @IBAction func btnLikeCommect(_ sender:UIButton) {
        let dict = final_comment[sender.tag]
        
        let comments_id = dict.comments_id ?? ""
        let like : like_status = dict.comments_like_status == 1 ? .unLike : .like
//        if like == .like {
//            let currentTitle = self.AddLike(currentTitle: sender.currentTitle ?? "")
//            sender.setTitle("\(currentTitle) Liked", for: .normal)
//        }else{
//            let currentTitle = self.removeLike(currentTitle: sender.currentTitle ?? "")
//            sender.setTitle("\(currentTitle) Liked", for: .normal)
//        }
//
//        let currentTitle = self.AddLike(currentTitle: sender.currentTitle ?? "")
//        sender.setTitle("\(currentTitle) Liked", for: .normal)
        self.viewModel.comment_likes_unlike(like: like, comment_id: comments_id)
        
    }
    @IBAction func btnReplyLikeCommect(_ sender:UIButton) {
        let dict = final_comment[sender.imageView!.tag].reply_on_comment[sender.tag]
        
        let comments_id = dict.reply_id ?? ""
        let like : like_status = dict.reply_like_status == 1 ? .unLike : .like
//        if like == .like {
//            let currentTitle = self.AddLike(currentTitle: sender.currentTitle ?? "")
//            sender.setTitle("\(currentTitle) Liked", for: .normal)
//        }else{
//            let currentTitle = self.removeLike(currentTitle: sender.currentTitle ?? "")
//            sender.setTitle("\(currentTitle) Liked", for: .normal)
//        }
        self.viewModel.comment_reply_likes_unlike(like: like, reply_id: comments_id)
    }
    func AddLike(currentTitle:String) -> Int {
        var updateLike = currentTitle.replacingOccurrences(of: "Liked", with: "")
        updateLike = updateLike.replacingOccurrences(of: "Like", with: "")
        updateLike = updateLike.replacingOccurrences(of: " ", with: "")
        guard var newl = Int(updateLike) else { return 0 }
        newl = newl + 1
        return newl
    }
    func removeLike(currentTitle:String) -> Int {
        var updateLike = currentTitle.replacingOccurrences(of: "Liked", with: "")
        updateLike = updateLike.replacingOccurrences(of: "Like", with: "")
        updateLike = updateLike.replacingOccurrences(of: " ", with: "")
        guard var newl = Int(updateLike) else { return 0 }
        newl = newl - 1
        return newl
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let dict = final_comment[section].comment ?? ""
        let height = dict.heightWithConstrainedWidth(width: self.view.frame.size.width - 85, font: UIFont.systemFont(ofSize: 14))
        let s = height + 60
        return s
//        return 90
    }
}

extension CommentVC  {
    
    func swipe() {
        let down = UISwipeGestureRecognizer(target : self, action : #selector(CommentVC.downSwipe))
        down.direction = .down
        self.commentView.addGestureRecognizer(down)
    }
    
    @objc func downSwipe(){
//        self.navigationController?.popViewController(animated: false)
        self.dismiss(animated: true) {
            self.status?(true,"dismiss")
        }
    }
}

struct CommentList {
    var name : String
    var replay : Bool = false
    var subC : [SubCommentList] = []
}
struct SubCommentList {
    var name : String
    var msg : String
}
