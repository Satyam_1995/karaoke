//
//  OtherUserVideoVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import AVKit


var mainplayer: AVPlayer?
var mainplayerController : AVPlayerViewController?
var mainavPlayerLayer : AVPlayerLayer!


//var preetiVideoView : VideoViewPreeti?

class OtherUserVideoVC: UIViewController {
    
    @IBOutlet weak var bottomView:HomeTab!
    @IBOutlet weak var imageBackground:UIImageView!
//    @IBOutlet weak var playVideo:VideoView!
    @IBOutlet weak var preetiVideoView:VideoViewPreeti!
    
    @IBOutlet weak var lblUserName : UILabel!
    @IBOutlet weak var imgUser : UIImageView!
    
    var videoURL : String?
    var video_id : String? {
        didSet {
            self.viewModel.get_video_details(video_d: video_id!)
        }
    }
    var openCommentView = false
    let viewModel = OtherUserVideoVM()
//    var playerView : PlayerView?
    typealias videoStatus = (String,String) -> Void
    var actionVideo : videoStatus?
    deinit {
        preetiVideoView?.player?.pause()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        viewModel.vc = self
        if nil != video_id {
//            self.viewModel.get_video_details(video_d: video_id!)
        }else{
            self.AlertControllerCuston(title: alertTitle.alert_error, message: "send video id", BtnTitle: ["OK"]) { (_) in
                self.navigationController?.popViewController(animated: true)
            }
        }
        self.setUp()
        self.setUpVideo()
        if openCommentView {
            openCommentView = false
            DispatchQueue.main.async {
                self.btnComment(UIButton())
            }
        }
        imageBackground.image = #imageLiteral(resourceName: "imgBack")
    }
    
    func setUp() {
        bottomView.btnFavorite.addTarget(self, action: #selector(btnFavorite(_:)), for: .touchUpInside)
        bottomView.btnComment.addTarget(self, action: #selector(btnComment(_:)), for: .touchUpInside)
        bottomView.btnSing.addTarget(self, action: #selector(btnSingIt(_:)), for: .touchUpInside)
        bottomView.btnLike.addTarget(self, action: #selector(btnLike(_:)), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        playerView?.player?.play()
        preetiVideoView?.player?.play()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
//        playVideo.stop()
//        playerView?.player?.pause()
//        mainavPlayerLayer?.player?.pause()
        preetiVideoView?.player?.pause()
    }
    func setUpVideo() {
        if nil != self.videoURL {
            self.loadPlayer()
        }
        DispatchQueue.main.async {
            if nil != self.videoURL {
////                self.playVideo.configure(url: self.videoURL!)
//                self.showPlayer(item: self.videoURL!)
//                self.loadPlayer()
            }
        }
    }
    func loadPlayer() {
//        preetiVideoView = VideoViewPreeti(frame: imageBackground.frame)
//        preetiVideoView?.frame = imageBackground.frame
        preetiVideoView?.contentMode = .scaleAspectFill
        preetiVideoView?.player?.isMuted = false
        preetiVideoView?.repeat = .loop
        preetiVideoView?.url = self.videoURL!.convert_to_url!
        preetiVideoView?.player?.play()
//        if preetiVideoView != nil {
//            imageBackground.addSubview(preetiVideoView!)
//        }
//        self.getPlayerPlay(viewFram: self.view, urlStr: self.videoURL!.convert_to_url!) { (_) in
//            self.imageBackground.layer.addSublayer(mainavPlayerLayer)
//            if mainavPlayerLayer != nil {
//                mainavPlayerLayer.player!.play()
//            }
//        }
    }
    func playVideo(url: URL?) {
        guard let url = url else {
            return
        }
        preetiVideoView?.url = self.videoURL!.convert_to_url!
        preetiVideoView?.player?.play()
    }
    
    func stopVideo() {
        preetiVideoView?.player?.pause()
    }
    func showPlayer(item: String) {
//        self.playerView = PlayerView(frame: self.view.frame, item: item)
////        let playerView = PlayerView(frame: self.view.frame, item: item)
//        imageBackground.addSubview(playerView!)
//        DispatchQueue.main.async {
//            self.playerView!.frame = self.view.frame
//            self.playerView!.center = self.view.center
//        }
    }
    func upNewData(new:Bool) {
        if viewModel.videoDetail == nil {
            viewModel.videoDetail = videoDetailCache[video_id!]
        }
        guard let dict = viewModel.videoDetail else {
            self.AlertControllerCuston(title: alertTitle.alert_warning, message: "video deleted", BtnTitle: ["OK"]) { (_) in
                self.actionVideo?("deleted",self.video_id!)
                self.navigationController?.popViewController(animated: true)
            }
            return
        }
        bottomView.updateLike = dict.total_likes?.formatPoints() ?? "0"
        bottomView.updateComment = dict.total_comments?.formatPoints() ?? "0"
        bottomView.imgFavorite.image = dict.user_fav_status == 0 ? #imageLiteral(resourceName: "fav") : #imageLiteral(resourceName: "favR")
        bottomView.imgLike.image = dict.user_like_status == 0 ? #imageLiteral(resourceName: "heartW") : #imageLiteral(resourceName: "heartR")
        if let url = dict.video_thumb?.convert_to_url {
            imageBackground.af.setImage(withURL: url)
        }
        lblUserName.text = dict.user_name
        if let url = dict.user_profile?.convert_to_url {
            imgUser.af.setImage(withURL: url)
        }
        
    }
    @IBAction func btnFavorite(_ sender:UIButton) {
        self.addToFav(btn: bottomView)
//        if bottomView.imgFavorite.image == #imageLiteral(resourceName: "favR") && sender.tag == 0 {
//            bottomView.imgFavorite.image = #imageLiteral(resourceName: "fav")
//            sender.tag = 1
//        }else{
//            AlertControllerOnr(title: nil, message: "Saved to favorite")
//            bottomView.imgFavorite.image = #imageLiteral(resourceName: "favR")
//            sender.tag = 0
//        }
    }
    func addToFav(btn:HomeTab) {
        
        guard let dict = viewModel.videoDetail else { return }
        
        guard let video_id = dict.video_id, video_id != ""  else {
            return
        }
        let likeS = dict.user_fav_status
        var d : favourite_status = .favourite
        if likeS == 1 {
            d = .unfavourite
            viewModel.videoDetail?.user_fav_status = 0
            btn.imgFavorite.image = #imageLiteral(resourceName: "fav")
        }else{
            d = .favourite
            viewModel.videoDetail?.user_fav_status = 1
            btn.imgFavorite.image = #imageLiteral(resourceName: "favR")
        }
        WebServices.submit_favorite_unfavorite_video(video_id: video_id, favourite_status: d) { (json) in
        } successData: { (data) in
            NotificationCenter.default.post(name: NSNotification.Name("removeToFavourite"), object: video_id)
        } failure: { (err) -> (Void) in
            print(err)
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
            if likeS != 1 {
                self.viewModel.videoDetail?.user_fav_status = 0
                btn.imgFavorite.image = #imageLiteral(resourceName: "fav")
            }else{
                self.viewModel.videoDetail?.user_fav_status = 1
                btn.imgFavorite.image = #imageLiteral(resourceName: "favR")
            }
        }
    }
    @IBAction func btnLike(_ sender:UIButton) {
        self.addToLike(btn: bottomView)
//        if bottomView.imgLike.image == #imageLiteral(resourceName: "heartR") && sender.tag == 0 {
//            bottomView.imgLike.image = #imageLiteral(resourceName: "heartW")
//            sender.tag = 1
//        }else{
//            bottomView.imgLike.image = #imageLiteral(resourceName: "heartR")
//            sender.tag = 0
//        }
    }
    func addToLike(btn:HomeTab) {
        guard let dict = viewModel.videoDetail else { return }
        guard let video_id = dict.video_id, video_id != "" else {
            return
        }
        let likeS = dict.user_like_status
        var d : like_status = .like
        if likeS == 1 {
            d = .unLike
            viewModel.videoDetail?.user_like_status = 0
            btn.imgLike.image = #imageLiteral(resourceName: "heartW")
            btn.removeLike()
        }else{
            d = .like
            viewModel.videoDetail?.user_like_status = 1
            btn.imgLike.image = #imageLiteral(resourceName: "heartR")
            btn.AddLike()
        }

        WebServices.submit_post_likes_unlike_video(video_id: video_id, like: d) { (json) in
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err)
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
            if likeS != 1 {
                self.viewModel.videoDetail?.user_like_status = 0
                btn.imgLike.image = #imageLiteral(resourceName: "heartW")
                btn.removeLike()
            }else{
                self.viewModel.videoDetail?.user_like_status = 1
                btn.imgLike.image = #imageLiteral(resourceName: "heartR")
                btn.AddLike()
            }
        }
    }
    @IBAction func btnComment(_ sender:UIButton) {
        let vc = CommentVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.video_id = video_id!
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnSingIt(_ sender:UIButton) {
        
        guard let dict1 = viewModel.videoDetail else {
            return
        }
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnProfile(_ sender:UIButton) {
        guard let friend_id = self.viewModel.videoDetail?.user_id else {
            return
        }
        let vc = OtherUserProfileVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = false
        vc.friend_id =  friend_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_Back(_ sender: UIButton){
        DispatchQueue.main.async {
//            self.playVideo.stop()
            self.navigationController?.popViewController(animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("err")
    }
}

extension UIViewController {
    func getPlayerPlay(viewFram:UIView, urlStr:URL, complition:(Bool) -> Void) {
        if mainavPlayerLayer == nil {
            mainplayer = AVPlayer(url: urlStr)
            mainavPlayerLayer = AVPlayerLayer(player: mainplayer)
            mainavPlayerLayer.videoGravity = .resizeAspect
            mainavPlayerLayer.masksToBounds = true
            mainavPlayerLayer.cornerRadius = 5
            mainavPlayerLayer.frame = viewFram.layer.bounds
        }else{
//            let asset = AVAsset(url: urlStr)
//            let  avPlayerItem = AVPlayerItem(asset: asset)
            let  avPlayerItem = AVPlayerItem(url: urlStr)
            mainplayer?.replaceCurrentItem(with: avPlayerItem)
        }
        complition(true)
    }
    func replaceCurrentItem(urlStr:URL,complition:(Bool) -> Void) {
        let  avPlayerItem = AVPlayerItem(url: urlStr)
        mainplayer?.replaceCurrentItem(with: avPlayerItem)
    }
}
