//
//  UploadedVC.swift
//  Karaoke
//
//  Created by Ankur  on 18/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class UploadedVC: UIViewController {
    
    @IBOutlet weak var PublishView : UIView!
    @IBOutlet weak var UploadedView : UIView!
    @IBOutlet weak var ProgressView : UIView!
    private var token :NSKeyValueObservation?
//    var timer: Timer!
    let loading = LoadingView()
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    let taylor = UploadedVCStatus()
    
    var videoURL : URL!
    var lyrics : String = ""
    var song_name : String = ""
    var music_cat_id : String = ""
    var artist_name : String = ""
    var old_video_id : String = ""
    var video_thumb : Data!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideOtherView(tag: 0)
//        loading.showActivityLoading(uiView: ProgressView)
        loading.showActivityLoading(uiView: self.view)
//        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (a) in
////            self.loading.hideActivityLoading(uiView: self.ProgressView)
//            self.loading.hideActivityLoading(uiView: self.view)
//            self.timer.invalidate()
//            self.hideOtherView(tag: 1)
//        })
        let url = WebServices.EndPoint.upload_video.path
        
        let para : JSONDictionary = [ApiKey.user_id:UserDetail.shared.getUserId(),
                                     ApiKey.lyrics:lyrics,
                                     ApiKey.song_name:song_name,
                                     ApiKey.music_cat_id:music_cat_id,
                                     ApiKey.artist_name: artist_name,
                                     ApiKey.old_video_id:old_video_id]
        var imgPara : [UploadFileParameter] = []
        
//        imgPara.append(UploadFileParameter(fileName: imgName + ".mp4", key: ApiKey.video, data: videoData!, mimeType: ".mp4"))
        let imgName = Date.getCurrentDateForName()
        imgPara.append(UploadFileParameter(fileName: imgName + ".png", key: ApiKey.video_thumb, data: video_thumb!, mimeType: ".png"))
        
        self.uploadData(VideoURLString: videoURL, URLString: url, httpMethod: .post, parameters: para, files: imgPara, headers: [:], loader: false) { (json) in
            self.loading.hideActivityLoading(uiView: self.view)
            self.hideOtherView(tag: 1)
        } successData: { (data) in
        } progress: { (pro) in
            
        } failure: { (err) in
            print(err)
        }

    }
    func uploadData(VideoURLString : URL,
                    URLString : String,
                    httpMethod : HTTPMethod,
                    parameters : JSONDictionary = [:],
                    files : [UploadFileParameter] = [],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    successData : @escaping (Data) -> Void,
                    progress : @escaping (Double) -> Void,
                    failure : @escaping (NSError) -> Void) {
        
//        let fileURL = Bundle.main.url(forResource: "video", withExtension: "mov")!
        let fileURL = VideoURLString
        
        let download = AF.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append(fileURL, withName: ApiKey.video)
            files.forEach({ (fileParamObject) in
                multipartFormData.append(fileParamObject.data, withName: fileParamObject.key, fileName: fileParamObject.fileName, mimeType: fileParamObject.mimeType)
            })
            parameters.forEach({ (paramObject) in
                if let data = (paramObject.value as AnyObject).data(using : String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: paramObject.key)
                }
            })
        }, to: URLString,
        usingThreshold: UInt64.init(),
        method: .post,
        headers: headers,
        interceptor: nil,
        requestModifier: nil).response { (encodingResult) in
            
            do {
                print("===================== METHOD =========================")
                print(httpMethod)
                print("===================== URL STRING =====================")
                print(URLString)
                print("===================== HEADERS ========================")
                print(headers)
                print("===================== PARAMETERS =====================")
                print(parameters)
                print("===================== Image_PARAMETERS =====================")
                print(files)
                print("===================== RESPONSE =======================")
                
                if let err = encodingResult.error{
                    if loader { CommonFunctions.hideActivityLoader() }
                    
                    if (err as NSError).code == NSURLErrorNotConnectedToInternet {
                        NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                    }
                    failure(err as NSError)
                    if let f = encodingResult.data {
                        print("Print Server Error: " + String(data: f, encoding: String.Encoding.utf8)!)
                    }
                    return
                }
                guard encodingResult.data != nil else{
                    print("===================== FAILURE data nil =======================")
                    failure(NSError(code: 0, localizedDescription: ""))
                    return
                }
                successData(encodingResult.data!)
                let value = try JSON(data: encodingResult.data!)
                print(JSON(value))
                success(value)
            }catch {
                print("===================== FAILURE =======================")
                print(error.localizedDescription)
                
                if (error as NSError).code == NSURLErrorNotConnectedToInternet {
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
                failure(error as NSError)
            }
        }.uploadProgress { (progressData) in
            progress(progressData.fractionCompleted)
        }
        token = taylor.observe(\UploadedVCStatus.cancel, options: .new) { person, change in
            if change.newValue == true {
                download.cancel()
            }
        }
    }
    func hideOtherView(tag:Int) {
        PublishView.isHidden = true
        UploadedView.isHidden = true
        if tag == 1{
            UploadedView.isHidden = false
        }else{
            PublishView.isHidden = false
        }
    }
    
    @IBAction func btnCancel(_ sender:UIButton) {
        self.taylor.cancel = true
        self.dismiss(animated: true, completion: {
            self.select!("s",0)
        })
    }
    
    @IBAction func btnOK(_ sender:UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",1)
        })
    }
}
@objc class UploadedVCStatus: NSObject {
    @objc dynamic var cancel = false
}
