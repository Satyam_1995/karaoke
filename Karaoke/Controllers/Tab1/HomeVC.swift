//
//  HomeVC.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import DropDown
import CoreLocation
import Firebase


class HomeVC: UIViewController {
    
    @IBOutlet weak var bottomConsColl: NSLayoutConstraint!
    @IBOutlet weak var bottomCons: NSLayoutConstraint!
    
    @IBOutlet weak var heightCons: NSLayoutConstraint!
    
    @IBOutlet var tableView: UITableView!
    let shotTableViewCellIdentifier = "ShotTableViewCell"
    let loadingCellTableViewCellCellIdentifier = "LoadingCellTableViewCell"
    var shouldShowLoadingCell = false
    
    
    @IBOutlet weak var collect:UICollectionView!
    let loading1 = indicator()
    let loading = LoadingView()
    var refreshControl = UIRefreshControl()
    let homeViewModel = HomeVM()
    var location : (latitude:Double, longitude:Double) = (latitude:17.4862083, longitude:-62.9708284)
    let user_id = UserDetail.shared.getUserId()
    
    //MARK: - DropDown's
    let taylor = VideoPlayKVO()
    private var token :NSKeyValueObservation?
    
    var visableViewCon = true
    var refreshStatus = true
    let menuDrop = DropDown()
    @objc dynamic var currentIndex = 0
    var oldAndNewIndices = (0,0)
    
    var timerPlayV : Timer?
    
    @IBOutlet var btnSelect:[UIButton]!
    @IBOutlet weak var viewBottom:UIView!
    @IBOutlet weak var imgBottom:UIImageView!
    
    @IBAction func btnBottomAction(_ sender:UIButton) {
        if sender.tag == 0 {
            print("reloadData______reloadData")
//            if visableViewCon == true && refreshStatus == false {
//                self.refreshStatus = true
//                self.pauseVideos2()
//                self.loading.showActivityLoading(uiView: self.view)
//                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (a) in
//                    self.loading.hideActivityLoading(uiView: self.view)
//                    self.homeViewModel.get_home_data(lat: "\(self.location.latitude)", long: "\(self.location.longitude)")
//                }
//            }
        }else{
            self.tabBarController?.selectedIndex = sender.tag
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        homeViewModel.vc = self
        if UIDevice.current.hasNotch {
            let ss = self.iScreenSizes()
            tabBarController!.tabBar.frame = CGRect(x: 10, y: ss - 30, width: tabBarController!.tabBar.frame.size.width, height: 80.0)
        }
        tabBarController?.delegate = self
//        self.tabBarController?.tabBar.isHidden = true
        Setup()
        self.loadTable()
//        if let countryCode = (Locale.current as NSLocale).object(forKey: .countryCode) as? String {
//            print(countryCode)
//            if let name = (Locale.current as NSLocale).displayName(forKey: .countryCode, value: countryCode) {
//                print("Country name was found  ",name)
//                CLGeocoder().geocodeAddressString(name) { (placemarks, error) in
//                    if let placemarks = placemarks,let location = placemarks.first?.location?.coordinate {
//                        self.location.latitude = location.latitude
//                        self.location.longitude = location.longitude
////                        self.homeViewModel.get_home_data(lat: "\(location.latitude)", long: "\(location.longitude)")
//                    }else{
//                        print("Country name cannot be found")
//                    }
//                }
//            } else {
//                print("Country name cannot be found")
//            }
//        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("hideView"), object: nil)
        
        self.location.latitude = 17.4862083
        self.location.longitude = -62.9708284
        self.homeViewModel.get_home_data(lat: "\(location.latitude)", long: "\(location.longitude)")
        
//        self.bottomConsColl.constant = self.topbarHeight
//        self.bottomCons.constant = -(self.topbarHeight)
        
        token = taylor.observe(\VideoPlayKVO.cell, options: [.new, .old]) { person, change in
            if let old = change.oldValue as? HomeDataCell {
                DispatchQueue.main.async {
                    old.playVideo.stop()
                }
            }
            if let new = change.newValue as? HomeDataCell {
                DispatchQueue.main.async {
                    new.playVideo.isLoop = true
                    if let di = new.cellData, let url = di.video_file?.convert_to_string {
//                        new.playVideo.configure(url: url, autoPlay: true)
                    }
                }
            }
        }
        getTime()
        removeToFavourite123()
        changeNotificationImage()
    }
    @objc func methodOfReceivedNotification(notification: Notification) {
//         self.tabBarController?.tabBar.isHidden = true
        }
    func removeToFavourite123() {
        NotificationCenter.default.addObserver(forName: NSNotification.Name("removeToFavourite"), object: nil, queue: .main) { (noti) in
            print("removeToFavourite")
            guard let obj = noti.object as? String else {
                return
            }
            for index in 0..<self.homeViewModel.video_list.count {
                let item = self.homeViewModel.video_list[index]
                if item.video_id == obj {
                    if self.homeViewModel.video_list[index].user_fav_status == 1 {
                        self.homeViewModel.video_list[index].user_fav_status = 0
                    }else{
                        self.homeViewModel.video_list[index].user_fav_status = 1
                    }
//                    self.tableView.reloadRows(at: [IndexPath(item: index, section: 0)], with: .automatic)
                    break
                }
            }
        }
//        NotificationCenter.default.addObserver(self, selector: #selector(removeToFavourite(_:)), name: NSNotification.Name("removeToFavourite"), object: nil)
    }
    func loadTable() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isPagingEnabled = true
//        tableView.estimatedRowHeight = 100
//        tableView.rowHeight = UITableView.automaticDimension
        var cellNib = UINib(nibName:shotTableViewCellIdentifier, bundle: nil)
        self.tableView.register(cellNib, forCellReuseIdentifier: shotTableViewCellIdentifier)
        cellNib = UINib(nibName:loadingCellTableViewCellCellIdentifier, bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: loadingCellTableViewCellCellIdentifier)
        tableView.separatorStyle = .none
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(self.appEnteredFromBackground),
                                               name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    func Setup(){
        self.navigationController?.isNavigationBarHidden = true
        tabBarController?.delegate = self
        
//    refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
//        collect.delegate = self
//        collect.dataSource = self
        collect.register(UINib(nibName: "HomeDataCell", bundle: nil), forCellWithReuseIdentifier: "HomeDataCell")
        collect.isPagingEnabled = true
    }
    func updatedata(new:Bool) {
//        collect.reloadData()
        DispatchQueue.main.async {
            let height = self.iScreenSizes()
            self.heightCons.constant = height
            self.tableView.reloadData()
            Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false) { (_) in
                DispatchQueue.main.async {
                    self.refreshStatus = false
                    if self.visableViewCon == true {
                        self.pausePlayeVideos()
                    }
                }
            }
        }
    }
    func insertNewRowsInTableNode(newPosts: [Video_list]) {
        guard newPosts.count > 0 else {
            return
        }
        let section = 0
        var indexPaths: [IndexPath] = []
        
//        tableView.isPagingEnabled = false
        let total = homeViewModel.video_list.count + newPosts.count
        for row in homeViewModel.video_list.count...total-1 {
            let path = IndexPath(row: row, section: section)
            indexPaths.append(path)
        }
        self.homeViewModel.video_list.append(contentsOf: newPosts)
        DispatchQueue.main.async {
//            self.tableView.beginUpdates()
            self.tableView.insertRows(at: indexPaths, with: .bottom)
//            self.tableView.setContentOffset( CGPoint(x: 0.0, y: 0.0), animated: false)
//            self.tableView.endUpdates()

//            Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (_) in
//                self.tableView.isPagingEnabled = true
//            }
        }
    }
    @IBAction func btn_Menu(_ sender: UIButton){
        self.menuDrop.anchorView = sender
        self.menuDrop.bottomOffset = CGPoint(x: -140, y: sender.bounds.height + 10)
        self.menuDrop.textColor = .black
        self.menuDrop.separatorColor = .clear
        self.menuDrop.selectionBackgroundColor = .clear
        self.menuDrop.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.menuDrop.dataSource.removeAll()
        self.menuDrop.cellHeight = 40
        self.menuDrop.dataSource.append(contentsOf: ["Share This Post","Report This Post","Cancel"])
        self.menuDrop.selectionAction = { [unowned self] (index, item) in
            if index == 0 {
                self.loading.showActivityLoading(uiView: self.view)
                let dict = homeViewModel.video_list[sender.tag]
                SharePost.shared.generateContentLink1(video_name: dict.song_name ?? "Karaoke", video_id: dict.video_id ?? "9", video_url: dict.video_file ?? "", video_image: dict.video_thumb ?? "") { (url) in
                    if url != nil {
                        
                        let activityVC = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                        activityVC.popoverPresentationController?.sourceView = self.view
                        activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                            self.loading.hideActivityLoading(uiView: self.view)
                            if !completed {
                                return
                            }
                        }
                        self.present(activityVC, animated: true, completion: nil)
                    }else{
                        self.loading.hideActivityLoading(uiView: self.view)
                    }
                }
            }else if index == 1{
                let dict = homeViewModel.video_list[sender.tag]
                let vc = ReportPopupVC.instantiate(fromAppStoryboard: .Home)
                vc.status = 2
                vc.followers_id = dict.user_id ?? ""
                vc.video_id = dict.video_id ?? ""
                self.present(vc, animated: true, completion: nil)
                
            }else if index == 2 {
                let vc = LoginVC.instantiate(fromAppStoryboard: .Main)
                UserDetail.shared.removeUserId()
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        self.menuDrop.show()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.viewControllers = [self]

//        self.bottomCons.constant = -(self.topbarHeight)
//        self.bottomConsColl.constant = self.topbarHeight
        if let cell = collect.visibleCells.first as? HomeDataCell {
            cell.play()
//            cell.playVideo.play()
        }
// =============================================
//        self.tabBarController?.tabBar.isHidden = true
        if let ta = self.tabBarController?.tabBar.frame {
            
            if UIDevice.current.hasNotch {
                self.viewBottom.frame = ta
                self.viewBottom.frame.origin.y = ta.origin.y + 7
                let ss = self.iScreenSizes()
                self.viewBottom.frame.origin.y = ss - 30 - 76
                self.viewBottom.frame.origin.x = 10
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        visableViewCon = true
        tabBarController?.delegate = self
        pausePlayeVideos()
//        self.tabBarController?.tabBar.isHidden = true
        if let ta = self.tabBarController?.tabBar.frame {
            self.viewBottom.frame = ta
            self.viewBottom.frame.origin.y = ta.origin.y + 7
            if UIDevice.current.hasNotch {
                let ss = self.iScreenSizes()
                self.viewBottom.frame.origin.y = ss - 30 - 76
                self.viewBottom.frame.origin.x = 10
            }
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let ta = self.tabBarController?.tabBar.frame {
            
            if UIDevice.current.hasNotch {
                self.viewBottom.frame = ta
                self.viewBottom.frame.origin.y = ta.origin.y + 7
                let ss = self.iScreenSizes()
                self.viewBottom.frame.origin.y = ss - 30 - 76
                self.viewBottom.frame.origin.x = 10
            }else{
//                let ss = self.iScreenSizes()
//                self.viewBottom.frame.origin.y = ss - 20 - ta.size.height
//                self.viewBottom.frame.origin.x = 10
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        visableViewCon = false
        tabBarController?.delegate = nil
//        self.taylor.cell = nil
        if let cell = collect.visibleCells.first as? HomeDataCell {
//            cell.playVideo.pause()
            cell.pause()
            
        }
        self.pauseVideos2()
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        visableViewCon = false
        tabBarController?.delegate = nil
    }
    @IBAction func btnFollow(_ sender:UIButton) {
        sender.isHidden = true

        let dict = homeViewModel.video_list[sender.tag]
        guard let friend_id = dict.user_id else {
            return
        }
        if user_id == friend_id {
            CommonFunctions.showToastWithMessage("You can't follow to yourself", displayTime: 2, completion: nil)
            return
        }
        WebServices.follow_unfollow(friend_id: friend_id, follow_type: .follow, loader: false) { (json) in
        } successData: { (data) in
            self.homeViewModel.video_list[sender.tag].user_follow_status = 1
            for item in 0..<self.homeViewModel.video_list.count {
                if (self.homeViewModel.video_list[item].user_id ?? "") == friend_id && sender.tag != item {
                    self.homeViewModel.video_list[item].user_follow_status = 1
//                    self.tableView.reloadRows(at: [IndexPath(item: item, section: 0)], with: .top)
//                    self.collect.reloadItems(at: [IndexPath(item: item, section: 0)])
                }
            }
        } failure: { (err) -> (Void) in
            print(err)
            sender.isHidden = false
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
        }
    }
    
       
    @IBAction func btnProfile(_ sender:UIButton) {
        let dict = homeViewModel.video_list[sender.tag]
        if user_id == dict.user_id! {
            self.tabBarController?.selectedIndex = 4
            return
        }
        guard let friend_id = dict.user_id else { return }
        let vc = OtherUserProfileVC.instantiate(fromAppStoryboard: .Home)
        vc.friend_id = friend_id
        vc.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func addCommect(index:Int) {
        let dict = homeViewModel.video_list[index]
        guard let video_id = dict.video_id else { return }
        let vc = CommentVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.video_id = video_id
        self.present(vc, animated: true, completion: nil)
    }
    @IBAction func btnaddSing(_ sender:UIButton) {
        self.addSignIt(index: sender.tag)
    }
    func addSignIt(index:Int) {
        
        let dict1 = homeViewModel.video_list[index]
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
//        vc.old_video_id = "104"
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
                
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func addToFav(index:Int,btn:HomeTab) {
        let dict = homeViewModel.video_list[index]
        guard let video_id = dict.video_id, video_id != "", let likeS = dict.user_fav_status else {
            return
        }
        var d : favourite_status = .favourite
        if likeS == 1 {
            d = .unfavourite
            homeViewModel.video_list[index].user_fav_status = 0
            btn.imgFavorite.image = #imageLiteral(resourceName: "fav")
        }else{
            d = .favourite
            homeViewModel.video_list[index].user_fav_status = 1
            btn.imgFavorite.image = #imageLiteral(resourceName: "favR")
        }
        WebServices.submit_favorite_unfavorite_video(video_id: video_id, favourite_status: d) { (json) in
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err)
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
            if likeS != 1 {
                self.homeViewModel.video_list[index].user_fav_status = 0
                btn.imgFavorite.image = #imageLiteral(resourceName: "fav")
            }else{
                self.homeViewModel.video_list[index].user_fav_status = 1
                btn.imgFavorite.image = #imageLiteral(resourceName: "favR")
            }
        }
    }
    func addToLike(index:Int,btn:HomeTab) {
        let dict = homeViewModel.video_list[index]
        guard let video_id = dict.video_id, video_id != "", let likeS = dict.user_like_status else {
            return
        }
        var d : like_status = .like
        if likeS == 1 {
            d = .unLike
            homeViewModel.video_list[index].user_like_status = 0
            btn.imgLike.image = #imageLiteral(resourceName: "heartW")
            btn.removeLike()
        }else{
            d = .like
            homeViewModel.video_list[index].user_like_status = 1
            btn.imgLike.image = #imageLiteral(resourceName: "heartR")
            btn.AddLike()
        }
        
        WebServices.submit_post_likes_unlike_video(video_id: video_id, like: d) { (json) in
        } successData: { (data) in
            
        } failure: { (err) -> (Void) in
            print(err)
            CommonFunctions.showToastWithMessage("Server not responding", displayTime: 2) {
            }
            if likeS != 1 {
                self.homeViewModel.video_list[index].user_like_status = 0
                btn.imgLike.image = #imageLiteral(resourceName: "heartW")
                btn.removeLike()
            }else{
                self.homeViewModel.video_list[index].user_like_status = 1
                btn.imgLike.image = #imageLiteral(resourceName: "heartR")
                btn.AddLike()
            }
        }
    }
}

extension HomeVC {
    func moveToVideoDetail(indexPath: IndexPath) {
        let dict = homeViewModel.video_list[indexPath.item]
        let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        if let videoURL =  dict.video_file?.convert_to_string, let v_id = dict.video_id {
            vc.videoURL = videoURL
            vc.video_id = v_id
            vc.actionVideo = { [weak self] (msg,v_id) in
                self?.homeViewModel.video_list.remove(at: indexPath.item)
                self?.collect.reloadData()
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "Something went wrong with this video.")
        }
    }
}
// MARK: - ScrollView Extension
extension HomeVC: UIScrollViewDelegate {
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        if collect == scrollView {
//            let w : Int = Int(scrollView.contentOffset.y / self.view.frame.size.height)
//            timerPlayV?.invalidate()
//            timerPlayV = nil
//            timerPlayV = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: false, block: { (_) in
//            DispatchQueue.main.async {
//                let cell = self.collect.visibleCells.last as? HomeDataCell
////                cell?.replay()
////                self.taylor.cell = cell
////                cell?.playVideo.replay()
//            }
//            })
//        }
//    }
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        let cell = self.collect.cellForItem(at: IndexPath(row: self.currentIndex, section: 0)) as? HomeDataCell
////        self.taylor.cell = cell
//        cell?.replay()
//    }
    
}


extension HomeVC:UITabBarControllerDelegate {
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        let index = tabBarController.selectedIndex
        if index == 0 {
            if visableViewCon == true && refreshStatus == false {
                self.refreshStatus = true
                self.pauseVideos2()
                self.loading.showActivityLoading(uiView: self.view)
                Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { (a) in
                    self.loading.hideActivityLoading(uiView: self.view)
                    self.homeViewModel.get_home_data(lat: "\(self.location.latitude)", long: "\(self.location.longitude)")
                }
                collect.reloadData()
            }
        }
    }
    
    @objc func refresh(sender:AnyObject) {
        self.loading.showActivityLoading(uiView: self.view)
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (a) in
            self.loading.hideActivityLoading(uiView: self.view)
        }
        collect.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print("err")
    }
}

extension HomeVC : UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homeViewModel.video_list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "HomeDataCell", for: indexPath) as! HomeDataCell
        let dict = homeViewModel.video_list[indexPath.item]
        cell.cellData = dict
        cell.configure(post: dict)
        cell.btnProfile.tag = indexPath.item
        cell.btnProfile.addTarget(self, action: #selector(self.btnProfile(_:)), for: .touchUpInside)
        cell.btn_Menu.tag = indexPath.item
        cell.btn_Menu.addTarget(self, action: #selector(self.btn_Menu(_:)), for: .touchUpInside)
        cell.btn_follow.tag = indexPath.item
        cell.btn_follow.addTarget(self, action: #selector(self.btnFollow(_:)), for: .touchUpInside)
        cell.playVideo.updateTimeStatus = { [weak self] (curr,duration) in
            if Int(curr) == (Int(duration) - 1) {
                print("watched_full_video")
            }
            if Int(curr) == Int((Int(duration) - 1)/2) {
                if let v_id = dict.video_id {
                    self?.homeViewModel.video_views(video_id: v_id, complition: { (_) in
                    })
                }
            }
        }
        if user_id == dict.user_id! {
            cell.btn_follow.isHidden = true
        }
        cell.bottomView.tabAction = { [weak self] (type,btn) in
            switch type {
            case .homeTab_likeBtnAction:
                print("homeTab_likeBtnAction")
                self?.addToLike(index: indexPath.item, btn: cell.bottomView)
                break
            case .homeTab_CommentBtnAction:
                print("homeTab_CommentBtnAction")
                self?.addCommect(index: indexPath.item)
                break
            case .homeTab_singItBtnAction:
                cell.pause()
                DispatchQueue.main.async {
                    self?.addSignIt(index:indexPath.item)
                }
                print("homeTab_singItBtnAction")
                break
            case .homeTab_favouriteBtnAction:
                print("homeTab_favouriteBtnAction")
                self?.addToFav(index: indexPath.item, btn: cell.bottomView)
                break
            }
        }
        if let url = dict.video_file?.convert_to_string {
//            cell.playVideo.configure(url: url, autoPlay: false)
        }
        return cell
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.moveToVideoDetail(indexPath: indexPath)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? HomeDataCell {
            cell.pause()
//            cell.playVideo.pause()
        }
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let cell = cell as? HomeDataCell{
            oldAndNewIndices.1 = indexPath.row
            currentIndex = indexPath.row
            cell.pause()
//            cell.playVideo.pause()
        }
    }
    func changeNotificationImage() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("newNotificaton"), object: nil)
        NotificationCenter.default.addObserver(forName: NSNotification.Name("newNotificaton"), object: nil, queue: .main) { (noti) in
            if let notifi = noti.object as? Bool, notifi == true {
                if let tabItems = self.tabBarController?.tabBar.items {
                    // In this case we want to modify the badge number of the third tab:
                    let tabItem = tabItems[1]
                    tabItem.image = #imageLiteral(resourceName: "Notification_2")
                }
//                self.imgBottom.image = #imageLiteral(resourceName: "tab_1")
            }else{
//                self.imgBottom.image = #imageLiteral(resourceName: "tab_2")
                if let tabItems = self.tabBarController?.tabBar.items {
                    // In this case we want to modify the badge number of the third tab:
                    let tabItem = tabItems[1]
                    tabItem.image = #imageLiteral(resourceName: "Notification")
                }
            }
        }
    }
}
