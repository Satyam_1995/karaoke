//
//  MediaPlayerView.swift
//  KD Tiktok-Clone
//
//  Created by Sam Ding on 10/5/20.
//  Copyright © 2020 Kaishan. All rights reserved.
//

import UIKit
import AVFoundation

class MediaPlayerView: UIView {
    
    public var player: AVQueuePlayer!
    public var playerLayer: AVPlayerLayer!
    public var playerItem: AVPlayerItem!
    public var playerLooper: AVPlayerLooper!
   
    deinit {
        player.pause()
        playerItem = nil
    }
    
    init(frame: CGRect, videoURL: URL) {
        super.init(frame: frame)
        self.clipsToBounds = true
        self.layer.cornerRadius = 12.0
        player = AVQueuePlayer()
        playerLayer = AVPlayerLayer(player: player)
        playerItem = AVPlayerItem(url: videoURL)
        playerLooper = AVPlayerLooper(player: player, templateItem: playerItem)
        
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.frame = self.layer.bounds
        playerLayer.cornerRadius = 12.0
        self.layer.addSublayer(playerLayer)
        
        let btn = UIButton()
        btn.frame.size = CGSize(width: 30, height: 30)
        btn.center = self.center
        btn.backgroundColor = .clear
        btn.setImage(#imageLiteral(resourceName: "Explore"), for: .selected)
        btn.setImage(#imageLiteral(resourceName: "play3"), for: .normal)
        btn.addTarget(self, action: #selector(btnPlayPaush(_:)), for: .touchUpInside)
//        self.addSubview(btn)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func play(){
        if player.timeControlStatus != .playing {
            player.play()
        }
    }
    
    func pause(){
        if player.timeControlStatus != .paused {
            player.pause()
        }
    }
    
    func reset(){
        if player.timeControlStatus != .paused {
            player.pause()
        }
        player.seek(to: CMTime.zero)
    }
    
    @IBAction func btnPlayPaush(_ sender:UIButton) {
        if sender.isSelected == true {
            if player.timeControlStatus != .playing {
                player.play()
            }
        }else{
            if player.timeControlStatus != .paused {
                player.pause()
            }
        }
        sender.isSelected = !sender.isSelected
    }
    
    func replaceCurrentItem(urlStr:URL,complition:(Bool) -> Void) {
        let  avPlayerItem = AVPlayerItem(url: urlStr)
        player.replaceCurrentItem(with: avPlayerItem)
    }
}
