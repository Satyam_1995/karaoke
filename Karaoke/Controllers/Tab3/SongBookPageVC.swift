//
//  SongBookPageVC.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SongBookPageVC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    @IBOutlet weak var collect:UICollectionView!
    @IBOutlet weak var txtSearch:UITextField!
    var Status:Int = 0
    var selectedIndex = IndexPath(item: 0, section: 0)
    
    let category_Arr = ["Recommended","Popular","Trending","Top Artist"]
    //    var selectedIndex = -1
    
    let viewModel = SongBookPageVM()
    var page = 0
    var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.vc = self
        Setup()
        if UIDevice.current.hasNotch {
            let ss = self.iScreenSizes()
            tabBarController!.tabBar.frame = CGRect(x: 10, y: ss - 30, width: tabBarController!.tabBar.frame.size.width, height: 80.0)
        }
    }
    func updatedata(new:Bool)  {
        self.table.reloadData()
    }
    func Setup(){
//        collect.delegate = self
//        collect.dataSource = self
        self.table.register(UINib(nibName: "SongsBookCell", bundle: nil), forCellReuseIdentifier: "SongsBookCell")
        table.delegate = self
        table.dataSource = self
    }
    
    
    @IBAction func btn_Back(_ sender: UIButton){
//        tabBarController?.selectedIndex = 0
        self.navigationController?.popViewController(animated: true)
    }
    var timerSearch : Timer?
    @IBAction func textFieldChang(_ textField:UITextField) {
        if textField.text != "" {
            timerSearch?.invalidate()
            timerSearch = nil
            timerSearch = Timer.scheduledTimer(withTimeInterval: 1, repeats: false, block: { (_) in
                self.viewModel.search_song(txt: textField.text!, page: 0, min_range: 0, max_range: 20)
            })
        }else{
            timerSearch?.invalidate()
            timerSearch = nil
            shouldShowLoadingCell = false
            viewModel.search_result = []
            self.table.reloadData()
        }
    }
}

extension SongBookPageVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = viewModel.search_result.count
        return shouldShowLoadingCell ? count + 1 : count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 110
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath) {
            return LoadingCell(style: .default, reuseIdentifier: "loading")
        } else {
            let cell = table.dequeueReusableCell(withIdentifier: "SongsBookCell", for: indexPath) as! SongsBookCell
            let dict = viewModel.search_result[indexPath.row]
            cell.viewCon = self
            cell.updateData = dict
            cell.btn_View.tag = indexPath.row
            cell.btn_Sing.tag = indexPath.row
            cell.btn_share.tag = indexPath.row
            cell.btn_Sing.addTarget(self, action: #selector(btnSing(_:)), for: .touchUpInside)
            cell.btn_View.addTarget(self, action: #selector(btnView(_:)), for: .touchUpInside)
            cell.btn_share.addTarget(self, action: #selector(btnShare(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        getMoreImages(page: page)
    }
    func getMoreImages(page:Int){
        let dict = viewModel.search_result.count + 1
        if dict > 18 {
            self.viewModel.search_song(txt: self.txtSearch.text!, page: 0, min_range: dict, max_range: dict + 4)
        }
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.viewModel.search_result.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict1 = viewModel.search_result[indexPath.row]
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSing(_ sender:UIButton) {
        
        let dict1 = viewModel.search_result[sender.tag]
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnView(_ sender:UIButton) {
        guard let dict1 = viewModel.search_result[sender.tag].video_id else{
            return
        }
        let vc = SongsInfoVC.instantiate(fromAppStoryboard: .Home)
        vc.status = 0
        vc.video_id = dict1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnShare(_ sender:UIButton) {
        let dict = viewModel.search_result[sender.tag]
        let loading = LoadingView()
        loading.showActivityLoading(uiView: self.view,color: .black)
        SharePost.shared.generateContentLink1(video_name: dict.song_name ?? "Karaoke", video_id: dict.video_id ?? "9", video_url: dict.video_file ?? "", video_image: dict.video_thumb ?? "") { (url) in
            if url != nil {
                
                let activityVC = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                    loading.hideActivityLoading(uiView: self.view)
                    if !completed {
                        
                    }
                }
                self.present(activityVC, animated: true, completion: nil)
            }else{
                loading.hideActivityLoading(uiView: self.view)
            }
        }
    }
}

extension SongBookPageVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category_Arr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "CategorySongCell", for: indexPath) as! CategorySongCell
        
        
        cell.lbl_Category.text = self.category_Arr[indexPath.item]
        cell.CategorySelected.isHidden = true
        cell.lbl_Category.textColor = #colorLiteral(red: 0.1250560284, green: 0.1427191794, blue: 0.3960507512, alpha: 1)
        if self.selectedIndex == indexPath {
            cell.CategorySelected.isHidden = false
            cell.lbl_Category.textColor = #colorLiteral(red: 0.9294117647, green: 0.2784313725, blue: 0.3411764706, alpha: 1)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedIndex = indexPath
        collect.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let tt = self.category_Arr[indexPath.item]
        let lbl = UILabel()
        lbl.text = tt
        let pp = lbl.textWidth()
        let h = collect.frame.size.height
        return CGSize(width: pp + 30, height: h)
        
    }
}
