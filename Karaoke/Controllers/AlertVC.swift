//
//  AlertVC.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 19/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import UIKit

class AlertVC: UIViewController {

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblMessage : UILabel!
    @IBOutlet weak var stack : UIStackView!
//    var btnList : [(titleBtn:String, backgroundColor:UIColor,textColor:UIColor,tag:Int)] = []
    var btnList : [AlertVCBtn] = []
    var titleStr : String = ""
    var messageStr : String = ""
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    var animation = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        for item in stack.subviews {
            item.removeFromSuperview()
        }
        btnList.forEach { (item) in
            let btn = UIButton(frame: CGRect(x: 0, y: 0, width: self.stack.frame.width, height: 40))
            btn.setTitle(item.titleBtn, for: .normal)
            btn.backgroundColor = item.backgroundColor
            btn.setTitleColor(item.textColor, for: .normal)
            btn.titleLabel?.font = AppFonts.Poppins_Regular.withSize(17)
            btn.tag = item.tag
            if item.isEnable {
                btn.addTarget(self, action: #selector(self.btnAction(_:)), for: .touchUpInside)
            }
            
            stack.addArrangedSubview(btn)
        }
        stack.translatesAutoresizingMaskIntoConstraints = false

        self.lblTitle.text = titleStr
        self.lblMessage.text = messageStr
        if messageStr == "" {
            self.lblMessage.isHidden = true
        }
        if titleStr == "" {
            self.lblTitle.isHidden = true
        }
    }
    @IBAction func btnAction(_ sender:UIButton) {
        self.dismiss(animated: true) {
            self.select?(sender.currentTitle ?? "",sender.tag)
        }
    }

   

}
struct AlertVCBtn {
    var titleBtn : String
    var backgroundColor : UIColor = .clear
    var textColor : UIColor = .black
    var tag : Int = 0
    var isEnable : Bool = true
}
