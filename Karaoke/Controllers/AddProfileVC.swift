//
//  AddProfileVC.swift
//  Karaoke
//
//  Created by Ankur  on 07/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import DropDown

class AddProfileVC: UIViewController {
    
    @IBOutlet weak var collect:UICollectionView!
    @IBOutlet weak var CreateProfileView:UIView!
    @IBOutlet weak var lbl_name:UILabel!
    @IBOutlet weak var lbl_userName:UILabel!
    @IBOutlet weak var lbl_description:UILabel!
    @IBOutlet weak var img_CreateProfile:UIImageView!
    @IBOutlet weak var img_Profile:UIImageView!
    @IBOutlet weak var btn_CreateProfile:UIButton!
    @IBOutlet weak var lbl_Following:UILabel!
    @IBOutlet weak var lbl_Followers:UILabel!
    @IBOutlet weak var lbl_Favorites:UILabel!
    @IBOutlet weak var lbl_firstCreate:UILabel!
    @IBOutlet weak var btn_Create:UIButton!
    let user_id = UserDetail.shared.getUserId()
    
    let imgArr = [#imageLiteral(resourceName: "i16"),#imageLiteral(resourceName: "i10"),#imageLiteral(resourceName: "i8"),#imageLiteral(resourceName: "i15"),#imageLiteral(resourceName: "i7")]
    var pStatus:Int = 1
    let addProAPI = AddProfileVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Setup()
        let ss = UserDefaults.standard.object(forKey: "start_singing") as? Int ?? 0
        if ss == 1{
//            CreateProfileView.isHidden = true
//            collect.isHidden = false
//            UploadData(str1: "10", str2: "310", str3: "5", strName: "Carry Linkona", strUserName: "@carrykona_12", imgUser: #imageLiteral(resourceName: "i15"), imgCrateProfile: nil, strFirstCreate: "Let's Create Your Profile First!", btnCreat: nil)
//            self.pStatus = 2
        }
        pStatus = 1
        self.updateUI()
        if UIDevice.current.hasNotch {
            let ss = self.iScreenSizes()
            tabBarController!.tabBar.frame = CGRect(x: 10, y: ss - 30, width: tabBarController!.tabBar.frame.size.width, height: 80.0)
        }
        NotificationCenter.default.addObserver(forName: NSNotification.Name("SubscriptionPlanVC"), object: nil, queue: .main) { (noti) in
            self.moveToSubscriptionPlanVC()
        }
    }
    
    func UploadData(str1:String,str2:String,str3:String,strName:String,strUserName:String,imgUser:UIImage,imgCrateProfile:UIImage?,strFirstCreate:String?,btnCreat:String?){
        
        lbl_Following.text = ""
        lbl_Followers.text = ""
        lbl_Favorites.text = ""
        lbl_name.text = ""
        lbl_userName.text = ""
//        img_Profile.image = nil
        img_CreateProfile.image = imgCrateProfile
        lbl_firstCreate.text = strFirstCreate
        btn_Create.setTitle(btnCreat, for: .normal)
        self.setData(new: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.viewControllers = [self]
        tabBarController?.tabBar.isHidden = false
        addProAPI.getData()
//        addProAPI.get_follower_profile()
        addProAPI.get_user_videos()
        
    }
    func moveToSubscriptionPlanVC() {
        let vc = SubscriptionPlanVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    func updateUI() {
        if pStatus == 0 {
            CreateProfileView.isHidden = false
            collect.isHidden = true
            UploadData(str1: "0", str2: "0", str3: "0", strName: "Your Name", strUserName: "@username", imgUser: #imageLiteral(resourceName: "dummyProfile"), imgCrateProfile: #imageLiteral(resourceName: "pro5"), strFirstCreate: "Let's Create Your Profile First!", btnCreat: "Create Profile")
            
        }else if pStatus == 2 {
            let data = addProAPI.Profiledetail?.data?.first?.bio ?? ""
            if data == "" {
                CreateProfileView.isHidden = false
                collect.isHidden = true
                UploadData(str1: "0", str2: "0", str3: "0", strName: "Your Name", strUserName: "@username", imgUser: #imageLiteral(resourceName: "dummyProfile"), imgCrateProfile: #imageLiteral(resourceName: "pro5"), strFirstCreate: "Let's Create Your Profile First!", btnCreat: "Create Profile")
                return
            }
            CreateProfileView.isHidden = false
            collect.isHidden = true
            UploadData(str1: "0", str2: "0", str3: "0", strName: "Your Name", strUserName: "@username", imgUser: #imageLiteral(resourceName: "i15"), imgCrateProfile: #imageLiteral(resourceName: "mic4"), strFirstCreate: "Let's Create Your First Recording!", btnCreat: "Start Singing")
        }else{
            CreateProfileView.isHidden = true
            collect.isHidden = false
            UploadData(str1: "0", str2: "0", str3: "0", strName: "Your Name", strUserName: "@username", imgUser: #imageLiteral(resourceName: "i15"), imgCrateProfile: nil, strFirstCreate: "Let's Create Your Profile First!", btnCreat: nil)
        }
    }
    @IBAction func btn_CreateProfile(_ sender: UIButton){
        if btn_Create.titleLabel?.text == "Create Profile"{
            let vc = EditProfileVC.instantiate(fromAppStoryboard: .Main)
            vc.hidesBottomBarWhenPushed = true
            vc.pStatus = 0
            vc.select = { [weak self] (str,status) in
                if status == 1 {
                    self!.pStatus = 2
                }
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.tabBarController?.selectedIndex = 3
            return
        }
    }
    
    func setData(new:Bool) {
        var data = addProAPI.Profiledetail?.data?.first
        if new == false {
            let sign = singletonClass.createsingleton()
            if let dat = sign?.userProfileData {
                data = dat.data?.first
            }
        }
        guard data != nil else {
            return
        }
        lbl_Following.text = "\(data?.total_following ?? 0)".formatPoints()
        lbl_Followers.text = "\(data?.total_follower ?? 0)".formatPoints()
        lbl_Favorites.text = "\(data?.total_favorite ?? 0)".formatPoints()
        lbl_name.text = data?.name ?? ""
        lbl_userName.text = data?.user_name ?? ""
        lbl_description.text = data?.bio ?? ""
//        img_CreateProfile.image = imgCrateProfile
//        lbl_firstCreate.text = strFirstCreate
//        btn_Create.titleLabel?.text = btnCreat
        if let url = data?.profile_img?.convert_to_url {
            img_Profile.af.setImage(withURL: url)
        }
        DispatchQueue.main.async {
            self.updateUI()
        }
    }
    
    @IBAction func btn_Following(_ sender: UIButton){
        let vc = FollowerVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.status = 1
        vc.follower_id = UserDetail.shared.getUserId()
        vc.fromVC = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Followers(_ sender: UIButton){
        let vc = FollowerVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        vc.status = 0
        vc.follower_id = UserDetail.shared.getUserId()
        vc.fromVC = 0
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btn_Favorites(_ sender: UIButton){
        let vc = FavoriteVC.instantiate(fromAppStoryboard: .Home)
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btn_Search(_ sender: UIButton){
        let vc = SongBookPageVC.instantiate(fromAppStoryboard: .Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
    //MARK: - DropDown's
    
    let menuDrop = DropDown()
    
    @IBAction func btn_Menu(_ sender: UIButton){
        self.menuDrop.anchorView = sender
        self.menuDrop.bottomOffset = CGPoint(x: -140, y: sender.bounds.height + 10)
        self.menuDrop.textColor = .black
        self.menuDrop.separatorColor = .clear
        self.menuDrop.selectionBackgroundColor = .clear
        self.menuDrop.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.menuDrop.dataSource.removeAll()
        self.menuDrop.cellHeight = 40
        self.menuDrop.dataSource.append(contentsOf: ["Edit Profile","Block List","Subscription Plan","Cancel"])
        self.menuDrop.selectionAction = { [unowned self] (index, item) in
            if index == 0 {
                let vc = EditProfileVC.instantiate(fromAppStoryboard: .Main)
                vc.hidesBottomBarWhenPushed = true
                vc.pStatus = 1
                vc.select = { [weak self] (str,status) in
                    if status == 0 {
                        self!.pStatus = 1
                    }
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if index == 1{
                let vc = BlockListVC.instantiate(fromAppStoryboard: .Home)
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else if index == 2{
                let vc = SubscriptionPlanVC.instantiate(fromAppStoryboard: .Home)
                vc.hidesBottomBarWhenPushed = true
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                
            }
        }
        self.menuDrop.show()
    }
    
    func Setup(){
        addProAPI.vc = self
        collect.delegate = self
        collect.dataSource = self
        self.collect.register(UINib(nibName: "VideoCell", bundle: nil), forCellWithReuseIdentifier: "VideoCell")
    }
}

extension AddProfileVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addProAPI.userVideoList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "VideoCell", for: indexPath) as! VideoCell
        let dict = addProAPI.userVideoList[indexPath.item]
        if let img = dict.video_thumb?.convert_to_url {
            cell.imgSong.af.setImage(withURL: img)
        }
        cell.lblLike.text = dict.total_likes ?? "0"
        cell.lblComment.text = dict.total_comments ?? "0"
//        cell.imgSong.image = imgArr[indexPath.item]
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let dict = addProAPI.userVideoList[indexPath.item]
        let vc = UserVideoVC.instantiate(fromAppStoryboard: .Video)
        vc.hidesBottomBarWhenPushed = true
        if let videoURL =  dict.video_file?.convert_to_string, let v_id = dict.video_id {
            vc.videoURL = videoURL
            vc.video_id = v_id
            vc.status = 1
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
            self.AlertControllerOnr(title: alertTitle.alert_error, message: "Something went wrong with this video.")
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collect.frame.size.width/3 - 2.5
        let h = collect.frame.size.height/2.3        
        return CGSize(width: w, height: h)
    }
}


