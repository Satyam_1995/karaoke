//
//  BlockListVC.swift
//  Karaoke
//
//  Created by Ankur  on 19/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class BlockListVC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    @IBOutlet weak var txtSearch : UITextField!
//    var list : [strBlockList] = []
    let viewModel = BlockListVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.vc = self
        viewModel.get_block_list()
        Setup()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.get_block_list()
    }
    func Setup(){
        self.table.register(UINib(nibName: "BlockListCell", bundle: nil), forCellReuseIdentifier: "BlockListCell")
        table.delegate = self
        table.dataSource = self
    }
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func updateData(new:Bool) {
        self.table.reloadData()
    }
}


extension BlockListVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.viewModel.blockList.count
//        return list.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = table.dequeueReusableCell(withIdentifier: "BlockListCell", for: indexPath) as! BlockListCell
        
        let dict = viewModel.blockList[indexPath.item]
        cell.lblUserName.text = dict.name
        cell.lbltotalFollowers.text = "\(dict.total_follower)".formatPoints() + " Followers"
        if let url = dict.user_profile?.convert_to_url {
            cell.imgUSer.af.setImage(withURL: url, placeholderImage: #imageLiteral(resourceName: "ProfileR"), completion:  { (result) in
            })
        }
        cell.btnUnblock.tag = indexPath.row
        cell.btnUnblock.addTarget(self, action: #selector(btnUnblock(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 40))
        headerView.backgroundColor = .clear
        let label = UILabel()
        label.frame = CGRect.init(x: 0, y: 0, width: headerView.frame.width-20, height: headerView.frame.height)
        
        label.text = "\(viewModel.blockList.count) People"
        label.textColor = .darkGray
        label.font = UIFont(name: "Poppins-Medium", size: 10.0)
        label.textAlignment = .left
        
        headerView.addSubview(label)
        
        return headerView
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = viewModel.blockList[indexPath.item]
        guard let friend_id = dict.followerid else { return }
        let vc = OtherUserProfileVC.instantiate(fromAppStoryboard: .Home)
        vc.friend_id = friend_id
        vc.hidesBottomBarWhenPushed = false
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnUnblock(_ sender:UIButton) {
        let dict = viewModel.blockList[sender.tag]
        guard let friend_id = dict.followerid else { return }
        viewModel.block_unblock(friend_id: friend_id)
        viewModel.blockList.remove(at: sender.tag)
        table.reloadData()
    }
    func updateBlockList(new: Bool){
//        self.AlertControllerOnr(title: nil, message: "User unblocked successfully.")
//        self.AlertControllerOnr(title: nil, message: "User blocked successfully.")
    }
}
struct  strBlockList {
    var strUserName : String?
    var strFollowerCount : String?
    var imgUser : UIImage?
}
