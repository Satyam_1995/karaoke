//
//  VideoCompositionWriter.swift
//  TikTok
//
//  Created by Nidhi Kulkarni on 3/13/20.
//  Copyright © 2020 Nidhi Kulkarni. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
import MobileCoreServices

enum VideoError {
    case audio(type: String)
    //    case file(type: Enums.FileError)
    case custom(errorDescription: String?)
}

extension VideoError:Error {
    var errorDescription: String? {
        switch self {
        case .audio(let type) : return type
        case .custom(let errorDescription): return errorDescription
        }
    }
}

class VideoCompositionWriter: NSObject {
    
    static var shared = VideoCompositionWriter()
    
    func merge(arrayVideos:[AVAsset]) -> AVMutableComposition {
        
        let mainComposition = AVMutableComposition()
        let compositionVideoTrack = mainComposition.addMutableTrack(withMediaType: .video, preferredTrackID: kCMPersistentTrackID_Invalid)
        compositionVideoTrack?.preferredTransform = CGAffineTransform(rotationAngle: .pi / 2)
        
        //    let soundtrackTrack = mainComposition.addMutableTrack(withMediaType: .audio, preferredTrackID: kCMPersistentTrackID_Invalid)
        
        var insertTime = CMTime.zero
        
        for videoAsset in arrayVideos {
            if videoAsset.tracks(withMediaType: .video).count > 0 {
                do{
                    try compositionVideoTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .video)[0], at: insertTime)
                    //    try! soundtrackTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: videoAsset.duration), of: videoAsset.tracks(withMediaType: .audio)[0], at: insertTime)
                    
                    insertTime = CMTimeAdd(insertTime, videoAsset.duration)
                }catch {
                    print(error)
                }
            }
        }
        return mainComposition
    }
    
    func mergeAudioVideo(_ documentsDirectory: URL, filename: String, clips: [String], completion: @escaping (Bool, URL?) -> Void) {
        
        
        var assets: [AVAsset] = []
        var totalDuration = CMTime.zero
        
        for clip in clips {
            let videoFile = documentsDirectory.appendingPathComponent(clip)
            let asset = AVURLAsset(url: videoFile)
            assets.append(asset)
            totalDuration = CMTimeAdd(totalDuration, asset.duration)
        }
        
        // 3 - Audio track
        let mixComposition = merge(arrayVideos: assets)
        guard let audioUrl = Bundle.main.url(forResource: "Body_Language", withExtension: "mp3") else { return }
        let loadedAudioAsset = AVURLAsset(url: audioUrl)
        let audioTrack = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: 0)
        do {
            try audioTrack?.insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                            duration: totalDuration),
                                            of: loadedAudioAsset.tracks(withMediaType: AVMediaType.audio)[0] ,
                                            at: CMTime.zero)
        } catch {
            print("Failed to load Audio track")
        }
        
        
        // 4 - Get path
        
        let url = documentsDirectory.appendingPathComponent("out_\(filename)")
        
        // 5 - Create Exporter
        guard let exporter = AVAssetExportSession(asset: mixComposition,
                                                  presetName: AVAssetExportPresetHighestQuality) else {
            return
        }
        exporter.outputURL = url
        exporter.outputFileType = AVFileType.mov
        exporter.shouldOptimizeForNetworkUse = true
        
        // 6 - Perform the Export
        exporter.exportAsynchronously() {
            DispatchQueue.main.async {
                if exporter.status == .completed {
                    completion(true, exporter.outputURL)
                } else {
                    completion(false, nil);
                }
                
            }
        }
        
    }
    func pavan_mergeVideoAndAudio(videoUrl: URL,
                                  //                            audioArrayList: [(url: URL,time:Double)],
                                  audioArrayList: [soundMergeList],
                                  shouldFlipHorizontally: Bool = false,
                                  filename: String,
                                  completion: @escaping (_ error: Error?, _ url: URL?) -> Void) {
        var audioAyyay : [(asses: AVAsset,time:Double)] = []
        for item in audioArrayList {
            audioAyyay.append((asses: AVAsset(url: item.url), time: item.time))
        }
        if audioAyyay.count == 0 {
            completion(NSError(localizedDescription: "send audio list"), nil)
            return
        }
        let mixComposition = AVMutableComposition()
        var mutableCompositionVideoTrack = [AVMutableCompositionTrack]()
        //      var mutableCompositionAudioTrack = [AVMutableCompositionTrack]()
        var mutableCompositionAudioOfVideoTrack = [AVMutableCompositionTrack]()
        
        //start merge
        
        let aVideoAsset = AVAsset(url: videoUrl)
        //      let aAudioAsset = AVAsset(url: audioUrl)
        
        let compositionAddVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                 preferredTrackID: kCMPersistentTrackID_Invalid)
        
        //      let compositionAddAudio = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
        //                                                               preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let compositionAddAudioOfVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                        preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioOfVideoAssetTrack: AVAssetTrack? = aVideoAsset.tracks(withMediaType: AVMediaType.audio).first
        //      let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        // Default must have tranformation
        compositionAddVideo?.preferredTransform = aVideoAssetTrack.preferredTransform
        
        if shouldFlipHorizontally {
            // Flip video horizontally
            var frontalTransform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            frontalTransform = frontalTransform.translatedBy(x: -aVideoAssetTrack.naturalSize.width, y: 0.0)
            frontalTransform = frontalTransform.translatedBy(x: 0.0, y: -aVideoAssetTrack.naturalSize.width)
            compositionAddVideo?.preferredTransform = frontalTransform
        }
        
        mutableCompositionVideoTrack.append(compositionAddVideo!)
        //      mutableCompositionAudioTrack.append(compositionAddAudio!)
        mutableCompositionAudioOfVideoTrack.append(compositionAddAudioOfVideo!)
        
        do {
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                duration: aVideoAssetTrack.timeRange.duration),
                                                                of: aVideoAssetTrack,
                                                                at: CMTime.zero)
            
            for item in audioAyyay {
                var mutableCompositionAudioTrack2 = [AVMutableCompositionTrack]()
                let compositionAddAudio2 = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                          preferredTrackID: kCMPersistentTrackID_Invalid)
                if let aAudioAssetTrack2: AVAssetTrack = item.asses.tracks(withMediaType: AVMediaType.audio).first {
                    //                if let aAudioAssetTrack2: AVAssetTrack = item.asses.tracks(withMediaType: AVMediaType.audio)[0]
                    mutableCompositionAudioTrack2.append(compositionAddAudio2!)
                    let austime = aVideoAssetTrack.timeRange.duration.seconds - aAudioAssetTrack2.timeRange.duration.seconds - 1.5
                    if austime > item.time {
                        try mutableCompositionAudioTrack2[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                             duration: aAudioAssetTrack2.timeRange.duration),
                                                                             of: aAudioAssetTrack2,
                                                                             at: CMTime(seconds: item.time, preferredTimescale: 1))
                    }else{
                        print("notAdd", item.time,"--->",austime)
                    }
                }
            }
            // adding audio (of the video if exists) asset to the final composition
            if let aAudioOfVideoAssetTrack = aAudioOfVideoAssetTrack {
                try mutableCompositionAudioOfVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                           duration: aVideoAssetTrack.timeRange.duration),
                                                                           of: aAudioOfVideoAssetTrack,
                                                                           at: CMTime.zero)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        let savePathUrl: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/\(filename).mp4")
        do { // delete old video
            try FileManager.default.removeItem(at: savePathUrl)
        } catch { print(error.localizedDescription) }
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetMediumQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.outputURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {
            case AVAssetExportSession.Status.completed:
                print("success")
                completion(nil, savePathUrl)
            case AVAssetExportSession.Status.failed:
                print("failed \(assetExport.error?.localizedDescription ?? "error nil")")
                completion(assetExport.error, nil)
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport.error?.localizedDescription ?? "error nil")")
                completion(assetExport.error, nil)
            default:
                print("complete")
                completion(assetExport.error, nil)
            }
        }
    }
    func trimAudio(asset: AVAsset, startTime: Double, stopTime: Double, finished:@escaping (URL) -> ())
    {
        
        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith:asset)
        
        if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
            
            guard let exportSession = AVAssetExportSession(asset: asset,
                                                           presetName: AVAssetExportPresetAppleM4A) else{return}
            
            // Creating new output File url and removing it if already exists.
            let furl: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/trimmedAudio.m4a")
            do { // delete old video
                try FileManager.default.removeItem(at: furl)
            } catch { print(error.localizedDescription) }
            
            exportSession.outputURL = furl
            exportSession.outputFileType = AVFileType.m4a
            
            let start: CMTime = CMTimeMakeWithSeconds(startTime, preferredTimescale: asset.duration.timescale)
            let stop: CMTime = CMTimeMakeWithSeconds(stopTime, preferredTimescale: asset.duration.timescale)
            let range: CMTimeRange = CMTimeRangeFromTimeToTime(start: start, end: stop)
            exportSession.timeRange = range
            
            exportSession.exportAsynchronously(completionHandler: {
                
                switch exportSession.status {
                case .failed:
                    print("Export failed: \(exportSession.error!.localizedDescription)")
                case .cancelled:
                    print("Export canceled")
                default:
                    print("Successfully trimmed audio")
                    DispatchQueue.main.async {
                        //                        let wavFileName: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/trimmedAudio.wav")
                        //                        do { // delete old video
                        //                            try FileManager.default.removeItem(at: wavFileName)
                        //                        } catch { print(error.localizedDescription) }
                        //                        try! FileManager.default.copyItem(at: furl, to: wavFileName)
                        //                        try! FileManager.default.removeItem(at: fileName)
                        finished(furl)
                    }
                }
            })
        }
    }
    func trimAudio2(asset: AVAsset, startTime: Double, stopTime: Double, finished:@escaping (URL) -> ())
    {
        let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality)!
        
        var outputURL = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        let fileManager =  FileManager.default
        do {
            try fileManager.createDirectory(at: outputURL, withIntermediateDirectories: false, attributes: nil)
            outputURL = outputURL.appendingPathComponent("output.m4a")
            
        }catch{
            print(error)
        }
        exportSession.outputURL = URL(fileURLWithPath: "\(outputURL)")
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.outputFileType = .m4a
        
        //        let startTime = CMTimeMake(value: Int64(Int(floor(0 * 100))), timescale: 100)
        //        let stopTime = CMTimeMake(value: Int64(Int(ceil(CMTimeGetSeconds(videoToTrimSecond) * 100))), timescale: 100)
        
        let startTime = CMTimeMake(value: Int64(Int(floor(startTime * 100))), timescale: 100)
        let stopTime = CMTimeMake(value: Int64(Int(floor(stopTime * 100))), timescale: 100)
        let range = CMTimeRangeFromTimeToTime(start: startTime, end: stopTime)
        
        exportSession.timeRange = range
        
        exportSession.exportAsynchronously(completionHandler: {
            switch exportSession.status {
            case .failed:
                print("Export failed: \(exportSession.error != nil ? exportSession.error!.localizedDescription : "No Error Info")")
            case .cancelled:
                print("Export canceled")
            case .completed:
                finished(outputURL)
            default:
                break
            }
            
        })
        
        //        let exportProgressBarTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(updateExportDisplay(_:)), userInfo: nil, repeats: true)
        //
        //        let Progresshud = MBProgressHUD.showAdded(to: view, animated: true)
        //        Progresshud?.mode = MBProgressHUDModeAnnularDeterminate
    }
    @objc func updateExportDisplay(_ timer: Timer?) {
        //        Progresshud.progress = exportSession.progress
        //        Progresshud.label.text = String(format: "%.0f%%", exporter.progress * 100)
    }
    
    
    //    func trimAudio(asset: AVAsset, startTime: Double, stopTime: Double, finished:@escaping (URL) -> ())
    //    {
    //
    //        let compatiblePresets = AVAssetExportSession.exportPresets(compatibleWith:asset)
    //
    //        if compatiblePresets.contains(AVAssetExportPresetMediumQuality) {
    //
    //            guard let exportSession = AVAssetExportSession(asset: asset,
    //                                                           presetName: AVAssetExportPresetAppleM4A) else{return}
    //
    //            // Creating new output File url and removing it if already exists.
    //            let furl: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/trimmedAudio.m4a")
    //            do { // delete old video
    //                try FileManager.default.removeItem(at: furl)
    //            } catch { print(error.localizedDescription) }
    //
    //            exportSession.outputURL = furl
    //            exportSession.outputFileType = AVFileType.m4a
    //
    //            let start: CMTime = CMTimeMakeWithSeconds(startTime, preferredTimescale: asset.duration.timescale)
    //            let stop: CMTime = CMTimeMakeWithSeconds(stopTime, preferredTimescale: asset.duration.timescale)
    //            let range: CMTimeRange = CMTimeRangeFromTimeToTime(start: start, end: stop)
    //            exportSession.timeRange = range
    //
    //            exportSession.exportAsynchronously(completionHandler: {
    //
    //                switch exportSession.status {
    //                case .failed:
    //                    print("Export failed: \(exportSession.error!.localizedDescription)")
    //                case .cancelled:
    //                    print("Export canceled")
    //                default:
    //                    print("Successfully trimmed audio")
    //                    DispatchQueue.main.async {
    ////                        let wavFileName: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/trimmedAudio.wav")
    ////                        do { // delete old video
    ////                            try FileManager.default.removeItem(at: wavFileName)
    ////                        } catch { print(error.localizedDescription) }
    ////                        try! FileManager.default.copyItem(at: furl, to: wavFileName)
    ////                        try! FileManager.default.removeItem(at: fileName)
    //                        finished(furl)
    //                    }
    //                }
    //            })
    //        }
    //    }
    
    func cropVideo(sourceURL1: URL, statTime:Double, endTime:Double, finished:@escaping (URL?) -> ()) {
        let manager = FileManager.default
        
        guard let documentDirectory = try? manager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true) else {return}
        let mediaType = "mp4"
        if mediaType == kUTTypeMovie as String || mediaType == "mp4" as String {
            let asset = AVAsset(url: sourceURL1 as URL)
            let length = Float(asset.duration.value) / Float(asset.duration.timescale)
            print("video length: \(length) seconds")
            
            let start = statTime
            let end = endTime
            
            var outputURL = documentDirectory.appendingPathComponent("cropoutput")
            do {
                try manager.createDirectory(at: outputURL, withIntermediateDirectories: true, attributes: nil)
                outputURL = outputURL.appendingPathComponent("\(UUID().uuidString).\(mediaType)")
            }catch let error {
                print(error)
            }
            
            //Remove existing file
            _ = try? manager.removeItem(at: outputURL)
            
            
            guard let exportSession = AVAssetExportSession(asset: asset, presetName: AVAssetExportPresetHighestQuality) else {return}
            exportSession.outputURL = outputURL
            exportSession.outputFileType = .mp4
            
            let startTime = CMTime(seconds: start, preferredTimescale: 1000)
            let endTime = CMTime(seconds: end, preferredTimescale: 1000)
            let timeRange = CMTimeRange(start: startTime, end: endTime)
            
            exportSession.timeRange = timeRange
            exportSession.exportAsynchronously{
                switch exportSession.status {
                case .completed:
                    print("exported at \(outputURL)")
                    finished(outputURL)
                case .failed:
                    print("failed \(String(describing: exportSession.error))")
                    finished(nil)
                case .cancelled:
                    print("cancelled \(String(describing: exportSession.error))")
                    finished(nil)
                default: break
                }
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    func pavan_mergeVideoAndAudio(videoUrl: URL,
                                  audioUrl: URL,
                                  audioArrayList: [soundMergeList],
                                  shouldFlipHorizontally: Bool = false,
                                  filename: String,
                                  completion: @escaping (_ error: Error?, _ url: URL?) -> Void) {
        var audioAyyay : [(asses: AVAsset,time:Double)] = []
        for item in audioArrayList {
            audioAyyay.append((asses: AVAsset(url: item.url), time: item.time))
        }
        //        if audioAyyay.count == 0 {
        //            completion(NSError(localizedDescription: "send audio list"), nil)
        //            return
        //        }
        let mixComposition = AVMutableComposition()
        var mutableCompositionVideoTrack = [AVMutableCompositionTrack]()
        var mutableCompositionAudioTrack = [AVMutableCompositionTrack]()
        var mutableCompositionAudioOfVideoTrack = [AVMutableCompositionTrack]()
        
        
        var audioMix: AVMutableAudioMix = AVMutableAudioMix()
        //start merge
        
        let aVideoAsset = AVAsset(url: videoUrl)
        let aAudioAsset = AVAsset(url: audioUrl)
        
        let compositionAddVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                 preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let compositionAddAudio = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                 preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let compositionAddAudioOfVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                        preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioOfVideoAssetTrack: AVAssetTrack? = aVideoAsset.tracks(withMediaType: AVMediaType.audio).first
        //      let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        guard let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio).first else {
            completion(VideoError.audio(type: "Audio track not found"),nil)
            return
        }
        
        // Default must have tranformation
        compositionAddVideo?.preferredTransform = aVideoAssetTrack.preferredTransform
        
        if shouldFlipHorizontally {
            // Flip video horizontally
            var frontalTransform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            frontalTransform = frontalTransform.translatedBy(x: -aVideoAssetTrack.naturalSize.width, y: 0.0)
            frontalTransform = frontalTransform.translatedBy(x: 0.0, y: -aVideoAssetTrack.naturalSize.width)
            compositionAddVideo?.preferredTransform = frontalTransform
        }
        
        mutableCompositionVideoTrack.append(compositionAddVideo!)
        mutableCompositionAudioTrack.append(compositionAddAudio!)
        mutableCompositionAudioOfVideoTrack.append(compositionAddAudioOfVideo!)
        
        //        let audioInputParams = AVMutableAudioMixInputParameters()
        //
        //        audioInputParams.setVolume(0.0, at: .zero)
        //        audioInputParams.trackID = aAudioAssetTrack.trackID
        //        audioMix.inputParameters = [audioInputParams]
        
        //        var audioMixParam: [AVMutableAudioMixInputParameters] = []
        //        let videoParam: AVMutableAudioMixInputParameters = AVMutableAudioMixInputParameters(track: aAudioAssetTrack)
        //        videoParam.trackID = aAudioAssetTrack.trackID
        //        videoParam.setVolume(0.1, at: CMTime.zero)
        ////        videoParam.setVolumeRamp(fromStartVolume: 0.0, toEndVolume: 0.0, timeRange: CMTimeRange(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration))
        //
        //        audioMixParam.append(videoParam)
        //
        //        audioMix.inputParameters = audioMixParam
        
        do {
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                duration: aVideoAssetTrack.timeRange.duration),
                                                                of: aVideoAssetTrack,
                                                                at: CMTime.zero)
            //        In my case my audio file is longer then video file so i took videoAsset duration
            //        instead of audioAsset duration
            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                duration: aVideoAssetTrack.timeRange.duration),
                                                                of: aAudioAssetTrack,
                                                                at: CMTime.zero)
            
            
            
            for item in audioAyyay {
                var mutableCompositionAudioTrack2 = [AVMutableCompositionTrack]()
                let compositionAddAudio2 = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                          preferredTrackID: kCMPersistentTrackID_Invalid)
                if let aAudioAssetTrack2: AVAssetTrack = item.asses.tracks(withMediaType: AVMediaType.audio).first {
                    //                if let aAudioAssetTrack2: AVAssetTrack = item.asses.tracks(withMediaType: AVMediaType.audio)[0]
                    mutableCompositionAudioTrack2.append(compositionAddAudio2!)
                    let austime = aVideoAssetTrack.timeRange.duration.seconds - aAudioAssetTrack2.timeRange.duration.seconds - 1.5
                    if austime > item.time {
                        try mutableCompositionAudioTrack2[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                             duration: aAudioAssetTrack2.timeRange.duration),
                                                                             of: aAudioAssetTrack2,
                                                                             at: CMTime(seconds: item.time, preferredTimescale: 1))
                    }else{
                        print("notAdd", item.time,"--->",austime)
                    }
                }
            }
            // adding audio (of the video if exists) asset to the final composition
            if let aAudioOfVideoAssetTrack = aAudioOfVideoAssetTrack {
                try mutableCompositionAudioOfVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                           duration: aVideoAssetTrack.timeRange.duration),
                                                                           of: aAudioOfVideoAssetTrack,
                                                                           at: CMTime.zero)
            }
        } catch {
            print(error.localizedDescription)
        }
        
        let savePathUrl: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/\(filename).mp4")
        do { // delete old video
            try FileManager.default.removeItem(at: savePathUrl)
        } catch { print(error.localizedDescription) }
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetMediumQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.audioMix = audioMix
        assetExport.outputURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {
            case AVAssetExportSession.Status.completed:
                print("success")
                completion(nil, savePathUrl)
            case AVAssetExportSession.Status.failed:
                print("failed \(assetExport.error?.localizedDescription ?? "error nil")")
                completion(assetExport.error, nil)
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport.error?.localizedDescription ?? "error nil")")
                completion(assetExport.error, nil)
            default:
                print("complete")
                completion(assetExport.error, nil)
            }
        }
    }
    
    func dk_mergeVideoAndAudio(videoUrl: URL,
                               audioUrl: URL,
                               micAudioURL:URL,
                               audioArrayList: [soundMergeList],
                               shouldFlipHorizontally: Bool = false,
                               filename: String,
                               completion: @escaping (_ error: Error?, _ url: URL?) -> Void) {
        //Export Audio For Echo effect
        var audioAyyay : [(asses: AVAsset,time:Double)] = []
        for item in audioArrayList {
            audioAyyay.append((asses: AVAsset(url: item.url), time: item.time))
        }
        //        if audioAyyay.count == 0 {
        //            completion(NSError(localizedDescription: "send audio list"), nil)
        //            return
        //        }
        let mixComposition = AVMutableComposition()
        var mutableCompositionVideoTrack = [AVMutableCompositionTrack]()
        var mutableCompositionAudioTrack = [AVMutableCompositionTrack]()
        var mutableCompositionAudioOfVideoTrack = [AVMutableCompositionTrack]()
        
        
        var audioMix: AVMutableAudioMix = AVMutableAudioMix()
        //start merge
        
        let aVideoAsset = AVAsset(url: videoUrl)
        let aAudioAsset = AVAsset(url: audioUrl)
        
        let compositionAddVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.video,
                                                                 preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let compositionAddAudio = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                 preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let compositionAddAudioOfVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                        preferredTrackID: kCMPersistentTrackID_Invalid)
        
        let aVideoAssetTrack: AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
        let aAudioOfVideoAssetTrack: AVAssetTrack? = aVideoAsset.tracks(withMediaType: AVMediaType.audio).first
        //      let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
        
        guard let aAudioAssetTrack: AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio).first else {
            completion(VideoError.audio(type: "Audio track not found"),nil)
            return
        }
        
        // Default must have tranformation
        compositionAddVideo?.preferredTransform = aVideoAssetTrack.preferredTransform
        
        if shouldFlipHorizontally {
            // Flip video horizontally
            var frontalTransform: CGAffineTransform = CGAffineTransform(scaleX: -1.0, y: 1.0)
            frontalTransform = frontalTransform.translatedBy(x: -aVideoAssetTrack.naturalSize.width, y: 0.0)
            frontalTransform = frontalTransform.translatedBy(x: 0.0, y: -aVideoAssetTrack.naturalSize.width)
            compositionAddVideo?.preferredTransform = frontalTransform
        }
        
        mutableCompositionVideoTrack.append(compositionAddVideo!)
        mutableCompositionAudioTrack.append(compositionAddAudio!)
        mutableCompositionAudioOfVideoTrack.append(compositionAddAudioOfVideo!)
        
        //        let audioInputParams = AVMutableAudioMixInputParameters()
        //
        //        audioInputParams.setVolume(0.0, at: .zero)
        //        audioInputParams.trackID = aAudioAssetTrack.trackID
        //        audioMix.inputParameters = [audioInputParams]
        
        //        var audioMixParam: [AVMutableAudioMixInputParameters] = []
        //        let videoParam: AVMutableAudioMixInputParameters = AVMutableAudioMixInputParameters(track: aAudioAssetTrack)
        //        videoParam.trackID = aAudioAssetTrack.trackID
        //        videoParam.setVolume(0.1, at: CMTime.zero)
        ////        videoParam.setVolumeRamp(fromStartVolume: 0.0, toEndVolume: 0.0, timeRange: CMTimeRange(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration))
        //
        //        audioMixParam.append(videoParam)
        //
        //        audioMix.inputParameters = audioMixParam
        
        do {
            try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                duration: aVideoAssetTrack.timeRange.duration),
                                                                of: aVideoAssetTrack,
                                                                at: CMTime.zero)
            //        In my case my audio file is longer then video file so i took videoAsset duration
            //        instead of audioAsset duration
            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                duration: aVideoAssetTrack.timeRange.duration),
                                                                of: aAudioAssetTrack,
                                                                at: CMTime.zero)
            
            
            
            for item in audioAyyay {
                var mutableCompositionAudioTrack2 = [AVMutableCompositionTrack]()
                let compositionAddAudio2 = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                          preferredTrackID: kCMPersistentTrackID_Invalid)
                if let aAudioAssetTrack2: AVAssetTrack = item.asses.tracks(withMediaType: AVMediaType.audio).first {
                    //                if let aAudioAssetTrack2: AVAssetTrack = item.asses.tracks(withMediaType: AVMediaType.audio)[0]
                    mutableCompositionAudioTrack2.append(compositionAddAudio2!)
                    let austime = aVideoAssetTrack.timeRange.duration.seconds - aAudioAssetTrack2.timeRange.duration.seconds - 1.5
                    if austime > item.time {
                        try mutableCompositionAudioTrack2[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                             duration: aAudioAssetTrack2.timeRange.duration),
                                                                             of: aAudioAssetTrack2,
                                                                             at: CMTime(seconds: item.time, preferredTimescale: 1))
                    }else{
                        print("notAdd", item.time,"--->",austime)
                    }
                }
            }
            // adding audio (of the video if exists) asset to the final composition
            //we don't Add this
//            if let aAudioOfVideoAssetTrack = aAudioOfVideoAssetTrack {
//                try mutableCompositionAudioOfVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
//                                                                                           duration: aVideoAssetTrack.timeRange.duration),
//                                                                           of: aAudioOfVideoAssetTrack,
//                                                                           at: CMTime.zero)
//            }
            
            let micAudioAsset = AVURLAsset(url: micAudioURL)
            //We add external mic data
            var mutableCompositionAudioTrack2 = [AVMutableCompositionTrack]()
            let compositionAddAudio2 = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio,
                                                                      preferredTrackID: kCMPersistentTrackID_Invalid)
            if let aAudioAssetTrack2: AVAssetTrack = micAudioAsset.tracks(withMediaType: AVMediaType.audio).first {
                mutableCompositionAudioTrack2.append(compositionAddAudio2!)
                
                try mutableCompositionAudioTrack2[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero,
                                                                                     duration: aAudioAssetTrack2.timeRange.duration),
                                                                     of: aAudioAssetTrack2,
                                                                     at: CMTime.zero)
            }
            
        } catch {
            print(error.localizedDescription)
        }
        
        let savePathUrl: URL = URL(fileURLWithPath: NSHomeDirectory() + "/Documents/\(filename).mp4")
        do { // delete old video
            try FileManager.default.removeItem(at: savePathUrl)
        } catch { print(error.localizedDescription) }
        
        let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetMediumQuality)!
        assetExport.outputFileType = AVFileType.mp4
        assetExport.audioMix = audioMix
        assetExport.outputURL = savePathUrl
        assetExport.shouldOptimizeForNetworkUse = true
        
        assetExport.exportAsynchronously { () -> Void in
            switch assetExport.status {
            case AVAssetExportSession.Status.completed:
                print("success")
                completion(nil, savePathUrl)
            case AVAssetExportSession.Status.failed:
                print("failed \(assetExport.error?.localizedDescription ?? "error nil")")
                completion(assetExport.error, nil)
            case AVAssetExportSession.Status.cancelled:
                print("cancelled \(assetExport.error?.localizedDescription ?? "error nil")")
                completion(assetExport.error, nil)
            default:
                print("complete")
                completion(assetExport.error, nil)
            }
        }
    }
    
    
}


extension AVAsset {
    func generateThumbnail(completion: @escaping (UIImage?) -> Void) {
        DispatchQueue.global().async {
            let imageGenerator = AVAssetImageGenerator(asset: self)
            let time = CMTime(seconds: 0.0, preferredTimescale: 600)
            let times = [NSValue(time: time)]
            imageGenerator.generateCGImagesAsynchronously(forTimes: times, completionHandler: { _, image, _, _, _ in
                if let image = image {
                    completion(UIImage(cgImage: image))
                } else {
                    completion(nil)
                }
            })
        }
    }
}
