 //
//  InAppPurchase.swift
//  ios_swift_in_app_purchases_sample
//
//  Created by Maxim Bilan on 7/27/15.
//  Copyright (c) 2015 Maxim Bilan. All rights reserved.
//

import Foundation
import StoreKit
 
protocol InAppPurchaseDelegate : NSObjectProtocol {
    func InAppPurchaseSuccess(data : SKPaymentTransaction)
    func InAppPurchaseCancel(data : String)
    func InAppPurchaseError(data : String)
 }
class InAppPurchase : NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
	
	static let sharedInstance = InAppPurchase()
    
    var delegate : InAppPurchaseDelegate? = nil
	
#if DEBUG
	let verifyReceiptURL = "https://sandbox.itunes.apple.com/verifyReceipt"
#else
	let verifyReceiptURL = "https://buy.itunes.apple.com/verifyReceipt"
#endif
	
	let kInAppProductPurchasedNotification = "InAppProductPurchasedNotification"
	let kInAppPurchaseFailedNotification   = "InAppPurchaseFailedNotification"
	let kInAppProductRestoredNotification  = "InAppProductRestoredNotification"
	let kInAppPurchasingErrorNotification  = "InAppPurchasingErrorNotification"
	
	let InAppPurchase1Monthly = "Yesitlabs.Monthly"
    let InAppPurchase3Month = "Yesitlabs.Monthly3"
    let InAppPurchase6Month = "Yesitlabs.Monthly6"
    let InAppPurchase1Year = "Yesitlabs.Year1"
    
    let unlockTestInAppPurchase1ProductId99 = "Yesitlabs.Monthly"
    let unlockTestInAppPurchasetestRestore = "yes.testRestore"
    
	
    public typealias SelectionClosure123 = (String, SKPaymentTransaction?) -> Void
    public var selectionActionPurchased: SelectionClosure123?
    public var selectionActionGetLastDate: SelectionClosure123?
    public var selectionActionFailed: SelectionClosure123?
    public var selectionActionRestored: SelectionClosure123?
    public var selectionActionError: SelectionClosure123?
    public var selectionActionNoProduct: SelectionClosure123?
    
	override init() {
		super.init()
		SKPaymentQueue.default().add(self)
	}
	
	func buyProduct(_ product: SKProduct) {
		print("Sending the Payment Request to Apple")
		let payment = SKPayment(product: product)
		SKPaymentQueue.default().add(payment)
	}
	
	func restoreTransactions() {
		SKPaymentQueue.default().restoreCompletedTransactions()
	}
	
	func request(_ request: SKRequest, didFailWithError error: Error) {
		print("Error %@ \(error)")
        selectionActionError?("Error %@ \(error)",nil)
		NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: error.localizedDescription)
	}
	
	func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
		let count: Int = response.products.count
        print("Got the request from Apple",count)
		if count > 0 {
			_ = response.products
			let validProduct: SKProduct = response.products[0] 
			print(validProduct.localizedTitle)
			print(validProduct.localizedDescription)
			print(validProduct.price)
			buyProduct(validProduct);
		}
		else {
            selectionActionNoProduct?("No products",nil)
			print("No products")
		}
	}
	
	func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
		print("Received Payment Transaction Response from Apple");
		
		for transaction: AnyObject in transactions {
			if let trans: SKPaymentTransaction = transaction as? SKPaymentTransaction {
				switch trans.transactionState {
				case .purchased:
					print("Product Purchased")
					
					savePurchasedProductIdentifier(trans.payment.productIdentifier)
					SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
					NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductPurchasedNotification), object: nil)
					receiptValidation()
                    self.delegate?.InAppPurchaseSuccess(data: trans)
                    self.selectionActionPurchased?("purchased",trans)
					break
					
				case .failed:
					print("Purchased Failed")
                    self.delegate?.InAppPurchaseError(data: "error")
					SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
					NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchaseFailedNotification), object: nil)
                    self.selectionActionFailed?("failed",trans)
					break
					
				case .restored:
					print("Product Restored")
					savePurchasedProductIdentifier(trans.payment.productIdentifier)
					SKPaymentQueue.default().finishTransaction(transaction as! SKPaymentTransaction)
					NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppProductRestoredNotification), object: nil)
                    self.selectionActionRestored?("restored",trans)
					break
                case .purchasing:
                    //self.selectionActionFailed?("failed",trans)
                    break
                case .deferred:
                    //self.selectionActionFailed?("failed",trans)
                    break
                @unknown default:
                    self.selectionActionFailed?("failed",trans)
                    break
                }
//				default:
////                    self.selectionActionFailed?("failed",trans)
//					break
//				}
			}
			else {
				
			}
		}
	}
	
	func savePurchasedProductIdentifier(_ productIdentifier: String!) {
		UserDefaults.standard.set(productIdentifier, forKey: productIdentifier)
		UserDefaults.standard.synchronize()
	}
	
	func receiptValidation() {

		let receiptFileURL = Bundle.main.appStoreReceiptURL
		let receiptData = try? Data(contentsOf: receiptFileURL!)
		let recieptString = receiptData?.base64EncodedString(options: NSData.Base64EncodingOptions(rawValue: 0))
//		let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "d8a21a2efe2e4d6e9482c7be996a79d0" as AnyObject]
        let jsonDict: [String: AnyObject] = ["receipt-data" : recieptString! as AnyObject, "password" : "ab59642069a0471bacf8ab1363f76bd6" as AnyObject]
		do {
			let requestData = try JSONSerialization.data(withJSONObject: jsonDict, options: JSONSerialization.WritingOptions.prettyPrinted)
			let storeURL = URL(string: verifyReceiptURL)!
			var storeRequest = URLRequest(url: storeURL)
			storeRequest.httpMethod = "POST"
			storeRequest.httpBody = requestData

			let session = URLSession(configuration: URLSessionConfiguration.default)
			let task = session.dataTask(with: storeRequest, completionHandler: { [weak self] (data, response, error) in
				do {
					let jsonResponse = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
					print(jsonResponse)
					if let date = self?.getExpirationDateFromResponse(jsonResponse as! NSDictionary) {
						print(date)
					}
				} catch let parseError {
					print(parseError)
				}
			})
			task.resume()
		} catch let parseError {
			print(parseError)
		}
	}
	
	func getExpirationDateFromResponse(_ jsonResponse: NSDictionary) -> Date? {
		if let receiptInfo: NSArray = jsonResponse["latest_receipt_info"] as? NSArray {
			let lastReceipt = receiptInfo.lastObject as! NSDictionary
			let formatter = DateFormatter()
			formatter.dateFormat = "yyyy-MM-dd HH:mm:ss VV"
			if let expiresDate = lastReceipt["expires_date"] as? String {
				return formatter.date(from: expiresDate)
			}
			return nil
		}
		else {
			return nil
		}
	}
	
	func unlockProduct(_ productIdentifier: String) {
		if SKPaymentQueue.canMakePayments() {
            let productID: NSSet = NSSet(object: productIdentifier)
			let productsRequest: SKProductsRequest = SKProductsRequest(productIdentifiers: productID as! Set<String>)
			productsRequest.delegate = self
			productsRequest.start()
			print("Fetching Products")
		}
		else {
            selectionActionNoProduct?("Сan't make purchases",nil)
			print("Сan't make purchases")
			NotificationCenter.default.post(name: Notification.Name(rawValue: kInAppPurchasingErrorNotification), object: NSLocalizedString("CANT_MAKE_PURCHASES", comment: "Can't make purchases"))
		}
	}
	
	func buyUnlockTestInAppPurchase1() {
		unlockProduct(InAppPurchase1Monthly)
	}
	func buyUnlockTestInAppPurchase2() {
		unlockProduct(InAppPurchase3Month)
	}
	func buyUnlockTestInAppPurchase3() {
		unlockProduct(InAppPurchase6Month)
	}
	func buyUnlockTestInAppPurchase4() {
		unlockProduct(InAppPurchase1Year)
	}
    func buyTestInAppPurchasetestRestore() {
        unlockProduct(unlockTestInAppPurchasetestRestore)
    }
    func buyPlayCoines(ProductId:String) {
        unlockProduct(ProductId)
    }
//    func buyPlayCoines(ProductId:String) {
//        guard let val = checkVlaidPayment(ProductId: ProductId) else {
//            selectionActionNoProduct?("Please select a valid payment.",nil)
//            return
//        }
//        print("Purchase_id--->  ",val)
//        unlockProduct(val)
//    }
    func checkVlaidPayment(ProductId:String) -> String? {
//        return "test_auto_karaoke"
        return "Yesitlabs.Monthly"
        return "another_1"
        
        return nil
    }
}
