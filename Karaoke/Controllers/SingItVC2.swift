//
//  SingItVC2.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 18/02/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation

extension SingItVC {
    func getSongList()  {
        
        //http://karaoke.yilstaging.com/api/get_sound_effects
        let para = [ApiKey.user_id:UserDetail.shared.getUserId()]
        WebServices.commonPostAPIModel(model: SoundEffect.self, parameters: para, endPoint: .get_sound_effects, loader: true) { (json) in
            
        } returnModel: { (model, error) in
            guard let model = model else{
                return
            }
            self.sound_list  = model.sound_list ?? []
            self.collect.reloadData()

            for item in self.sound_list {
                if self.read_url(fromDocumentsWithFileName: item.sound_id! + ".mp3") != nil{
                }else{
                    AppNetworking.DOWNLOAD(endPoint: item.effect_url!, fileName: item.sound_id!, parameters: [:], mediaType: ".mp3", loader: false) { (status) in
                    } successData: { (url) in
                    } failure: { (error) in
                        print(error)
                    }
                }
            }
        } successData: { (data) in
        } failure: { (error) -> (Void) in
        }
        
        let params : [String:Any] = [ApiKey.user_id:UserDetail.shared.getUserId(),
                                     ApiKey.video_id:self.old_video_id]
        
        WebServices.commonPostAPIModel(model: SingleVideoDetail.self, parameters: params, endPoint: .get_video_details, loader: true) { (json) in
            
        } returnModel: { (model, error) in
            guard let model = model else{
                return
            }
            
            self.videoDetail = model.video_detail?.first
            if let url = self.videoDetail?.main_video_file {
//                self.loadPlayer(url: url)
                let name = url.fileNameWithExtension()
                let have = self.read_url(fromDocumentsWithFileName: name)
                if have != nil{
                    self.audioURLStr = have
                    self.lyricsVideoURL = have
                    self.loadPlayer(url: "", urlurl: have)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        self.audioSetup()
                    }
                }else{
                    self.loading.showActivityLoading(uiView: self.preetiVideoView)
                    AppNetworking.DOWNLOAD(endPoint: url, fileName: url.fileName(), parameters: [:], mediaType: ".mp4", loader: true) { (status) in
                    } successData: { (urln) in
                        guard let urlnew = urln else { return }
                        self.audioURLStr = urlnew
                        self.lyricsVideoURL = urlnew
                        self.loadPlayer(url: "", urlurl: urlnew)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                            self.audioSetup()
                        }
                    } failure: { (error) in
                        print(error)
                    }
                }
                
            }
        } successData: { (data) in
        } failure: { (error) -> (Void) in
        }
        
    }
    func user_subscription_plan()  {
//        http://karaoke.yilstaging.com/api/user_subscription_plan
        // user_id
//        let loader = ProfileResponse == nil ? true : false
        var parameters : [String:Any] = [:]
        let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as! String
        parameters[ApiKey.user_id] = user_id
        
        WebServices.commonPostAPI(parameters: parameters, endPoint: .user_subscription_plan, loader: true) { (json) in
            
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(SubscriptionModel.self, from: data)
                if d.status == "success" {
                    if let da = d.current_date {
                        self.currentDeviceDate = self.convertDateFormat(inputDate:da)
                    }
                    
                    self.myplan_details = d.myplan_details ?? []
                    if self.myplan_details.count > 0 {
                        let planLastDate = self.currentDeviceDate
                        for item in self.myplan_details {
                            let s = self.convertDateFormat(inputDate: item.expire_on)
                            if s >= planLastDate {
                                self.user_have_plan = true
                                break
                            }
                        }
                    }else{
                        self.user_have_plan = false
                    }
                    if self.user_have_plan == false {
                        let s = self.checkAppInstall(viewCon: self, showAlert: false
                                                     , currentDate: Date())
                        if s == nil {
                            self.AlertControllerCuston(title: alertTitle.alert_message, message: "Please upgrade to a paid plan to upload video.", BtnTitle: ["Upgrade","Cancel"]) { (str) in
                                if str == "Upgrade" {
                                    self.navigationController?.popViewController(animated: false)
                                    Timer.scheduledTimer(withTimeInterval: 0.5, repeats: false) { (_) in
                                        DispatchQueue.main.async {
                                            self.select?("",2)
                                        }
                                    }
                                }else{
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }
                        }
                    }
                }
            }
            catch {
              print(error)
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func documentDirectory() -> String {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                    .userDomainMask,
                                                                    true)
        return documentDirectory[0]
    }
    func append(toPath path: String,
                        withPathComponent pathComponent: String) -> String? {
        if var pathURL = URL(string: path) {
            pathURL.appendPathComponent(pathComponent)
            
            return pathURL.absoluteString
        }
        
        return nil
    }
    func save(text: String,
                      toDirectory directory: String,
                      withFileName fileName: String) {
        guard let filePath = self.append(toPath: directory,
                                         withPathComponent: fileName) else {
            return
        }
        
        do {
            try text.write(toFile: filePath,
                           atomically: true,
                           encoding: .utf8)
        } catch {
            print("Error", error)
            return
        }
        
        print("Save successful",filePath)
    }
    func read(fromDocumentsWithFileName fileName: String) -> String? {
        guard let filePath = self.append(toPath: self.documentDirectory(),
                                         withPathComponent: fileName) else {
                                            return nil
        }
        do {
            let savedString = try String(contentsOfFile: filePath)
            print(savedString)
            return savedString
        } catch {
            print("Error reading saved file")
            return nil
        }
    }

    
    func read_url(fromDocumentsWithFileName fileName: String) -> URL? {
        let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
        let url = NSURL(fileURLWithPath: path)
        if let pathComponent = url.appendingPathComponent(fileName) {
            let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: filePath) {
//                let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
//                let destinationUrl = documentsDirectoryURL.appendingPathComponent(fileName)
                return pathComponent
            } else {
                return nil
            }
        } else {
            return nil
        }
    }
    func documentsRemoveItem(fromDocumentsWithFileName fileName: URL) {
        let filePath = fileName.path
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: filePath) {
            do {
                try? fileManager.removeItem(atPath: "\(filePath)")
            }catch{
                print(error.localizedDescription)
            }
        }
    }
}

