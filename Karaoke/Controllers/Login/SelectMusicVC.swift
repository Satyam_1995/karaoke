//
//  SelectMusicVC.swift
//  Karaoke
//
//  Created by Ankur  on 06/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SelectMusicVC: UIViewController {
    
    @IBOutlet weak var collect:UICollectionView!
    var music_category : [(musicL:Music_category,isSelected:Bool)] = []
//    let imgArr = [#imageLiteral(resourceName: "s1"),#imageLiteral(resourceName: "s2"),#imageLiteral(resourceName: "s3"),#imageLiteral(resourceName: "s4"),#imageLiteral(resourceName: "s5"),#imageLiteral(resourceName: "s6")]
//    let typeTxtArr = ["POP Music","Love Songs","Melody","Indian","Rock","Classical"]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collect.delegate = self
        collect.dataSource = self
        collect.register(UINib(nibName: "SelectMusicCell", bundle: nil), forCellWithReuseIdentifier: "SelectMusicCell")
        collect.register(Header.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Header.identifier)
        self.get_music_category()
    }
    
    @IBAction func btnSkip(_ sender:UIButton) {
        let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNext(_ sender:UIButton) {
        add_user_music_choice()
//        let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    func get_music_category() {
//        http://karaoke.yilstaging.com/api/get_music_category
        let para : JSONDictionary = [:]
        WebServices.commonPostAPI(parameters: para, endPoint: .get_music_category, loader: true) { (json) in
//            guard let dict = json.dictionaryObject else{
//                return
//            }
            
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(Music_category_all.self, from: data)
                if let cat = d.music_category {
                    self.music_category.removeAll()
                    for item in cat {
                        self.music_category.append((musicL: item, isSelected: false))
                    }
                }
                self.collect.reloadData()
            }catch{
                
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func add_user_music_choice() {
//        Para:user_id,music_category(1,2,3,4,5)
//        URL -  http://karaoke.yilstaging.com/api/add_user_music_choice
        var catList = ""
        for item in self.music_category {
            if item.isSelected == true {
                if catList == "" {
                    catList = item.musicL.music_category_id ?? ""
                }else{
                    catList = catList + "," + (item.musicL.music_category_id ?? "")
                }
            }
        }
        let para : JSONDictionary = [ApiKey.user_id:UserDetail.shared.getUserId(),
                                     ApiKey.music_category:catList]
        WebServices.commonPostAPI(parameters: para, endPoint: .add_user_music_choice, loader: true) { (json) in
            let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
            self.navigationController?.pushViewController(vc, animated: true)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
}

extension SelectMusicVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return music_category.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collect.dequeueReusableCell(withReuseIdentifier: "SelectMusicCell", for: indexPath) as! SelectMusicCell
        let dict = music_category[indexPath.item].musicL
        if let url = dict.category_img?.convert_to_url {
            cell.img_musicType.af.setImage(withURL: url)
        }
        cell.img_select.isHidden = true
        cell.back_view.isHidden = true
        cell.lbl_type.text = dict.category_name
        setCustomColor.shared.p_setTextlblColor(lbl: cell.lbl_type)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        music_category[indexPath.item].isSelected = !music_category[indexPath.item].isSelected
        let cell = collectionView.cellForItem(at: indexPath) as! SelectMusicCell
        if cell.img_select.isHidden == false {
            cell.img_select.isHidden = true
            cell.back_view.isHidden = true
        }else{
            cell.img_select.isHidden = false
            cell.back_view.isHidden = false
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collect.frame.size.width/2 - 5
        let h = collect.frame.size.height/3
        return CGSize(width: w, height: h)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.size.width, height: 60)
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collect.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: Header.identifier, for: indexPath) as! Header
        header.Configure(str: "Tell us what you like to sing!", txtColor: .white, align: .center)
        return header
    }
    
}
