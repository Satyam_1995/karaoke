//
//  ChangePassVC.swift
//  Karaoke
//
//  Created by Ankur  on 19/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class ChangePassVC: UIViewController {
    
    @IBOutlet weak var txt_oldPassword:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    @IBOutlet weak var txt_confirmPass:UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    @IBAction func btn_Submit(_ sender:UIButton){
        self.updatePass()
//        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btn_Close(_ sender:UIButton){
        self.dismiss(animated: true, completion: nil)
    }
    func updatePass() {
//        URL - http://karaoke.yilstaging.com/api/change_password
        guard validation() else {
            return
        }
        var para : JSONDictionary = [:]
        para[ApiKey.old_password] = self.txt_oldPassword.text!
        para[ApiKey.password] = self.txt_password.text!
        para[ApiKey.user_id] = UserDetail.shared.getUserId()
        
        WebServices.commonPostAPI(parameters: para, endPoint: .change_password, loader: true) { (json) in
            if let dict = json.dictionaryObject {
                if let state = dict["status"] as? String , state == "fail" {
                    self.AlertControllerOnr(title: alertTitle.alert_error, message: dict["message"] as? String)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
            
        } successData: { (data) in
        } failure: { (err) -> (Void) in
        }
    }
    func validation() -> Bool {
        
        if self.txt_oldPassword.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.oldpassword)
            return false
        }
        if self.txt_password.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.password)
            return false
        }
        if !self.isPasswordValid(self.txt_password.text!)  {
            CommonFunctions.showToastWithMessage(MessageString.passwordValid,displayTime: 5)
            return false
        }
        if self.txt_confirmPass.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.confirmpassword)
            return false
        }
        if self.txt_confirmPass.text != self.txt_password.text  {
            CommonFunctions.showToastWithMessage(MessageString.matchConfirm)
            return false
        }
        
        return true
    }
}
