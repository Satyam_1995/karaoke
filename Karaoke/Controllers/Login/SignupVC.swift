//
//  SignupVC.swift
//  Karaoke
//
//  Created by Ankur  on 03/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SignupVC: UIViewController {

    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_username:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    @IBOutlet weak var txt_confirmPass:UITextField!
    @IBOutlet weak var btn_submit:UIButton!
    @IBOutlet weak var btn_login:UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_username.tag = 0
        txt_email.tag = 0
        
//        self.txt_name.text = "test1"
//        self.txt_username.text = "test1"
//        self.txt_email.text = "@yopmail.com"
//        self.txt_password.text = "Test@123"
//        self.txt_confirmPass.text = "Test@123"
        txt_password.isSecureTextEntry = true
        txt_confirmPass.isSecureTextEntry = true
    }
    
    @IBAction func btnLogin(_ sender:UIButton) {
        let vc = LoginVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnTerms(_ sender:UIButton) {
         let vc = TermsVC.instantiate(fromAppStoryboard: .Main)
         vc.status = 1
         self.navigationController?.pushViewController(vc, animated: true)
     }
     
     @IBAction func btnPrivacy(_ sender:UIButton) {
         let vc = TermsVC.instantiate(fromAppStoryboard: .Main)
         vc.status = 2
         self.navigationController?.pushViewController(vc, animated: true)
     }
    
    @IBAction func btnSignup(_ sender:UIButton) {
        self.view.endEditing(true)
        guard RechabilityConnect.isConnectedToNetwork() else {
            self.AlertControllerCuston(title: "No Internet Connection", message: "Make sure your device is connected to the internet.", BtnTitle: ["OK"]) { (str) in
            }
            return
        }
        guard validation() else {
            return
        }
//        let vc = CreateProfileVC.instantiate(fromAppStoryboard: .Main)
//        vc.txt_passwordStr = self.txt_password.text!
//        vc.txt_nameStr = self.txt_name.text ?? ""
//        vc.txt_usernameStr = self.txt_username.text ?? ""
//        vc.txt_emailStr = self.txt_email.text ?? ""
//        self.navigationController?.pushViewController(vc, animated: true)
//        return
        let sendE = self.txt_email.text!
        var para : JSONDictionary = [:]
            para[ApiKey.email] = sendE
        
        WebServices.commonPostAPI(parameters: para, endPoint: .send_otp, loader: true) { (json) in
            
            if let dict = json.dictionaryObject {
                if let status = dict["status"] as? Bool, status == true, let otp = dict["otp"] as? String {
                    if let user_id = dict["userid"] as? String, user_id != "" {
//                        self.AlertControllerOnr(title: alertTitle.alert_error, message: MessageString.alreadyMobile, BtnTitle: "OK")
                    }else{
                        CommonFunctions.showToastWithMessage(MessageString.sentOTP)
                        let vc = OTPVC.instantiate(fromAppStoryboard: .Main)
                        vc.Otp = otp
                        vc.txt_Email = sendE
                        vc.select = { [weak self] (str,status) in
                            if status == 1 {
                                let vc = CreateProfileVC.instantiate(fromAppStoryboard: .Main)
                                vc.txt_passwordStr = self!.txt_password.text!
                                vc.txt_nameStr = self!.txt_name.text ?? ""
                                vc.txt_usernameStr = self!.txt_username.text ?? ""
                                vc.txt_emailStr = self!.txt_email.text ?? ""
                                self?.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        self.present(vc, animated: true, completion: nil)
                    }
                }
            }
        } successData: { (data) in
        } failure: { (error) -> (Void) in
            print(error)
        }
    }
    
    @IBAction func editEmail(_ textField: UITextField) {
        if textField.text == "" {
           return
        }
        if textField == txt_username {
            self.check_username()
        }
        if textField == txt_email {
            self.check_email()
        }
    }
}
extension SignupVC {
    func check_username() {
        if txt_username.text == "" {
            return
        }
        let para : JSONDictionary = [ApiKey.username:txt_username.text!]
        WebServices.commonPostAPI(parameters: para, endPoint: .check_username_is_valid, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            let status = dict["status"] as? String ?? ""
            if status == "success" {
                self.txt_username.tag = 1
            }else if let suggested = dict["suggested"] as? String {
                let msg = MessageString.username_already + "\n\nsuggested username is \"\(suggested)\""
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: msg, BtnTitle: ["Use suggested","Use another"]) { (str) in
                    if str == "Use suggested" {
                        self.txt_username.text = suggested
                        self.txt_username.tag = 1
                    }else{
//                        self.txt_username.becomeFirstResponder()
                        self.txt_username.tag = 0
                    }
                }
            }else {
                self.txt_username.tag = 0
                self.AlertControllerCuston(title: alertTitle.alert_error, message: "Something went wrong please login again", BtnTitle: ["OK"], completion: { (_) in
                })
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func check_email() {
        if txt_email.text == "" {
            return
        }
        let para : JSONDictionary = [ApiKey.email:txt_email.text!]
        WebServices.commonPostAPI(parameters: para, endPoint: .check_email, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            let message = dict["message"] as? String ?? ""
            if message == "Email not registered!" {
                self.txt_email.tag = 1
            }else {
                self.txt_email.tag = 0
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: MessageString.Email_already_exists, BtnTitle: ["OK"]) { (str) in
//                    self.txt_email.becomeFirstResponder()
                }
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func validation() -> Bool {
        if self.txt_name.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.name)
            return false
        }
        if self.txt_username.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.username)
            return false
        }
        if self.txt_username.tag != 1  {
            CommonFunctions.showToastWithMessage(MessageString.username_already)
            return false
        }
        if self.txt_email.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.emailorMobile)
            return false
        }
        if !self.isValidEmail(testStr: self.txt_email.text!) {
            CommonFunctions.showToastWithMessage(MessageString.validEmail)
            return false
        }
        
        if self.txt_email.tag != 1  {
            CommonFunctions.showToastWithMessage(MessageString.Email_already_exists)
            return false
        }
        if self.txt_password.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.password)
            return false
        }
        if !self.isPasswordValid(self.txt_password.text!)  {
            CommonFunctions.showToastWithMessage(MessageString.passwordValid,displayTime: 5)
            return false
        }
        if self.txt_confirmPass.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.confirmpassword)
            return false
        }
        if self.txt_confirmPass.text != self.txt_password.text  {
            CommonFunctions.showToastWithMessage(MessageString.matchConfirm)
            return false
        }
        
//        if self.btnTerms.tag != 1  {
//            CommonFunctions.showToastWithMessage(MessageString.terms,displayTime: 4)
//            return false
//        }
        return true
    }
}
