//
//  LoginVC.swift
//  Karaoke
//
//  Created by Ankur  on 02/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKLoginKit

class LoginVC: UIViewController {
    @IBOutlet weak var txt_email:UITextField!
    @IBOutlet weak var txt_password:UITextField!
    
    @IBOutlet weak var signInButton: GIDSignInButton!
    var music_category : [(musicL:Music_category,isSelected:Bool)] = []
    let appleSignIn = HSAppleSignIn()
    let loginManager = LoginManager()
    let signInConfig = GIDConfiguration.init(clientID: "1064307263446-m9c03jdlrqg91qhtsci0d3ca9ho1em71.apps.googleusercontent.com")
    override func viewDidLoad() {
        super.viewDidLoad()
        let user_id = UserDetail.shared.getUserId()
        if user_id != "" {
            let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
            self.navigationController?.pushViewController(vc, animated: false)
        }else{
//            txt_password.text = "Test@123"
        }
        txt_password.isSecureTextEntry = true
        get_music_category()
        let status = KeyChain.delete(key: "karaoke_first_time")
//        print(status)
        if let receivedData = KeyChain.load(key: "karaoke_first_time") {
            let result = receivedData.to(type: Date.self)
            print("result: ", result)
//            let status = KeyChain.delete(key: "karaoke_first_time")
//            print(status)
        }else{
//            let modifiedDate = Calendar.current.date(byAdding: .day, value: -40, to: Date())!
            
//            let int: Date = modifiedDate
            let int: Date = Date()
            let data = Data(from: int)
            let status = KeyChain.save(key: "karaoke_first_time", data: data)
            print("status: ", status)
        }
    }
    
    @IBAction func btnSignup(_ sender:UIButton) {
        let vc = SignupVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnLogin(_ sender:UIButton) {
        guard validation() else { return }
        self.login()
    }
    func validation() -> Bool {
        if self.txt_email.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.emailorMobile)
            return false
        }
        if self.txt_password.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.password)
            return false
        }
        return true
    }
    @IBAction func btnForgotPass(_ sender:UIButton) {
        let vc = ForgotPassVC.instantiate(fromAppStoryboard: .Main)
        self.present(vc, animated: true, completion: nil)
    }
    
    @IBAction func btnTerms(_ sender:UIButton) {
        let vc = TermsVC.instantiate(fromAppStoryboard: .Main)
        vc.status = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnPrivacy(_ sender:UIButton) {
        let vc = TermsVC.instantiate(fromAppStoryboard: .Main)
        vc.status = 2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func login() {
        
        //        Para: password, email(optional), user_name(optional), fcm_token
        //        URL- http://karaoke.yilstaging.com/api/login
        
        let token = UserDefaults.standard.object(forKey: "fcm_token") as? String ?? "123456"
        var para : JSONDictionary = [ApiKey.password:txt_password.text!,
                                     ApiKey.fcm_token:token]
        if txt_email.text!.contains(s: "@") {
            para[ApiKey.email] = txt_email.text!
        }else{
            para[ApiKey.user_name] = txt_email.text!
        }
        WebServices.commonPostAPI(parameters: para, endPoint: .login, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{ return }
            if let status = dict["status"] as? String, status == "success", let user_id = dict["user_id"] as? String {
                UserDetail.shared.setUserId(user_id)
                let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
                self.navigationController?.pushViewController(vc, animated: false)
            }else {
                let message = dict["message"] as? String ?? ""
                let user_id = dict["user_id"] as? String
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: message, BtnTitle: ["OK"]) { (str) in
                }
            }
        } successData: { (data) in
        } failure: { (error) -> (Void) in
            print(error)
            self.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
        }
    }
    func social_login(social_id:String, email:String, name:String, image:Data?,social_type:social_type) {
        //        Para:  social_id,email,name,image(optional),social_type(facebook, google, apple)
        //        URL -  http://karaoke.yilstaging.com/api/social_login
        var para : JSONDictionary = [:]
        
        para[ApiKey.social_id] = social_id
        para[ApiKey.name] = name
        para[ApiKey.email] = email
        para[ApiKey.social_type] = social_type.rawValue
        para[ApiKey.fcm_token] = "123456789"
        
        var pmgPar :[UploadFileParameter] = []
        
        if let data = image {
            let name = Date.getCurrentDateForName() + ".png"
            pmgPar.append(UploadFileParameter(fileName: name, key: ApiKey.image, data: data, mimeType: ".png"))
        }
        
        WebServices.commonPostAPI_data(parameters: para, files: pmgPar, endPoint: .social_login, loader: true) { (json) in
            print (para)
            guard let dict = json.dictionaryObject else{ return }
            if let status = dict["status"] as? String, status == "success", let user_id = dict["user_id"] as? String {
                UserDetail.shared.setUserId(user_id)
                
                self.add_user_music_choice()
                let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
                self.navigationController?.pushViewController(vc, animated: false)
                
            }else {
                
                let message = dict["message"] as? String ?? ""
                let user_id = dict["user_id"] as? String
                
                UserDetail.shared.setUserId(user_id ?? "")
                
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: message, BtnTitle: ["OK"]) { (str) in
                    if str == "OK"{
                        print(UserDetail.shared.getUserId())
                        self.add_user_music_choice()
                        let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
                        self.navigationController?.pushViewController(vc, animated: false)
                    }
                }
            }
        } successData: { (data) in
            
        } progress: {(pr) in
            
        } failure: { (error) -> (Void) in
            print(error)
            self.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
        }
    }
    func add_user_music_choice() {
//        Para:user_id,music_category(1,2,3,4,5)
//        URL -  http://karaoke.yilstaging.com/api/add_user_music_choice
        var catList = ""
        var joindAray:[String] = []
        for item in self.music_category {
//item.musicL.music_category_id ?? ""
            joindAray.append(item.musicL.music_category_id ?? "")
            print(item.musicL.music_category_id ?? "" )
            
//                    catList = (item.musicL.music_category_id ?? "") + "," + catList
            }
        catList = joindAray.joined(separator: ",")
        
        print(catList,self.music_category)
        let para : JSONDictionary = [ApiKey.user_id:UserDetail.shared.getUserId(),
                                     ApiKey.music_category:catList]
        
        WebServices.commonPostAPI(parameters: para, endPoint: .add_user_music_choice, loader: true) { (json) in
            print(para,json)
            let vc = TabbarVC.instantiate(fromAppStoryboard: .Home)
           self.navigationController?.pushViewController(vc, animated: false)
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func get_music_category() {
//        http://karaoke.yilstaging.com/api/get_music_category
        let para : JSONDictionary = [:]
        WebServices.commonPostAPI(parameters: para, endPoint: .get_music_category, loader: true) { (json) in
//            guard let dict = json.dictionaryObject else{
//                return
//            }
            
        } successData: { (data) in
            do {
                let d = try JSONDecoder().decode(Music_category_all.self, from: data)
                if let cat = d.music_category {
                    self.music_category.removeAll()
                    for item in cat {
                        self.music_category.append((musicL: item, isSelected: false))
                    }
                }
               
            }catch{
                
            }
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    
    @IBAction func btn_Facebook(_ sender:UIButton) {
        loginManager.logIn(
            permissions: [.publicProfile,.email],
            viewController: self
        ) { result in
            self.loginManagerDidComplete(result)
        }
    }
    
    @IBAction func btnGoogle(_ sender:UIButton) {
        
        GIDSignIn.sharedInstance.signIn(with: self.signInConfig, presenting: self) { user, error in
            if let error = error {
                if (error as NSError).code == GIDSignInError.hasNoAuthInKeychain.rawValue {
                    print("The user has not signed in before or they have since signed out.")
                    self.AlertControllerOnr(title: alertTitle.alert_error, message: "The user has not signed in before or they have since signed out.")
                } else {
                    print("\(error.localizedDescription)")
                    self.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
                }
                return
            }
            guard error == nil else { return }
            guard let user = user else { return }

            let email = user.profile?.email

            let fullName = user.profile?.name ?? "india"
            let givenName = user.profile?.givenName ?? "india"
            let familyName = user.profile?.familyName ?? "india"

            let profilePicUrl = user.profile?.imageURL(withDimension: 320)
            let userId = user.userID ?? user.authentication.idToken ?? user.profile?.givenName ?? user.profile?.email ?? ""
            let idToken = user.authentication.idToken
//            print(emailAddress!,fullName as Any,givenName as Any,familyName as Any,profilePicUrl!)
            
            self.social_login(social_id: userId, email: email ?? "\(givenName )@gmail.com", name: fullName , image: nil, social_type: .google)
            
            GIDSignIn.sharedInstance.signOut()
        }
        
        
        
        
//        GIDSignIn.sharedInstance()?.delegate = self
//        GIDSignIn.sharedInstance()?.presentingViewController = self
        
//        // Automatically sign in the user.
//        GIDSignIn.sharedInstance()?.restorePreviousSignIn()
//        GIDSignIn.sharedInstance().signOut()
//        GIDSignIn.sharedInstance()?.signIn()
    }
    
    @IBAction func btnApple(_ sender:UIButton) {
        if #available(iOS 13.0, *) {
            // Dream$Work@2018
            appleSignIn.didTapLoginWithApple1 { (userInfo, message) in
                if let userInfo = userInfo{
                    print(userInfo.email)
                    print(userInfo.userid)
                    print(userInfo.firstName)
                    print(userInfo.lastName)
                    print(userInfo.fullName)
                    let name = userInfo.firstName != "" ? userInfo.firstName : "Test"
                    let email = userInfo.email != "" ? userInfo.email : "\(name)@gmail.com"
                    self.social_login(social_id: userInfo.userid, email: email, name: name, image: nil, social_type: .apple)
                }else if let message = message{
                    print("Error Message: \(message)")
                    self.AlertControllerOnr(title: "Alert", message: "Error Message: \(message)", BtnTitle: "OK")
                }else{
                    print("Unexpected error!")
                    self.AlertControllerOnr(title: "Alert", message: "Unexpected error!", BtnTitle: "OK")
                }
            }
        }else{
            self.AlertControllerOnr(title: "Alert", message: "Sign in with apple feature available with ios 13", BtnTitle: "OK")
        }
    }
}


extension LoginVC {
//    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!,
//              withError error: Error!) {
//        if let error = error {
//            if (error as NSError).code == GIDSignInErrorCode.hasNoAuthInKeychain.rawValue {
//                print("The user has not signed in before or they have since signed out.")
//                self.AlertControllerOnr(title: alertTitle.alert_error, message: "The user has not signed in before or they have since signed out.")
//            } else {
//                print("\(error.localizedDescription)")
//                self.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
//            }
//            return
//        }
//        // Perform any operations on signed in user here.
//        let userId = user.userID ?? user.authentication.idToken ?? user.profile.givenName ?? user.profile.email ?? ""
//        let idToken = user.authentication.idToken // Safe to send to the server
//        let fullName = user.profile.name ?? "india"
//        let givenName = user.profile.givenName ?? "India"
//        let familyName = user.profile.familyName ?? "india"
//        let email = user.profile.email ?? "\(givenName)@gmail.com"
//        // ...
//        self.social_login(social_id: userId, email: email, name: fullName, image: nil, social_type: .google)
//        GIDSignIn.sharedInstance().signOut()
//    }
//
//    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!,
//              withError error: Error!) {
//        // Perform any operations when the user disconnects from app here.
//        // ...
//    }
    
    @IBAction func didTapSignOut(_ sender: AnyObject) {
        GIDSignIn.sharedInstance.signOut()
    }
    
}

extension LoginVC {
    
    func loginManagerDidComplete(_ result: LoginResult) {
        switch result {
        case .cancelled:
            break
        case .failed(let error):
            self.AlertControllerOnr(title: "Login Fail", message: "Login failed with error \(error)")
        case .success( _, _,let tokenD):
            let ree = GraphRequest(graphPath: "me", parameters: ["fields":"id,name,email,first_name,last_name,picture"], tokenString: tokenD.tokenString, version: nil, httpMethod: .get)
            //id,name,email,first_name
            ree.start { (connection, result, error) in
                if(error == nil) {
                    print("result \(result!)")
                    let newDataFacebook = result as! NSDictionary
                    let idToken = newDataFacebook["id"] as! String
                    let fullname = newDataFacebook["name"] as! String
                    let email = newDataFacebook["email"] as? String ?? "\(idToken)@gmail.com"
                    
                    if let picture = newDataFacebook["picture"] as? NSDictionary,let data = picture["data"] as? NSDictionary, let url = data["url"] as? String {
                        let url = URL(string:url)
                        if let dataimg = try? Data(contentsOf: url!) {
                            self.social_login(social_id: idToken, email: email, name: fullname, image: dataimg, social_type: .facebook)
                          
                           
                        }else{
                            self.social_login(social_id: idToken, email: email, name: fullname, image: nil, social_type: .facebook)
                           
                          
                        }
                    }else{
                        self.social_login(social_id: idToken, email: email, name: fullname, image: nil, social_type: .facebook)
                       
                     
                    }
                    self.loginManager.logOut()
                } else {
                    print("error \(String(describing: error))")
                    self.AlertControllerOnr(title: alertTitle.alert_error, message: error?.localizedDescription)
                }
            }
        }
    }
}

enum social_type : String {
    case facebook = "facebook"
    case google = "google"
    case apple = "apple"
}
