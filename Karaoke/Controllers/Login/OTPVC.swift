//
//  OTPVC.swift
//  SNMS
//
//  Created by YATIN  KALRA on 24/08/20.
//  Copyright © 2020 Dhakar. All rights reserved.
//

import UIKit

class OTPVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var firstTxt: UITextField!
    @IBOutlet weak var secondTxt: UITextField!
    @IBOutlet weak var thirdTxt: UITextField!
    @IBOutlet weak var fourtxt: UITextField!
    
    @IBOutlet weak var v1: UIView!
    @IBOutlet weak var v2: UIView!
    @IBOutlet weak var v3: UIView!
    @IBOutlet weak var v4: UIView!
    
    typealias selectBtn = (String,Int) -> Void
    var select : selectBtn? = nil
    var Otp = ""
    var txt_Email = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
        if txt_Email.contains("@")  {
            lblMessage.text = "We have sent a one-time verification code to your email please enter below"
        }else{
            lblMessage.text = "We have sent a one-time verification code to your phone please enter below"
        }
        print(self.Otp,"<== otp")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.AlertControllerOnr(title: "OTP-" + Otp, message: nil, BtnTitle: "OK")
    }
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: true, completion: {
            self.select!("s",0)
        })
    }
    @IBAction func backSubmitAction(_ sender: UIButton) {
        self.backSubmitAction1()
    }
    @IBAction func backResendAction(_ sender: UIButton) {
        self.resend()
    }
    func backSubmitAction1() {
        let otp = "\((firstTxt?.text)!)\((secondTxt?.text)!)\((thirdTxt?.text)!)\((fourtxt?.text)!)"
        if "" == otp {
            CommonFunctions.showToastWithMessage(MessageString.enterOTP)
            return
        }
        if self.Otp == otp {
            self.dismiss(animated: true, completion: {
                self.select!("s",1)
            })
        }else{
            CommonFunctions.showToastWithMessage(MessageString.validOTP)
        }
    }
    /// Initial Setup
    private func initialSetup() {
        firstTxt.delegate = self
        secondTxt.delegate = self
        thirdTxt.delegate = self
        fourtxt.delegate = self
        firstTxt.keyboardType = .numberPad
        secondTxt.keyboardType = .numberPad
        thirdTxt.keyboardType = .numberPad
        fourtxt.keyboardType = .numberPad
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range:NSRange, replacementString string: String) -> Bool {
         // Range.length == 1 means,clicking backspace
         let lightGry = UIColor.init(red: 216.0/255.0, green: 1.0/255.0, blue: 0.0/255.0, alpha: 1).cgColor
         let darkRed = UIColor.init(red: 216.0/255.0, green: 1.0/255.0, blue: 0.0/255.0, alpha: 1).cgColor
         
         if (range.length == 0){
             if textField == firstTxt {
                 secondTxt?.becomeFirstResponder()
                firstTxt.layer.borderColor = lightGry
             }
             
             if textField == secondTxt {
                 thirdTxt?.becomeFirstResponder()
                 secondTxt.layer.borderColor =  lightGry
             }
             if textField == thirdTxt {
                 fourtxt?.becomeFirstResponder()
                 thirdTxt.layer.borderColor =  lightGry
             }
             if textField == fourtxt {
                 fourtxt?.resignFirstResponder()
                 fourtxt.layer.borderColor =  lightGry
                 let otp = "\((firstTxt?.text)!)\((secondTxt?.text)!)\((thirdTxt?.text)!)\((fourtxt?.text)!)"
                 debugPrint(otp)
             }
             textField.text? = string
            resetPlaceHolder()
             return false
         }else if (range.length == 1) {
             
             if textField == fourtxt {
                 thirdTxt?.becomeFirstResponder()
                 fourtxt.layer.borderColor =  darkRed
             }
             if textField == thirdTxt {
                 secondTxt?.becomeFirstResponder()
                 thirdTxt.layer.borderColor =  darkRed
             }
             if textField == secondTxt {
                 firstTxt?.becomeFirstResponder()
                 secondTxt.layer.borderColor =  darkRed
             }
             if textField == firstTxt {
                 firstTxt?.resignFirstResponder()
                 firstTxt.layer.borderColor =  darkRed
             }
             textField.text? = ""
            resetPlaceHolder()
             return false
         }
        resetPlaceHolder()
         return true
     }
    func resetPlaceHolder()  {
        v1.isHidden = firstTxt.text == "" ? false : true
        v2.isHidden = secondTxt.text == "" ? false : true
        v3.isHidden = thirdTxt.text == "" ? false : true
        v4.isHidden = fourtxt.text == "" ? false : true
    }
    func resend() {
        let sendE = txt_Email
        var para : JSONDictionary = [:]
        if sendE.contains("@")  {
            para[ApiKey.email] = sendE
        }else{
            para[ApiKey.mobile] = sendE
        }
        WebServices.commonPostAPI(parameters: para, endPoint: .send_otp, loader: true) { (json) in
            if let dict = json.dictionaryObject {
                if let status = dict["status"] as? Bool, status == true, let otp = dict["otp"] as? String {
                    self.Otp = otp
                    self.AlertControllerOnr(title: alertTitle.alert_success, message: "One Time Verification code has been sent to your entered email/phone.", BtnTitle: "OK")
//                    self.AlertControllerOnr(title: "OTP-" + otp, message: "One Time Verification code has been sent to your entered email/phone.", BtnTitle: "OK")
                }else{
                    if let message = dict["message"] as? String {
                        self.AlertControllerOnr(title: alertTitle.alert_error, message: message, BtnTitle: "OK")
                    }
                }
            }
        } successData: { (data) in
        } failure: { (error) -> (Void) in
            print(error)
        }
    }
}
