//
//  CreateProfileVC.swift
//  Karaoke
//
//  Created by Ankur  on 05/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class CreateProfileVC: UIViewController {
    
    @IBOutlet weak var imgProfile:UIImageView!
    @IBOutlet weak var txt_bio: UITextView!
    @IBOutlet weak var txt_name:UITextField!
    @IBOutlet weak var txt_username:UITextField!
    @IBOutlet weak var txt_email:UITextField!
    var txt_passwordStr = ""
    var txt_nameStr = ""
    var txt_usernameStr = ""
    var txt_emailStr = ""
    @IBOutlet weak var btn_back:UIButton!
    @IBOutlet weak var btn_skip:UIButton!
    @IBOutlet weak var btn_submit:UIButton!
    var txt_bio_placeholdr = ""
    var imagePicker1: ImagePicker!
    var imageaAtt : imageArray?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        txt_bio.delegate = self
        imagePicker1 = ImagePicker(presentationController: self, delegate: self)
        
        txt_name.text = txt_nameStr
        txt_username.text = txt_usernameStr
        txt_email.text = txt_emailStr
        
        self.txt_username.tag = 1
        self.txt_email.tag = 1
        self.txt_username.isEnabled = true
        self.txt_email.isEnabled = false
        self.txt_bio_placeholdr = txt_bio.text!
    }
    func enableBtns(isEnable:Bool) {
        btn_back.isEnabled = isEnable
        btn_skip.isEnabled = isEnable
        btn_submit.isEnabled = isEnable
    }
    
    @IBAction func btnSubmit(_ sender:UIButton) {
//        let vc = SelectMusicVC.instantiate(fromAppStoryboard: .Main)
//        self.navigationController?.pushViewController(vc, animated: true)
        guard self.validation() else {
            return
        }
        createProfile()
    }
    
    @IBAction func btnSkip(_ sender:UIButton) {
//        let vc = SelectMusicVC.instantiate(fromAppStoryboard: .Main)
//        self.navigationController?.pushViewController(vc, animated: true)
        createProfile()
    }
    
    @IBAction func btnBack(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    
    @IBAction func btn_takePhoto(_ sender:UIButton) {
              imagePicker1.present(from: sender)
          }
    
}

extension CreateProfileVC : ImagePickerDelegate {
    func didSelect(image: UIImage?, tag: Int) {
        guard let img = image else {
            return
        }
        img.resizeByByte(maxMB: 1) { [self] (data) in
            imageaAtt = imageArray(image: img, data: data, url: nil, image_id: nil)
        }
        self.imgProfile.image = image
    }
}

extension CreateProfileVC : UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txt_bio.text == self.txt_bio_placeholdr {
            txt_bio.text = ""
//         txt_chat.textColor = .white
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return true
    }
    //    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
    //
    //        return true
    //    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == ""{
            textView.text = txt_bio_placeholdr
            //   textView.textColor = .lightGray
        }
    }
    func createProfile() {
        
//        name, user_name, mobile, email, password, fcm_token, bio, user_image
//        URL- http://karaoke.yilstaging.com/api/register
        let bio : String = txt_bio.text == txt_bio_placeholdr ? "" : txt_bio.text!
        
        let token = UserDefaults.standard.object(forKey: "fcm_token") as? String ?? "123456"
        let para : JSONDictionary = [ApiKey.name:txt_name.text!,
                                     ApiKey.user_name:txt_username.text!,
                                     ApiKey.mobile:"",
                                     ApiKey.email:txt_email.text!,
                                     ApiKey.password:txt_passwordStr,
                                     ApiKey.fcm_token:token,
                                     ApiKey.bio:bio]
        var pmgPar :[UploadFileParameter] = []
        
        if let da = imageaAtt?.data {
            let name = Date.getCurrentDateForName() + ".png"
            pmgPar.append(UploadFileParameter(fileName: name, key: ApiKey.user_image, data: da, mimeType: ".png"))
        }
        
        WebServices.commonPostAPI_data(parameters: para, files: pmgPar, endPoint: .register_user, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            if let status = dict["status"] as? String, status == "success", let user_id = dict["user_id"] as? Int {
                UserDetail.shared.setUserId("\(user_id)")
                let vc = SelectMusicVC.instantiate(fromAppStoryboard: .Main)
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                let message = dict["message"] as? String ?? ""
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: message, BtnTitle: ["OK"]) { (str) in
                    
                }
            }
        } successData: { (data) in
            
        } progress: { (data) in
        
        }failure: { (error) -> (Void) in
            print(error)
            self.AlertControllerOnr(title: alertTitle.alert_error, message: error.localizedDescription)
        }
    }
    
    func validation() -> Bool {
        if self.imageaAtt?.data == nil  {
            CommonFunctions.showToastWithMessage(MessageString.addPhoto)
            return false
        }
        if self.txt_name.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.name)
            return false
        }
        if self.txt_username.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.username)
            return false
        }
        if self.txt_username.tag != 1  {
            CommonFunctions.showToastWithMessage(MessageString.username_already)
            return false
        }
        if self.txt_email.text == ""  {
            CommonFunctions.showToastWithMessage(MessageString.emailorMobile)
            return false
        }
        if !self.isValidEmail(testStr: self.txt_email.text!) {
            CommonFunctions.showToastWithMessage(MessageString.validEmail)
            return false
        }
        
        if self.txt_email.tag != 1  {
            CommonFunctions.showToastWithMessage(MessageString.Email_already_exists)
            return false
        }
        if self.txt_bio.text == "" || self.txt_bio.text == self.txt_bio_placeholdr {
            CommonFunctions.showToastWithMessage(MessageString.bio)
            return false
        }
        return true
    }
    
    func check_username() {
        if txt_username.text == "" {
            return
        }
        let para : JSONDictionary = [ApiKey.username:txt_username.text!]
        WebServices.commonPostAPI(parameters: para, endPoint: .check_username_is_valid, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            let status = dict["status"] as? String ?? ""
            if status == "success" {
                self.txt_username.tag = 1
            }else if status == "success",let suggested = dict["suggested"] as? String {
                let msg = MessageString.username_already + "\n\nsuggested username is \"\(suggested)\""
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: msg, BtnTitle: ["Use suggested","Use another"]) { (str) in
                    if str != "Use suggested" {
                        self.txt_username.text = suggested
                        self.txt_username.tag = 1
                    }else{
                        self.txt_username.becomeFirstResponder()
                        self.txt_username.tag = 0
                    }
                }
            }else {
                self.txt_username.tag = 0
                self.AlertControllerCuston(title: alertTitle.alert_error, message: "Something went wrong please login again", BtnTitle: ["OK"], completion: { (_) in
                })
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    func check_email() {
        if txt_email.text == "" {
            return
        }
        let para : JSONDictionary = [ApiKey.email:txt_email.text!]
        WebServices.commonPostAPI(parameters: para, endPoint: .check_email, loader: true) { (json) in
            guard let dict = json.dictionaryObject else{
                return
            }
            let status = dict["status"] as? String ?? ""
            if status == "success" {
                self.txt_email.tag = 1
            }else {
                self.txt_email.tag = 0
                self.AlertControllerCuston(title: alertTitle.alert_alert, message: MessageString.Email_already_exists, BtnTitle: ["OK"]) { (str) in
                    self.txt_email.becomeFirstResponder()
                }
            }
        } successData: { (data) in
        } failure: { (err) -> (Void) in
            print(err.localizedDescription)
        }
    }
    @IBAction func editEmail(_ textField: UITextField) {
        if textField.text == "" {
           return
        }
        if textField == txt_username {
            self.check_username()
        }
        if textField == txt_email {
            self.check_email()
        }
    }
}
