//
//  SeeAllVC.swift
//  Karaoke
//
//  Created by Ankur  on 23/11/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit

class SeeAllVC: UIViewController {
    
    @IBOutlet weak var table:UITableView!
    @IBOutlet weak var lblHeader:UILabel!
    var Str:String = ""
    var musicCat : musicCategory = .recomended
    let viewModel = SeeAllVM()
    var page = 0
    var shouldShowLoadingCell = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Setup()
        viewModel.vc = self
        viewModel.explore_screen(type: musicCat, page: 0, min_range: 0, max_range: 5)
        
        lblHeader.text = Str
//        tabBarController?.tabBar.isHidden = true
    }
    func updatedata(new:Bool)  {
        
        self.table.reloadData()
    }
    func Setup(){
        
        self.table.register(UINib(nibName: "SongsBookCell", bundle: nil), forCellReuseIdentifier: "SongsBookCell")
        
        table.delegate = self
        table.dataSource = self
    }
    
    @IBAction func btn_Back(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension SeeAllVC :UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = viewModel.top_artist_videos.count
        return shouldShowLoadingCell ? count + 1 : count
//        return self.viewModel.top_artist_videos.count
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 110
//    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if isLoadingIndexPath(indexPath) {
            return LoadingCell(style: .default, reuseIdentifier: "loading")
        } else {
            let cell = table.dequeueReusableCell(withIdentifier: "SongsBookCell", for: indexPath) as! SongsBookCell
            let dict = viewModel.top_artist_videos[indexPath.row]
            cell.viewCon = self
            cell.updateData = dict
            
            cell.btn_View.tag = indexPath.row
            cell.btn_Sing.tag = indexPath.row
            cell.btn_share.tag = indexPath.row
            cell.btn_Sing.addTarget(self, action: #selector(btnSing(_:)), for: .touchUpInside)
            cell.btn_View.addTarget(self, action: #selector(btnView(_:)), for: .touchUpInside)
            cell.btn_share.addTarget(self, action: #selector(btnShare(_:)), for: .touchUpInside)
            return cell
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard isLoadingIndexPath(indexPath) else { return }
        getMoreImages(page: page)
//        if viewModel.notification_list.count == indexPath.row && viewModel.notification_list.count%5 == 0{
//           getMoreImages(page: page)
//        }
    }
    func getMoreImages(page:Int){
        let dict = viewModel.top_artist_videos.count + 1
        self.viewModel.explore_screen(type: musicCat, page: 0, min_range: dict, max_range: dict + 4)
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldShowLoadingCell else { return false }
        return indexPath.row == self.viewModel.top_artist_videos.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict1 = viewModel.top_artist_videos[indexPath.row]
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSing(_ sender:UIButton) {
        
        let dict1 = viewModel.top_artist_videos[sender.tag]
        let vc = SingItVC.instantiate(fromAppStoryboard: .Home)
        vc.lyrics = dict1.lyrics ?? ""
        vc.song_name = dict1.song_name ?? ""
        vc.music_cat_id = dict1.music_cat_id ?? ""
        vc.artist_name = dict1.artist_name ?? ""
        vc.old_video_id = dict1.video_id ?? ""
        vc.select = { [weak self] (str,status) in
            if status == 1 {
                self?.tabBarController?.selectedIndex = 0
//                self?.navigationController?.removeControllers(.all) { $0.isKind(of: SeeAllVM.self) }
            }
            if status == 2 {
                self?.tabBarController?.selectedIndex = 4
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                    NotificationCenter.default.post(name: NSNotification.Name("SubscriptionPlanVC"), object: nil)
                }
            }
        }
        vc.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func btnView(_ sender:UIButton) {
        guard let dict1 = viewModel.top_artist_videos[sender.tag].video_id else{
            return
        }
        let vc = SongsInfoVC.instantiate(fromAppStoryboard: .Home)
        vc.status = 0
        vc.video_id = dict1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnShare(_ sender:UIButton) {
        let dict = viewModel.top_artist_videos[sender.tag]
        let loading = LoadingView()
        loading.showActivityLoading(uiView: self.view,color: .black)
        SharePost.shared.generateContentLink1(video_name: dict.song_name ?? "Karaoke", video_id: dict.video_id ?? "9", video_url: dict.video_file ?? "", video_image: dict.video_thumb ?? "") { (url) in
            if url != nil {
                
                let activityVC = UIActivityViewController(activityItems: [url!], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                    loading.hideActivityLoading(uiView: self.view)
                    if !completed {
                        
                    }
                }
                self.present(activityVC, animated: true, completion: nil)
            }else{
                loading.hideActivityLoading(uiView: self.view)
            }
        }
    }    
}
