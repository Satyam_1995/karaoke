//
//  AppDelegate.swift
//  Karaoke
//
//  Created by Ankur  on 30/10/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import GoogleSignIn
import FBSDKCoreKit
import IQKeyboardManagerSwift
import CoreLocation
import Firebase
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes
import DropDown


//email - yesitlabs@gmail.com
//Pass - AppleUSA@$$2034


var videoDetailCache : [String:Video_detail] = [:]
//Client com.voicetm.karaoke1
//office Yesitlabs.Karaoke
//var firebaseToken = "123456"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,CrashesDelegate {
    var seenError = false
    var locationFixAchieved = false
    var locationManager = CLLocationManager()
    
    var window: UIWindow?
    static let shared = UIApplication.shared.delegate as! AppDelegate
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
//        GIDSignIn.sharedInstance()?.clientID = "1064307263446-m9c03jdlrqg91qhtsci0d3ca9ho1em71.apps.googleusercontent.com"
        Crashes.enabled = true
        AppCenter.start(withAppSecret: "5740fd63-6b03-4fd5-bc7f-42682b05b6f8", services: [Analytics.self, Crashes.self])
        let enabled = Crashes.enabled
        print("here\(enabled)")
        UserDefaults.standard.set(0, forKey: "start_singing")
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
//        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "com.Yesitlabs.Karaoke"
        FirebaseOptions.defaultOptions()?.deepLinkURLScheme = "com.voicetm.karaoke"

        FirebaseApp.configure()
        self.configureNotification()
//        InAppPurchaseManager.sharedManager.start(withHandler: DemoTransactionHandler())
        DropDown.startListeningToKeyboard()
        sleep(3)
        return true
    }
    func configureNotification() {
        let notificationDelegate = SampleNotificationDelegate()
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in }
            center.delegate = notificationDelegate
            let openAction = UNNotificationAction(identifier: "OpenNotification", title: NSLocalizedString("Abrir", comment: ""), options: UNNotificationActionOptions.foreground)
            let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [openAction], intentIdentifiers: [], options: [])
            center.setNotificationCategories(Set([deafultCategory]))
        } else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
        Messaging.messaging().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        Messaging.messaging().token { token, error in
            if let error = error {
                print("Error fetching FCM registration token: \(error)")
            } else if let token = token {
                print("FCM registration token: \(token)")
                //            self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
//                firebaseToken = token
                UserDefaults.standard.setValue("\(token)", forKey: "fcm_token")
            }
        }
    }
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if ApplicationDelegate.shared.application(app, open: url, options: options) {
            return true
        }else if GIDSignIn.sharedInstance.handle(url) {
            return true
        }else if application(app, open: url,
                             sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                             annotation: "") {
//        }else {
            return application(app, open: url,
                               sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                               annotation: "")
            
        }else{
            let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url)
            if let dynamicLink = dynamicLink {
              let link = dynamicLink.url
              print(link)
              return true
            }
            return false
        }
        
        let facebookF = ApplicationDelegate.shared.application(app, open: url, options: options)
        let isGoogleOpenUrl = GIDSignIn.sharedInstance.handle(url)
        
        //        return ApplicationDelegate.shared.application(app, open: url, options: options)
    }
    func application(_ application: UIApplication,
                     open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
          // Handle the deep link. For example, show the deep-linked content or
          // apply a promotional offer to the user's account.
          // ...
          return true
        }
        return GIDSignIn.sharedInstance.handle(url)
    }
    
}

extension AppDelegate : CLLocationManagerDelegate {
    func initLocationManager() {
        seenError = false
        locationFixAchieved = false
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest

        locationManager.requestAlwaysAuthorization()

    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if (locationFixAchieved == false) {
            locationFixAchieved = true
            let locationArray = locations as NSArray
            let locationObj = locationArray.lastObject as! CLLocation
            let coord = locationObj.coordinate
            UserDefaults.standard.setValue(String(coord.latitude), forKey: "latitude")
            UserDefaults.standard.setValue(String(coord.longitude), forKey: "longitude")
            locationManager.stopUpdatingLocation()
        }
        CLGeocoder().reverseGeocodeLocation(manager.location!, completionHandler: {(placemarks, error)->Void in
            if (error != nil) {
                print("Reverse geocoder failed with error" + error!.localizedDescription)
                return
            }

            if placemarks!.count > 0 {
                let pm = placemarks![0] as CLPlacemark
                self.displayLocationInfo(placemark: pm)
            } else {
                print("Problem with the data received from geocoder")
            }
        })
    }

    func displayLocationInfo(placemark: CLPlacemark) {
        //stop updating location to save battery life
        locationManager.stopUpdatingLocation()
//        print(placemark.locality )
//        print(placemark.postalCode )
//        print(placemark.administrativeArea )
//        print(placemark.country )
        UserDefaults.standard.setValue(placemark.locality, forKey: "city")
        UserDefaults.standard.setValue(placemark.administrativeArea, forKey: "state")
    }


    // authorization status
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        var shouldIAllow = false
        var locationStatus = ""

        switch status {
        case .notDetermined:
            locationStatus = "Status not determined"
        case .restricted:
            locationStatus = "Restricted Access to location"
        case .denied:
            locationStatus = "User denied access to location"
        case .authorizedAlways:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        case .authorizedWhenInUse:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        @unknown default:
            locationStatus = "Allowed to location Access"
            shouldIAllow = true
        }
        NotificationCenter.default.post(name: NSNotification.Name("LabelHasbeenUpdated"), object: nil)
        if (shouldIAllow == true) {
            print("Location to Allowed")
            // Start location services
            locationManager.startUpdatingLocation()
        } else {
            print("Denied access: \(locationStatus)")
        }
    }
}
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String!) {
      print("Firebase registration token: \(String(describing: fcmToken))")
        
        UserDefaults.standard.setValue("\(fcmToken)", forKey: "fcm_token")
//        firebaseToken = fcmToken
//        singletonClass.createsingleton()?.firebaseToken = fcmToken
        let dataDict:[String: String] = ["token": fcmToken ]
      NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
      // TODO: If necessary send token to application server.
      // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      Messaging.messaging().apnsToken = deviceToken
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
        guard (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) != nil else{
            return
        }
        print(dynamiclink as Any,dynamiclink?.url as Any)
        
        guard let link = dynamiclink?.url else{ return }
        let components = URLComponents(url: link, resolvingAgainstBaseURL: false)
        
        if components?.path == "/video" {
            guard let queryItems = components?.queryItems else{ return }
            let video_id = queryItems.filter({$0.name == "video_id"})
            let video_url = queryItems.filter({$0.name == "video_url"})
            let vc = OtherUserVideoVC.instantiate(fromAppStoryboard: .Home)
            vc.hidesBottomBarWhenPushed = true
            vc.videoURL = video_url.first?.value ?? ""
            vc.video_id = video_id.first?.value ?? "9"
            (self.window?.rootViewController as? UINavigationController)?.pushViewController(vc, animated: true)
        }
      }
      return handled
    }
}

//https://sdks.support.brightcove.com/ios/basics/step-step-simple-video-app-using-swift.html
//
//https://cocoapods.org/pods/BMPlayer
//
//https://nexplayersdk.com/ios-player-sdk/
//
//https://github.com/josejuanqm/VersaPlayer
//
