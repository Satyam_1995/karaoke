

import Foundation
struct FavouriteModel : Codable {
	let message : String?
	let fav_list : [Video_list]?
	let status : String?

	enum CodingKeys: String, CodingKey {

		case message = "message"
		case fav_list = "fav_list"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		fav_list = try values.decodeIfPresent([Video_list].self, forKey: .fav_list)
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}
struct Fav_list : Codable {
    let total_reported : String?
    let artist_name : String?
    let video_id : String?
    let song_name : String?
    let video_file : String?
    let total_comments : String?
    let created_on : String?
    let total_likes : String?
    let video_status : String?
    let user_name : String?
    let music_cat_id : String?
    let user_id : String?
    let video_thumb : String?
    let user_profile : String?
    let lyrics : String?

    enum CodingKeys: String, CodingKey {

        case total_reported = "total_reported"
        case artist_name = "artist_name"
        case video_id = "video_id"
        case song_name = "song_name"
        case video_file = "video_file"
        case total_comments = "total_comments"
        case created_on = "created_on"
        case total_likes = "total_likes"
        case video_status = "video_status"
        case user_name = "user_name"
        case music_cat_id = "music_cat_id"
        case user_id = "user_id"
        case video_thumb = "video_thumb"
        case user_profile = "user_profile"
        case lyrics = "lyrics"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
        video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
        total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        music_cat_id = try values.decodeIfPresent(String.self, forKey: .music_cat_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
    }

}
