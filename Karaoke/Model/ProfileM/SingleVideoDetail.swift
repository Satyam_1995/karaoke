

import Foundation
struct SingleVideoDetail : Codable {
    let video_detail : [Video_detail]?
    let status : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case video_detail = "video_list"
        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        video_detail = try values.decodeIfPresent([Video_detail].self, forKey: .video_detail)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}

struct Video_detail : Codable {
	let video_id : String?
	let user_id : String?
	let video_file : String?
	let lyrics : String?
	let artist_name : String?
	let song_name : String?
	let video_thumb : String?
	let created_on : String?
	let total_likes : String?
	let total_comments : String?
	let total_reported : String?
	let video_status : String?
	let user_name : String?
	let user_profile : String?
    var user_fav_status : Int = 0
    var user_like_status : Int = 0
    let music_cat_id : String?
    let main_video_file, main_video_id : String?
    
	enum CodingKeys: String, CodingKey {

		case video_id = "video_id"
		case user_id = "user_id"
		case video_file = "video_file"
		case lyrics = "lyrics"
		case artist_name = "artist_name"
		case song_name = "song_name"
		case video_thumb = "video_thumb"
		case created_on = "created_on"
		case total_likes = "total_likes"
		case total_comments = "total_comments"
		case total_reported = "total_reported"
		case video_status = "video_status"
		case user_name = "user_name"
		case user_profile = "user_profile"
		case user_fav_status = "user_fav_status"
        case user_like_status, music_cat_id, main_video_file, main_video_id
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
		user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
		video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
		lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
		artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
		song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
		video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
		created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
		total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
		total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
		total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
		video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
		user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
		user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        user_fav_status = try values.decodeIfPresent(Int.self, forKey: .user_fav_status) ?? 0
        user_like_status = try values.decodeIfPresent(Int.self, forKey: .user_like_status) ?? 0
        music_cat_id = try values.decodeIfPresent(String.self, forKey: .music_cat_id)
        
        main_video_file = try values.decodeIfPresent(String.self, forKey: .main_video_file)
        main_video_id = try values.decodeIfPresent(String.self, forKey: .main_video_id)
	}
}
