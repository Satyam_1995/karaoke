

import Foundation
struct SubscriptionModel : Codable {
	let status : String?
//	let myplan_details : [Subscription_plan]?
    let myplan_details : [Myplan_details]?
    
	let message, current_date : String?
	let subscription_plan : [Subscription_plan]?

    enum CodingKeys: String, CodingKey {

		case status = "status"
		case myplan_details = "myplan_details"
		case message = "message"
		case subscription_plan = "subscription_plan"
        case current_date
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
//		myplan_details = try values.decodeIfPresent([Subscription_plan].self, forKey: .myplan_details)
        myplan_details = try values.decodeIfPresent([Myplan_details].self, forKey: .myplan_details)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		subscription_plan = try values.decodeIfPresent([Subscription_plan].self, forKey: .subscription_plan)
        current_date = try values.decodeIfPresent(String.self, forKey: .current_date)
        
	}
}

struct Subscription_plan : Codable {
    let plan_status : String?
    let plan_swipes : String?
    let plan_image : String?
    let plan_details : String?
    let plan_id : String?
    let plan_name : String?
    let plan_amount : String?
    let apple_product_id : String?
    enum CodingKeys: String, CodingKey {

        case plan_status = "plan_status"
        case plan_swipes = "plan_swipes"
        case plan_image = "plan_image"
        case plan_details = "plan_details"
        case plan_id = "plan_id"
        case plan_name = "plan_name"
        case plan_amount = "plan_amount"
        case apple_product_id
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        plan_status = try values.decodeIfPresent(String.self, forKey: .plan_status)
        plan_swipes = try values.decodeIfPresent(String.self, forKey: .plan_swipes)
        plan_image = try values.decodeIfPresent(String.self, forKey: .plan_image)
        plan_details = try values.decodeIfPresent(String.self, forKey: .plan_details)
        plan_id = try values.decodeIfPresent(String.self, forKey: .plan_id)
        plan_name = try values.decodeIfPresent(String.self, forKey: .plan_name)
        plan_amount = try values.decodeIfPresent(String.self, forKey: .plan_amount)
        apple_product_id = try values.decodeIfPresent(String.self, forKey: .apple_product_id)
        
    }
}
struct Myplan_details: Codable {
    let transaction_token, transaction_id, amount, planid: String
    let expire_on,createdat, userid, status: String
    enum CodingKeys: String, CodingKey {
        case transaction_token = "transaction_token"
        case transaction_id = "transaction_id"
        case amount, planid, createdat, userid, expire_on,status
    }
}
