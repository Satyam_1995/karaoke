//
//  BlockListModel.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 08/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation

struct BlockListModel : Codable {
    
    let message : String?
    let status : String?
    let data : [BlockListUser]?
    
    enum CodingKeys : String, CodingKey {
        case message
        case status
        case data = "data"
    }
    init(from deroder: Decoder) throws {
        let value = try deroder.container(keyedBy: CodingKeys.self)
        message = try value.decodeIfPresent(String.self, forKey: .message)
        status = try value.decodeIfPresent(String.self, forKey: .status)
        data = try value.decodeIfPresent([BlockListUser].self, forKey: .data)
    }
}
struct BlockListUser : Codable {
    
    let block_id : String?
    let userid : String?
    let followerid : String?
    let block_status : String?
    let date_added : String?
    let name : String?
    let user_profile : String?
    var total_follower : Int = 0
    
    
    enum CodingKeys : String, CodingKey {
        case block_id
        case userid
        case followerid
        case block_status
        case date_added
        case name
        case user_profile
        case total_follower
    }
    init(from deroder:Decoder) throws {
        let value = try deroder.container(keyedBy: CodingKeys.self)
        block_id = try value.decodeIfPresent(String.self, forKey: .block_id)
        userid = try value.decodeIfPresent(String.self, forKey: .userid)
        followerid = try value.decodeIfPresent(String.self, forKey: .followerid)
        block_status = try value.decodeIfPresent(String.self, forKey: .block_status)
        date_added = try value.decodeIfPresent(String.self, forKey: .date_added)
        name = try value.decodeIfPresent(String.self, forKey: .name)
        user_profile = try value.decodeIfPresent(String.self, forKey: .user_profile)
        total_follower = try value.decodeIfPresent(Int.self, forKey: .total_follower) ?? 0
    }
}
