

import Foundation
struct FriendProfileData : Codable {
    var data : Friend_Data?
	let status : String?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case data = "data"
		case status = "status"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		data = try values.decodeIfPresent(Friend_Data.self, forKey: .data)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}
struct Friend_Data : Codable {
    var user_follow_status : Int?
    let friend_follow_status : Int?
    let user_profile : String?
    let friend_block_status : Int?
    let friend_id : String?
    var user_block_status : Int?
    let total_follower : Int?
    let total_following : Int?
    let name : String?
    let user_name : String?
    let profile_img : String?
    let user_bio : String?
    
    enum CodingKeys: String, CodingKey {

        case user_follow_status = "user_follow_status"
        case friend_follow_status = "friend_follow_status"
        case user_profile = "user_profile"
        case friend_block_status = "friend_block_status"
        case friend_id = "friend_id"
        case user_block_status = "user_block_status"
        case total_follower = "total_follower"
        case user_name = "user_name"
        case total_following
        case name
        case profile_img
        case user_bio
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status)
        friend_follow_status = try values.decodeIfPresent(Int.self, forKey: .friend_follow_status)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        friend_block_status = try values.decodeIfPresent(Int.self, forKey: .friend_block_status)
        friend_id = try values.decodeIfPresent(String.self, forKey: .friend_id)
        user_block_status = try values.decodeIfPresent(Int.self, forKey: .user_block_status)
        total_follower = try values.decodeIfPresent(Int.self, forKey: .total_follower)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        total_following = try values.decodeIfPresent(Int.self, forKey: .total_following)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        profile_img = try values.decodeIfPresent(String.self, forKey: .profile_img)
        user_bio = try values.decodeIfPresent(String.self, forKey: .user_bio)        
        
    }

}
