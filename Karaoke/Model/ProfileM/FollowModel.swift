

import Foundation
struct FollowModel : Codable {
	let message : String?
    var follower : [Follower]  = []
//    var following : [Following] = []
    var following : [Follower] = []
	let status : String?

	enum CodingKeys: String, CodingKey {

		case message = "message"
		case follower = "follower"
		case following = "following"
		case status = "status"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		message = try values.decodeIfPresent(String.self, forKey: .message)
        follower = try values.decodeIfPresent([Follower].self, forKey: .follower) ?? []
		following = try values.decodeIfPresent([Follower].self, forKey: .following) ?? []
		status = try values.decodeIfPresent(String.self, forKey: .status)
	}

}
struct Following : Codable {
    let user_name : String?
    let follow_status : String?
    let friend_block_status : Int?
    let friend_id : String?
//    let userid : String?
    let total_follower : Int?
    let user_block_status : Int?
    let user_profile : String?
    let following_id : String?
    var friend_follow_status : Int = 0
    var user_follow_status : Int = 0

    enum CodingKeys: String, CodingKey {

        case user_name = "user_name"
        case follow_status = "follow_status"
        case friend_block_status = "friend_block_status"
        case friend_id = "friend_id"
//        case userid = "userid"
        case total_follower = "total_follower"
        case user_block_status = "user_block_status"
        case user_profile = "user_profile"
        case following_id = "following_id"
        case friend_follow_status = "friend_follow_status"
        case user_follow_status = "user_follow_status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        follow_status = try values.decodeIfPresent(String.self, forKey: .follow_status)
        friend_block_status = try values.decodeIfPresent(Int.self, forKey: .friend_block_status)
        friend_id = try values.decodeIfPresent(String.self, forKey: .friend_id)
//        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        total_follower = try values.decodeIfPresent(Int.self, forKey: .total_follower)
        user_block_status = try values.decodeIfPresent(Int.self, forKey: .user_block_status)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        following_id = try values.decodeIfPresent(String.self, forKey: .following_id)
        friend_follow_status = try values.decodeIfPresent(Int.self, forKey: .friend_follow_status) ?? 0
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status) ?? 0
    }

}
struct Follower : Codable {
    let user_name : String?
    let name : String?
    let follow_status : String?
    let friend_block_status : Int?
    let friend_id : String?
//    let userid : String?
    let total_follower : Int?
    let user_block_status : Int?
    let user_profile : String?
    let following_id : String?
    var friend_follow_status : Int = 0
    var user_follow_status : Int = 0

    enum CodingKeys: String, CodingKey {

        case user_name = "user_name"
        case follow_status = "follow_status"
        case friend_block_status = "friend_block_status"
        case friend_id = "friend_id"
//        case userid = "userid"
        case total_follower = "total_follower"
        case user_block_status = "user_block_status"
        case user_profile = "user_profile"
        case following_id = "following_id"
        case friend_follow_status = "friend_follow_status"
        case user_follow_status = "user_follow_status"
        case name
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        follow_status = try values.decodeIfPresent(String.self, forKey: .follow_status)
        friend_block_status = try values.decodeIfPresent(Int.self, forKey: .friend_block_status)
        friend_id = try values.decodeIfPresent(String.self, forKey: .friend_id)
//        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        total_follower = try values.decodeIfPresent(Int.self, forKey: .total_follower)
        user_block_status = try values.decodeIfPresent(Int.self, forKey: .user_block_status)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        following_id = try values.decodeIfPresent(String.self, forKey: .following_id)
        friend_follow_status = try values.decodeIfPresent(Int.self, forKey: .friend_follow_status) ?? 0
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status) ?? 0
        name = try values.decodeIfPresent(String.self, forKey: .name)
        
    }

}
