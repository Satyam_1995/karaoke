struct Music_category_all : Codable {
    let music_category : [Music_category]?
    let status : String?
    let message : String?

    enum CodingKeys: String, CodingKey {

        case music_category = "music_category"
        case status = "status"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        music_category = try values.decodeIfPresent([Music_category].self, forKey: .music_category)
        status = try values.decodeIfPresent(String.self, forKey: .status)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}


import Foundation
struct Music_category : Codable {
	let music_category_id : String?
	let category_name : String?
    let category_img : String?

	enum CodingKeys: String, CodingKey {

		case music_category_id = "music_category_id"
		case category_name = "category_name"
        case category_img = "category_img"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		music_category_id = try values.decodeIfPresent(String.self, forKey: .music_category_id)
		category_name = try values.decodeIfPresent(String.self, forKey: .category_name)
        category_img = try values.decodeIfPresent(String.self, forKey: .category_img)
	}

}
