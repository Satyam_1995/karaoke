

import Foundation
struct HomeVideoList : Codable {
	let status : String?
	let message : String?
	let latest_song_list : [Latest_song_list]?
	let video_list : [Video_list]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case latest_song_list = "latest_song_list"
		case video_list = "video_list"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
		latest_song_list = try values.decodeIfPresent([Latest_song_list].self, forKey: .latest_song_list)
		video_list = try values.decodeIfPresent([Video_list].self, forKey: .video_list)
	}

}
struct Latest_song_list : Codable {
    let song_name : String?
    let song_track : String?
    let created_at : String?
    let song_genre : String?
    let song_artist : String?
    let song_id : String?
    let song_thumbnail : String?

    enum CodingKeys: String, CodingKey {

        case song_name = "song_name"
        case song_track = "song_track"
        case created_at = "created_at"
        case song_genre = "song_genre"
        case song_artist = "song_artist"
        case song_id = "song_id"
        case song_thumbnail = "song_thumbnail"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
        song_track = try values.decodeIfPresent(String.self, forKey: .song_track)
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        song_genre = try values.decodeIfPresent(String.self, forKey: .song_genre)
        song_artist = try values.decodeIfPresent(String.self, forKey: .song_artist)
        song_id = try values.decodeIfPresent(String.self, forKey: .song_id)
        song_thumbnail = try values.decodeIfPresent(String.self, forKey: .song_thumbnail)
    }

}
struct Video_list : Codable {
    let user_profile : String?
    let total_likes : String?
    let video_thumb : String?
    let user_name : String?
    let song_name : String?
    var user_fav_status : Int?
    var user_like_status : Int?
    var user_follow_status : Int = 0
    let total_comments : String?
    let user_id : String?
    let video_id : String?
    let created_on : String?
    let artist_name : String?
    let total_reported : String?
    let video_file : String?
    let lyrics : String?
    let video_status : String?
    let music_cat_id : String?
    enum CodingKeys: String, CodingKey {

        case user_profile = "user_profile"
        case total_likes = "total_likes"
        case video_thumb = "video_thumb"
        case user_name = "user_name"
        case song_name = "song_name"
        case user_fav_status = "user_fav_status"
        case total_comments = "total_comments"
        case user_id = "user_id"
        case video_id = "video_id"
        case created_on = "created_on"
        case artist_name = "artist_name"
        case total_reported = "total_reported"
        case video_file = "video_file"
        case lyrics = "lyrics"
        case video_status = "video_status"
        case user_like_status = "user_like_status"
        case user_follow_status,music_cat_id
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
        user_fav_status = try values.decodeIfPresent(Int.self, forKey: .user_fav_status)
        total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
        video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
        lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
        video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
        user_like_status = try values.decodeIfPresent(Int.self, forKey: .user_like_status)
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status) ?? 0
        music_cat_id = try values.decodeIfPresent(String.self, forKey: .music_cat_id)
    }
}
