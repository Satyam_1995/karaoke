

import Foundation

struct CommentModel : Codable {
	let user_comment : [All_comment]?
	let all_comment : [All_comment]?
	let status : String?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case user_comment = "user_comment"
		case all_comment = "all_comment"
		case status = "status"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		user_comment = try values.decodeIfPresent([All_comment].self, forKey: .user_comment)
		all_comment = try values.decodeIfPresent([All_comment].self, forKey: .all_comment)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}

struct All_comment : Codable {
    let comments_id : String?
    let video_id : String?
    let user_id : String?
    let comment : String?
    let is_visible : String?
    let created_on : String?
    let total_likes : String?
    
    let user_name : String?
    let user_profile : String?
    var comments_like_status : Int = 0
    var reply_on_comment : [Reply_on_comment] = []

    enum CodingKeys: String, CodingKey {

        case comments_id = "comments_id"
        case video_id = "video_id"
        case user_id = "user_id"
        case comment = "comment"
        case is_visible = "is_visible"
        case created_on = "created_on"
        case total_likes = "total_likes"
        case reply_on_comment = "reply_on_comment"
        case user_profile = "user_profile"
        case user_name = "user_name"
        case comments_like_status = "comments_like_status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        comments_id = try values.decodeIfPresent(String.self, forKey: .comments_id)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        is_visible = try values.decodeIfPresent(String.self, forKey: .is_visible)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        reply_on_comment = try values.decodeIfPresent([Reply_on_comment].self, forKey: .reply_on_comment) ?? []
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        comments_like_status = try values.decodeIfPresent(Int.self, forKey: .comments_like_status) ?? 0
        
    }
}
//struct User_comment : Codable {
//    let comments_id : String?
//    let video_id : String?
//    let user_id : String?
//    let comment : String?
//    let is_visible : String?
//    let created_on : String?
//    let total_likes : String?
//    let reply_on_comment : [[Reply_on_comment]]?
//    enum CodingKeys: String, CodingKey {
//        case comments_id = "comments_id"
//        case video_id = "video_id"
//        case user_id = "user_id"
//        case comment = "comment"
//        case is_visible = "is_visible"
//        case created_on = "created_on"
//        case total_likes = "total_likes"
//        case reply_on_comment = "reply_on_comment"
//    }
//    init(from decoder: Decoder) throws {
//        let values = try decoder.container(keyedBy: CodingKeys.self)
//        comments_id = try values.decodeIfPresent(String.self, forKey: .comments_id)
//        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
//        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
//        comment = try values.decodeIfPresent(String.self, forKey: .comment)
//        is_visible = try values.decodeIfPresent(String.self, forKey: .is_visible)
//        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
//        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
//        reply_on_comment = try values.decodeIfPresent([[Reply_on_comment]].self, forKey: .reply_on_comment)
//    }
//}
struct Reply_on_comment : Codable {
    let reply_id : String?
    let comment_id : String?
    let user_id : String?
    let comment : String?
    let is_visible : String?
    let created_on : String?
    let total_likes : String?
    let user_profile : String?
    let user_name : String?
    var reply_like_status : Int = 0
    enum CodingKeys: String, CodingKey {
        case reply_id = "reply_id"
        case comment_id = "comment_id"
        case user_id = "user_id"
        case comment = "comment"
        case is_visible = "is_visible"
        case created_on = "created_on"
        case total_likes = "total_likes"
        case user_profile = "user_profile"
        case user_name = "user_name"
        case reply_like_status = "reply_like_status"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        reply_id = try values.decodeIfPresent(String.self, forKey: .reply_id)
        comment_id = try values.decodeIfPresent(String.self, forKey: .comment_id)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        comment = try values.decodeIfPresent(String.self, forKey: .comment)
        is_visible = try values.decodeIfPresent(String.self, forKey: .is_visible)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        reply_like_status = try values.decodeIfPresent(Int.self, forKey: .reply_like_status) ?? 0
    }
}
