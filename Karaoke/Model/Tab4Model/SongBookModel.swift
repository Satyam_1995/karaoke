//
//  SongBookModel.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 28/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
struct SongBookModel : Codable {
    let status : String?
//    let search_result : [Search_result]?
    let search_result : [Popular_videos]?
    
    let message : String?

    enum CodingKeys: String, CodingKey {

        case status = "status"
        case search_result = "search_result"
        case message = "message"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        status = try values.decodeIfPresent(String.self, forKey: .status)
//        search_result = try values.decodeIfPresent([Search_result].self, forKey: .search_result)
        search_result = try values.decodeIfPresent([Popular_videos].self, forKey: .search_result)
        message = try values.decodeIfPresent(String.self, forKey: .message)
    }

}
struct Search_result : Codable {
    let user_fav_status : Int?
    let user_name : String?
    let video_status : String?
    let video_thumb : String?
    let month_views : String?
    let user_profile : String?
    let video_file : String?
    let total_likes : String?
    let lyrics : String?
    let total_sing : String?
    let total_reported : String?
    let total_comments : String?
    let music_cat_id : String?
    let created_on : String?
    let artist_name : String?
    let video_id : String?
    let old_video_id : String?
    let week_likes : String?
    let month_likes : String?
    let user_follow_status : Int?
    let total_views : String?
    let week_views : String?
    let user_like_status : Int?
    let user_id : String?
    let song_name : String?

    enum CodingKeys: String, CodingKey {

        case user_fav_status = "user_fav_status"
        case user_name = "user_name"
        case video_status = "video_status"
        case video_thumb = "video_thumb"
        case month_views = "month_views"
        case user_profile = "user_profile"
        case video_file = "video_file"
        case total_likes = "total_likes"
        case lyrics = "lyrics"
        case total_sing = "total_sing"
        case total_reported = "total_reported"
        case total_comments = "total_comments"
        case music_cat_id = "music_cat_id"
        case created_on = "created_on"
        case artist_name = "artist_name"
        case video_id = "video_id"
        case old_video_id = "old_video_id"
        case week_likes = "week_likes"
        case month_likes = "month_likes"
        case user_follow_status = "user_follow_status"
        case total_views = "total_views"
        case week_views = "week_views"
        case user_like_status = "user_like_status"
        case user_id = "user_id"
        case song_name = "song_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_fav_status = try values.decodeIfPresent(Int.self, forKey: .user_fav_status)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
        video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
        video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
        month_views = try values.decodeIfPresent(String.self, forKey: .month_views)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
        total_sing = try values.decodeIfPresent(String.self, forKey: .total_sing)
        total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
        total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
        music_cat_id = try values.decodeIfPresent(String.self, forKey: .music_cat_id)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        old_video_id = try values.decodeIfPresent(String.self, forKey: .old_video_id)
        week_likes = try values.decodeIfPresent(String.self, forKey: .week_likes)
        month_likes = try values.decodeIfPresent(String.self, forKey: .month_likes)
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status)
        total_views = try values.decodeIfPresent(String.self, forKey: .total_views)
        week_views = try values.decodeIfPresent(String.self, forKey: .week_views)
        user_like_status = try values.decodeIfPresent(Int.self, forKey: .user_like_status)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
    }

}
