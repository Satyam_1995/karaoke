

import Foundation
import AVFoundation
import AVKit

struct Explore_screenVM : Codable {
	let top_artist_videos : [Popular_videos]?
	let recomended_videos : [Popular_videos]?
	let popular_videos : [Popular_videos]?
	let top_singer_videos : [Popular_videos]?
	let trending_videos : [Popular_videos]?
    let video_list : [Popular_videos]?
    
	enum CodingKeys: String, CodingKey {

		case top_artist_videos = "top_artist_videos"
		case recomended_videos = "recomended_videos"
		case popular_videos = "popular_videos"
		case top_singer_videos = "top_singer_videos"
		case trending_videos = "trending_videos"
        case video_list
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		top_artist_videos = try values.decodeIfPresent([Popular_videos].self, forKey: .top_artist_videos)
		recomended_videos = try values.decodeIfPresent([Popular_videos].self, forKey: .recomended_videos)
		popular_videos = try values.decodeIfPresent([Popular_videos].self, forKey: .popular_videos)
		top_singer_videos = try values.decodeIfPresent([Popular_videos].self, forKey: .top_singer_videos)
		trending_videos = try values.decodeIfPresent([Popular_videos].self, forKey: .trending_videos)
        video_list = try values.decodeIfPresent([Popular_videos].self, forKey: .video_list)
        
	}

}

struct Popular_videos : Codable {
    let video_status : String?
    let artist_name : String?
    let total_views : String?
    let total_reported : String?
    let user_id : String?
    let user_like_status : Int?
    let user_fav_status : Int?
    let lyrics : String?
    let video_file : String?
    let total_comments : String?
    let video_thumb : String?
    let created_on : String?
    let song_name : String?
    let user_profile : String?
    let total_likes : String?
    let music_cat_id : String?
    let video_id : String?
    let user_follow_status : Int?
    let user_name : String?

    enum CodingKeys: String, CodingKey {

        case video_status = "video_status"
        case artist_name = "artist_name"
        case total_views = "total_views"
        case total_reported = "total_reported"
        case user_id = "user_id"
        case user_like_status = "user_like_status"
        case user_fav_status = "user_fav_status"
        case lyrics = "lyrics"
        case video_file = "video_file"
        case total_comments = "total_comments"
        case video_thumb = "video_thumb"
        case created_on = "created_on"
        case song_name = "song_name"
        case user_profile = "user_profile"
        case total_likes = "total_likes"
        case music_cat_id = "music_cat_id"
        case video_id = "video_id"
        case user_follow_status = "user_follow_status"
        case user_name = "user_name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        total_views = try values.decodeIfPresent(String.self, forKey: .total_views)
        total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        user_like_status = try values.decodeIfPresent(Int.self, forKey: .user_like_status)
        user_fav_status = try values.decodeIfPresent(Int.self, forKey: .user_fav_status)
        lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
        video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
        total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
        video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        music_cat_id = try values.decodeIfPresent(String.self, forKey: .music_cat_id)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status)
        user_name = try values.decodeIfPresent(String.self, forKey: .user_name)
    }

}



struct Teacher123: Codable, PrimaryKey {
    let sound_id : String?
    let effect_url : String?
    let effect_name : String?
    let effect_download : URL?
    func primaryKey() -> String {
        return "sound_id"
    }
}
public protocol PrimaryKey {
    func primaryKey() -> String
}
