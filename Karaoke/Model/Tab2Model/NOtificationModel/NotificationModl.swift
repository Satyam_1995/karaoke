
import Foundation
struct NotificationModl : Codable {
	let notification_list : [Notification_list]?
	let status : String?
	let message : String?

	enum CodingKeys: String, CodingKey {

		case notification_list = "notification_list"
		case status = "status"
		case message = "message"
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		notification_list = try values.decodeIfPresent([Notification_list].self, forKey: .notification_list)
		status = try values.decodeIfPresent(String.self, forKey: .status)
		message = try values.decodeIfPresent(String.self, forKey: .message)
	}

}
struct Notification_list : Codable {
    let user_profile : String?
    var user_likes : [User_likes] = []
    let created_at : String?
    let friendid : String?
    let notification_id : String?
    let type : String?
    let video_details : Video_details?
    let notification_status : String?
    let name : String?
    let message : String?
    let userid : String?
    let videoid : String?
    var user_follow_status : Int = 0
    var friend_follow_status : Int = 0
    var maybelater : String = "0"
    
    enum CodingKeys: String, CodingKey {

        case user_profile = "user_profile"
        case user_likes = "user_likes"
        case created_at = "created_at"
        case friendid = "friendid"
        case notification_id = "notification_id"
        case type = "type"
        case video_details = "video_details"
        case notification_status = "notification_status"
        case name = "name"
        case message = "message"
        case userid = "userid"
        case videoid = "videoid"
        case user_follow_status = "user_follow_status"
        case friend_follow_status = "friend_follow_status"
        case maybelater
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        user_profile = try values.decodeIfPresent(String.self, forKey: .user_profile)
        user_likes = try values.decodeIfPresent([User_likes].self, forKey: .user_likes) ?? []
        created_at = try values.decodeIfPresent(String.self, forKey: .created_at)
        friendid = try values.decodeIfPresent(String.self, forKey: .friendid)
        notification_id = try values.decodeIfPresent(String.self, forKey: .notification_id)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        video_details = try values.decodeIfPresent(Video_details.self, forKey: .video_details)
        notification_status = try values.decodeIfPresent(String.self, forKey: .notification_status)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        message = try values.decodeIfPresent(String.self, forKey: .message)
        userid = try values.decodeIfPresent(String.self, forKey: .userid)
        videoid = try values.decodeIfPresent(String.self, forKey: .videoid)
        friend_follow_status = try values.decodeIfPresent(Int.self, forKey: .friend_follow_status) ?? 0
        user_follow_status = try values.decodeIfPresent(Int.self, forKey: .user_follow_status) ?? 0
        maybelater = try values.decodeIfPresent(String.self, forKey: .maybelater) ?? "0"
    }

}
struct User_likes : Codable {
    let created_on : String?
    let like_status : String?
    let user_id : String?
    let likes_id : String?
    let video_id : String?
    let name : String?
    
    enum CodingKeys: String, CodingKey {

        case created_on = "created_on"
        case like_status = "like_status"
        case user_id = "user_id"
        case likes_id = "likes_id"
        case video_id = "video_id"
        case name = "name"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        like_status = try values.decodeIfPresent(String.self, forKey: .like_status)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        likes_id = try values.decodeIfPresent(String.self, forKey: .likes_id)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        
    }

}
struct Video_details : Codable {
    let total_comments : String?
    let total_likes : String?
    let user_id : String?
    let total_reported : String?
    let total_views : String?
    let video_file : String?
    let song_name : String?
    let created_on : String?
    let lyrics : String?
    let video_id : String?
    let music_cat_id : String?
    let artist_name : String?
    let video_status : String?
    let video_thumb : String?

    enum CodingKeys: String, CodingKey {

        case total_comments = "total_comments"
        case total_likes = "total_likes"
        case user_id = "user_id"
        case total_reported = "total_reported"
        case total_views = "total_views"
        case video_file = "video_file"
        case song_name = "song_name"
        case created_on = "created_on"
        case lyrics = "lyrics"
        case video_id = "video_id"
        case music_cat_id = "music_cat_id"
        case artist_name = "artist_name"
        case video_status = "video_status"
        case video_thumb = "video_thumb"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        total_comments = try values.decodeIfPresent(String.self, forKey: .total_comments)
        total_likes = try values.decodeIfPresent(String.self, forKey: .total_likes)
        user_id = try values.decodeIfPresent(String.self, forKey: .user_id)
        total_reported = try values.decodeIfPresent(String.self, forKey: .total_reported)
        total_views = try values.decodeIfPresent(String.self, forKey: .total_views)
        video_file = try values.decodeIfPresent(String.self, forKey: .video_file)
        song_name = try values.decodeIfPresent(String.self, forKey: .song_name)
        created_on = try values.decodeIfPresent(String.self, forKey: .created_on)
        lyrics = try values.decodeIfPresent(String.self, forKey: .lyrics)
        video_id = try values.decodeIfPresent(String.self, forKey: .video_id)
        music_cat_id = try values.decodeIfPresent(String.self, forKey: .music_cat_id)
        artist_name = try values.decodeIfPresent(String.self, forKey: .artist_name)
        video_status = try values.decodeIfPresent(String.self, forKey: .video_status)
        video_thumb = try values.decodeIfPresent(String.self, forKey: .video_thumb)
    }

}
