//
//  Router.swift
//  FirebaseChatDemo
//
//  Created by Bhavneet Singh on 29/07/18.
//  Copyright © 2018 Bhavneet Singh. All rights reserved.
//

import UIKit
//import SlideMenuControllerSwift

enum AppRouter {
    
    /// Go To Home Screen
//    static func goToHome() {
//        
//    
//        let mainViewController = TabBarViewController.instantiate(fromAppStoryboard: .Home)
//        //let leftViewController = MenuVC.instantiate(fromAppStoryboard: .Menu)
//        let nvc = UINavigationController(rootViewController: mainViewController)
//        nvc.isNavigationBarHidden = false
//        //leftViewController.HomeVc = nvc
//
//
//       // let slideMenuController = SlideMenuController(mainViewController: nvc, leftMenuViewController: leftViewController)
//        UIView.transition(with: AppDelegate.shared.window!, duration: 0.33, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
// 
//            AppDelegate.shared.window?.rootViewController =  nvc
//        }, completion: { (finished) in
//            UIApplication.shared.registerForRemoteNotifications()
//        })
//    
//        AppDelegate.shared.window?.becomeKey()
//        AppDelegate.shared.window?.makeKeyAndVisible()
//    }
    
    /// Go To Login Screen
    static func goToLogin() {
//
//        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
//        UserModel.main = UserModel()
//
//        AppUserDefaults.removeAllValues()
//        let getStartedScene = LoginVC.instantiate(fromAppStoryboard: .Main)
//
//        let nvc = UINavigationController(rootViewController: getStartedScene)
//        nvc.isNavigationBarHidden = true
//        nvc.automaticallyAdjustsScrollViewInsets = false
//
//        UIView.transition(with: AppDelegate.shared.window!, duration: 0.33, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//            AppDelegate.shared.window?.rootViewController = nvc
//        }, completion: nil)
//        AppUserDefaults.save(value: true, forKey: .tutorialDisplayed)
//
//        AppDelegate.shared.window?.becomeKey()
//        AppDelegate.shared.window?.makeKeyAndVisible()
    }
    
//    static func goToWalkthrough() {
//           
//           HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
//           UserModel.main = UserModel()
//           AppUserDefaults.removeAllValues()
//           let getStartedScene = WalkThroughVC.instantiate(fromAppStoryboard: .PreLogin)
//           
//           let nvc = UINavigationController(rootViewController: getStartedScene)
//           nvc.isNavigationBarHidden = true
//           nvc.automaticallyAdjustsScrollViewInsets = false
//           
//           UIView.transition(with: AppDelegate.shared.window!, duration: 0.33, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//               AppDelegate.shared.window?.rootViewController = nvc
//           }, completion: nil)
//           
//           AppDelegate.shared.window?.becomeKey()
//           AppDelegate.shared.window?.makeKeyAndVisible()
//       }
    
    
    /// Go To Any View Controller
    static func goToVC(viewController: UIViewController) {
        
//        let nvc = UINavigationController(rootViewController: viewController)
//        
//        nvc.isNavigationBarHidden = true
//        nvc.automaticallyAdjustsScrollViewInsets = false
//        UIView.transition(with: AppDelegate.shared.window!, duration: 0.33, options: UIView.AnimationOptions.transitionCrossDissolve, animations: {
//            AppDelegate.shared.window?.rootViewController = nvc
//        }, completion: nil)
//        AppDelegate.shared.window?.becomeKey()
//        AppDelegate.shared.window?.makeKeyAndVisible()
    }
}


