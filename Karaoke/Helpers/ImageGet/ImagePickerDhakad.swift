//
//  ImagePickerDhakad.swift
//  Caviar
//
//  Created by YATIN  KALRA on 08/10/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import UIKit
import YPImagePicker
import AVFoundation
import Photos

protocol imagePickerDelegate: class {
    func didSelect(image: UIImage?,tag:Int,data:Data?)
    func didSelectList(list: [imageArray])
//    func canceled(tag:Int)
    func imagePicker_Error(tag:Int)
    func didVideo(image: UIImage?,tag:Int,data:Data?)
}
class ImagePickerDhakad: NSObject {

    private weak var presentationController: UIViewController?
    private weak var delegate: imagePickerDelegate?
    private var tag  = 0
    
    public init(presentationController: UIViewController, delegate: imagePickerDelegate) {
        
        super.init()
        
        self.presentationController = presentationController
        self.delegate = delegate
    }
    public func showImagePicker(titleStr:String,optionIdentifier : Int,scerrn : [YPPickerScreen] = [.library, .photo] , mediaType : YPlibraryMediaType = .photo) {
         
        var config = YPImagePickerConfiguration()
        config.screens = scerrn
        config.showsCrop = .rectangle(ratio: 1)
        config.library.mediaType = mediaType
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = true
        config.library.minWidthForItem = nil
        config.library.defaultMultipleSelection = false
//        config.library.maxNumberOfItems = 1
//        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        config.gallery.hidesRemoveButton = false
        config.showsPhotoFilters = true
        config.isScrollToChangeModesEnabled = true
        config.onlySquareImagesFromCamera = true
        config.shouldSaveNewPicturesToAlbum = false
//        config.startOnScreen = YPPickerScreen.library
        config.startOnScreen = scerrn.first!
         
         let picker = YPImagePicker(configuration: config)
         picker.navigationItem.backBarButtonItem?.tintColor = UIColor.white
         picker.navigationBar.topItem?.rightBarButtonItem?.tintColor = UIColor.white
         picker.navigationBar.topItem?.leftBarButtonItem?.tintColor = UIColor.white
         
         picker.didFinishPicking { [unowned picker] items, cancelled in
             if cancelled {
                 print("Picker was canceled")
                 picker.dismiss(animated: true, completion: nil)
                 return
             }
             DispatchQueue.main.async {
                 if let photo = items.singlePhoto {
                     let imgatdata : UIImage = photo.modifiedImage ?? photo.originalImage
                     imgatdata.resizeByByte(maxMB: 0.5) { (imageD) in
                        self.delegate?.didSelect(image: imgatdata, tag: optionIdentifier, data: imageD)
                     }
                 }
             }
             picker.dismiss(animated: true, completion: nil)
         }
        presentationController?.present(picker, animated: true, completion: nil)
     }
        public func showImagePickerMultiple(titleStr:String,optionIdentifier : Int,scerrn : [YPPickerScreen] = [.library, .photo] , mediaType : YPlibraryMediaType = .photo) {
             
            var config = YPImagePickerConfiguration()
            config.screens = scerrn
            config.showsCrop = .rectangle(ratio: 1)
            config.library.mediaType = mediaType
            config.library.options = nil
            config.library.onlySquare = false
            config.library.isSquareByDefault = true
            config.library.minWidthForItem = nil
            config.library.defaultMultipleSelection = true
            config.library.maxNumberOfItems = 5
    //        config.library.minNumberOfItems = 1
            config.library.numberOfItemsInRow = 4
            config.library.spacingBetweenItems = 1.0
            config.library.skipSelectionsGallery = false
            config.library.preselectedItems = nil
            config.gallery.hidesRemoveButton = false
            config.showsPhotoFilters = true
            config.isScrollToChangeModesEnabled = true
            config.onlySquareImagesFromCamera = true
            config.shouldSaveNewPicturesToAlbum = false
    //        config.startOnScreen = YPPickerScreen.library
            config.startOnScreen = scerrn.first!
             
             let picker = YPImagePicker(configuration: config)
             picker.navigationItem.backBarButtonItem?.tintColor = UIColor.white
             picker.navigationBar.topItem?.rightBarButtonItem?.tintColor = UIColor.white
             picker.navigationBar.topItem?.leftBarButtonItem?.tintColor = UIColor.white
             
             picker.didFinishPicking { [unowned picker] items, cancelled in
                 if cancelled {
                     print("Picker was canceled")
                     picker.dismiss(animated: true, completion: nil)
                     return
                 }
                 DispatchQueue.main.async {
                    var load = 0
                    var li : [imageArray] = []
                     for item in items {
                         switch item {
                         case .photo(let photo):
                             print(photo)
                             let imgatdata : UIImage = photo.modifiedImage ?? photo.originalImage
                             imgatdata.resizeByByte(maxMB: 1) { (imageD) in
                                 load = load + 1
                                 li.append(imageArray(image: imgatdata, data: imageD))
                                 if load == items.count {
                                     self.delegate?.didSelectList(list: li)
                                 }
                             }
                         case .video(let video):
                             print(video)
                         }
                     }
                    if let photo = items.singlePhoto {
                         let imgatdata : UIImage = photo.modifiedImage ?? photo.originalImage
                         imgatdata.resizeByByte(maxMB: 0.5) { (imageD) in
                            self.delegate?.didSelect(image: imgatdata, tag: optionIdentifier, data: imageD)
                         }
                     }
                 }
                 picker.dismiss(animated: true, completion: nil)
             }
            presentationController?.present(picker, animated: true, completion: nil)
         }
    
    public func showVideoPicker(titleStr:String,optionIdentifier : Int,scerrn : [YPPickerScreen] = [.library, .video] , mediaType : YPlibraryMediaType = .video) {
         
        var config = YPImagePickerConfiguration()
        config.screens = scerrn
        config.showsCrop = .rectangle(ratio: 1)
        config.library.mediaType = mediaType
        
        config.isScrollToChangeModesEnabled = true
        config.usesFrontCamera = false
        config.showsPhotoFilters = true
        config.showsVideoTrimmer = true
        config.startOnScreen = YPPickerScreen.library
        config.screens = [.library, .video]
        config.showsCrop = .none
        config.targetImageSize = YPImageSize.original
//        config.overlayView = UIView()
        config.hidesStatusBar = true
        config.hidesBottomBar = false
        config.hidesCancelButton = false
        config.preferredStatusBarStyle = UIStatusBarStyle.default
        config.bottomMenuItemSelectedTextColour = .purple
        config.bottomMenuItemUnSelectedTextColour = .blue
        
//        config.maxCameraZoomFactor = 1.0
//        config.preSelectItemOnMultipleSelection = true
        
        
        config.library.options = nil
        config.library.onlySquare = false
        config.library.isSquareByDefault = false
        config.library.minWidthForItem = nil
        config.library.defaultMultipleSelection = false
//        config.library.maxNumberOfItems = 1
//        config.library.minNumberOfItems = 1
        config.library.numberOfItemsInRow = 4
        config.library.spacingBetweenItems = 1.0
        config.library.skipSelectionsGallery = false
        config.library.preselectedItems = nil
        config.gallery.hidesRemoveButton = false
//        config.showsPhotoFilters = true
//        config.isScrollToChangeModesEnabled = true
//        config.onlySquareImagesFromCamera = true
        config.shouldSaveNewPicturesToAlbum = false
//        config.startOnScreen = YPPickerScreen.library
        config.startOnScreen = scerrn.first!
        
        config.video.compression = AVAssetExportPresetMediumQuality
        config.video.fileType = .mov
        config.video.recordingTimeLimit = 1200.0
        config.video.libraryTimeLimit = 1200.0
        config.video.minimumTimeLimit = 3.0
//        config.video.trimmerMaxDuration = 60.0
        config.video.trimmerMinDuration = 3.0
        
         let picker = YPImagePicker(configuration: config)
         picker.navigationItem.backBarButtonItem?.tintColor = UIColor.white
         picker.navigationBar.topItem?.rightBarButtonItem?.tintColor = UIColor.white
         picker.navigationBar.topItem?.leftBarButtonItem?.tintColor = UIColor.white
         
         picker.didFinishPicking { [unowned picker] items, cancelled in
            picker.dismiss(animated: true, completion: nil)
             if cancelled {
                 print("Picker was canceled")
                 return
             }
             DispatchQueue.main.async {
                if let v = items.singleVideo {
                    let alert = UIAlertController(title: nil, message: NSLocalizedString("Please wait", comment: ""), preferredStyle: .alert)
                    
                    let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
                    loadingIndicator.hidesWhenStopped = true
                    loadingIndicator.style = UIActivityIndicatorView.Style.gray
                    loadingIndicator.startAnimating();
                    alert.view.addSubview(loadingIndicator)
                    self.presentationController?.present(alert, animated: true, completion: {
                        let videoURL = v.url
                        
//                        let data = NSData(contentsOf: videoURL)!
//                        print("File size before compression: \(Double(data.length) / 1048576.00) mb")
                        let compressedURL = NSURL.fileURL(withPath: NSTemporaryDirectory() + NSUUID().uuidString + ".m4v")
                        self.compressVideo(inputURL: videoURL, outputURL: compressedURL) { (exportSession) in
                                guard let session = exportSession else {
                                    return
                                }
                            switch session.status {
                            
                            case .unknown:
                                alert.dismiss(animated: true) {
                                    self.delegate?.imagePicker_Error(tag: optionIdentifier)
                                }
                                break
                            case .waiting:
                                break
                            case .exporting:
                                alert.dismiss(animated: true) {
                                    self.delegate?.imagePicker_Error(tag: optionIdentifier)
                                }
                                break
                            case .completed:
                                guard let compressedData = NSData(contentsOf: compressedURL) else {
                                    alert.dismiss(animated: true) {
                                        self.delegate?.imagePicker_Error(tag: optionIdentifier)
                                    }
                                    return
                                }
                                print("File size after compression: \(Double(compressedData.length) / 1048576.00) mb")
                                
                                DispatchQueue.main.async {
                                    alert.dismiss(animated: true) {
                                        DispatchQueue.main.async {
                                            self.delegate?.didVideo(image: v.thumbnail, tag: optionIdentifier, data: compressedData as Data)
                                        }
                                    }
                                }
                            case .failed:
                                alert.dismiss(animated: true) {
                                    self.delegate?.imagePicker_Error(tag: optionIdentifier)
                                }
                                break
                            case .cancelled:
                                alert.dismiss(animated: true) {
                                    self.delegate?.imagePicker_Error(tag: optionIdentifier)
                                
                                }
                                break
                            @unknown default:
                                alert.dismiss(animated: true) {
                                    self.delegate?.imagePicker_Error(tag: optionIdentifier)
                                }
                                break
                            }
                        }
                        
                    })
                }
             }
             
         }
        presentationController?.present(picker, animated: true, completion: nil)
     }
    
    func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
            let urlAsset = AVURLAsset(url: inputURL, options: nil)
            guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
                handler(nil)

                return
            }
//        AVAssetExportPresetLowQuality
//        AVAssetExportPresetMediumQuality
//        AVAssetExportPresetHighestQuality
//        AVAssetExportPreset640x480
//        AVAssetExportPreset960x540
            exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mov
            exportSession.shouldOptimizeForNetworkUse = true
            exportSession.exportAsynchronously { () -> Void in
                handler(exportSession)
            }
        }
}
