//
//  DateExyension.swift
//  Caviar
//
//  Created by YATIN  KALRA on 27/08/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation
extension Date {
    static func getCurrentDateForName() -> String {
        return self.getCustomDate(formet: "dd_MM_yyyy_HH_mm_ss_a_SSS", date: Date())
    }
    static func getCurrentDate(formet:String = "MM/dd/yyyy HH:mm:ss a") -> String {
        return self.getCustomDate(formet: formet, date: Date())
    }
    static func getCustomDate(formet:String = "MM/dd/yyyy HH:mm:ss a", date: Date = Date()) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formet
        return dateFormatter.string(from: date)
    }
    static func getCustomDateOnly(formet:String = "MM/dd/yyyy", date: Date = Date()) -> String {
        return self.getCustomDate(formet: formet, date: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formet
        return dateFormatter.string(from: date)
    }
    static func getCustomTimeOnly(formet:String = "HH:mm:ss a", date: Date = Date()) -> String {
        return self.getCustomDate(formet: formet, date: date)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formet
        return dateFormatter.string(from: date)
    }
    
}
