//
//  Array+Extension.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 05/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
extension Array {

    func uniques<T: Hashable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return reduce([]) { result, element in
            let alreadyExists = (result.contains(where: { $0[keyPath: keyPath] == element[keyPath: keyPath] }))
            return alreadyExists ? result : result + [element]
        }
    }
}


