//
//  UserDefaultsExtension.swift
//  Caviar
//
//  Created by YATIN  KALRA on 20/08/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation

extension UserDefaults {
    func india_object(forKey: String, withSaveUser_id: withSaveUser_id = .with_id) -> Any {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        let obj = UserDefaults.standard.object(forKey: user_id + forKey)
        return obj as Any
    }
    func india_Set(_ data:Any, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    
    func india_Set(_ data:String, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    func india_Set(_ data:Bool, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    func india_Set(_ data:Int, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    func india_Set(_ data:Double, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    func india_Set(_ data:Float, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    
    func india_Set(_ data:NSArray, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
    func india_Set(_ data:NSDictionary, forKey: String, withSaveUser_id: withSaveUser_id = .with_id) {
        var user_id = ""
        if withSaveUser_id == .with_id {
            user_id = (UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String) ?? ""
        }
        UserDefaults.standard.set(data, forKey: user_id + forKey)
    }
}
public enum withSaveUser_id: String {
    
    case with_id = "true"
    case withOut_id = "false"
}
