//
//  PlayerView.swift
//  Karaoke
//
//  Created by YATIN  KALRA on 12/01/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class PlayerView: UIView {

    var item: String? {
        didSet {
            if let id = item {
               itemId = id
            }
        }
    }

    var itemId = String()

    init(frame: CGRect, item: String) {
        super.init(frame: frame)
        self.itemId = item

        setupPlayerView()
        setupContainerView()
        backgroundColor = UIColor(white: 0.3, alpha: 1)
    }
    func setupContainerView() {
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public var player: AVPlayer?

    func setupPlayerView() {

//        let baseurl = "http://xample.com/play.m3u?id="
//        let urlString = "\(baseurl)\(itemId)"
        
        let urlString = itemId
        print(urlString)
        if let url = URL(string: urlString) {
            player = AVPlayer(url: url)

            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.videoGravity = .resizeAspectFill
            self.layer.addSublayer(playerLayer)
            playerLayer.frame = self.frame

//            let audioSession = AVAudioSession.sharedInstance()
//            do{
//                try audioSession.setCategory(AVAudioSession.Category.playback)
//            } catch let err {
//                print(err)
//                return
//            }

            player?.play()
            
            player?.addObserver(self, forKeyPath: "currentItem.status", options: .new, context: nil)
        }
    }
}
class PlayerLauncher: NSObject {
    var item: String? {
        didSet {
            
        }
    }
    func showVideoPlayer() {
        print("Showing the player...")

        if let keyWindow = UIApplication.shared.keyWindow {
            let view = UIView(frame: keyWindow.frame)
            view.backgroundColor = UIColor.white

            view.frame = CGRect(x: keyWindow.frame.width - 10, y: keyWindow.frame.height - 10, width: 10, height: 10)

            let playerFrame = CGRect(x: 0, y: 0, width: keyWindow.frame.width, height: keyWindow.frame.height)
            let playerView = PlayerView(frame: playerFrame, item: item ?? "")
            playerView.item = item
            playerView.setupPlayerView()
            view.addSubview(playerView)

            keyWindow.addSubview(view)

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                view.frame = keyWindow.frame
            }, completion: { (completedAnimation) in
                //later...

            })
        }
    }
}
