//
//  RejectionFilter.cpp
//  Karaoke
//
//  Created by JDBTechs on 8/10/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

#import "RejectionFilter.h"

const Float32 kDefaultPoleDist = 0.975f;

@interface RejectionFilter()


@end

@implementation RejectionFilter

- (id)init
{
    if (self = [super init]) {
        mY1 = mX1 = 0;
    }
    return self;
}


- (void) ProcessInplace:(Float32*) ioData  numFrames: (UInt32) numFrames {
    for (UInt32 i=0; i < numFrames; i++)
    {
        Float32 xCurr = ioData[i];
        ioData[i] = ioData[i] - mX1 + (kDefaultPoleDist * mY1);
        mX1 = xCurr;
        mY1 = ioData[i];
    }
}

@end

