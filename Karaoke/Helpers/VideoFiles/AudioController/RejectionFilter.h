//
//  RejectionFilter.hpp
//  Karaoke
//
//  Created by JDBTechs on 8/10/21.
//  Copyright © 2021 Ankur. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface RejectionFilter : NSObject {
    Float32 mY1;
    Float32 mX1;
}

- (void) ProcessInplace:(Float32*) ioData  numFrames: (UInt32) numFrames;
@end

