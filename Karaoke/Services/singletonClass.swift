//
//  singletonClass.swift
//  Caviar
//
//  Created by YATIN  KALRA on 28/08/20.
//  Copyright © 2020 Ankur. All rights reserved.
//

import Foundation

class singletonClass: NSObject {
    
    static var instance : singletonClass?
    var userProfileData : ProfileDetailModel?
    
    var firebaseToken : String = "123"
    
    
    class func createsingleton() -> singletonClass? {
        if instance == nil {
            instance = singletonClass()
        }
        return instance
    }
    
}

struct JDB {
    static var isNetworkConnected:Bool = false
    static func log(_ logMessage: String,_ args:Any... , functionName: String = #function ,file:String = #file,line:Int = #line) {
        
        let newArgs = args.map({arg -> CVarArg in String(describing: arg)})
        let messageFormat = String(format: logMessage, arguments: newArgs)
        
        print("LOG :- \(((file as NSString).lastPathComponent as NSString).deletingPathExtension)--> \(functionName) ,Line:\(line) :", messageFormat)
    }
    static func error(_ logMessage: String,_ args:Any... , functionName: String = #function ,file:String = #file,line:Int = #line) {
        
        let newArgs = args.map({arg -> CVarArg in String(describing: arg)})
        let messageFormat = String(format: logMessage, arguments: newArgs)
        
        print("ERROR :- \(((file as NSString).lastPathComponent as NSString).deletingPathExtension)--> \(functionName) ,Line:\(line) :", messageFormat)
    }
}
