


import Foundation

class AppConstants {
}


class IdentifierStoryboard {
    static let sSplashVC = "SplashVC"

    static let sHomeVC = "HomeVC"
    static let sChatVC = "ChatVC"
    static let sChatHomeVC = "ChatHomeVC"
    static let sreportVC = "reportVC"
    static let sPlanVC = "PlanVC"
    static let sEditInfoVC = "EditInfoVC"
    static let sImageHomeVC = "ImageHomeVC"
    static let sImageHomeVC1 = "ImageHomeVC1"
    static let sMatchScreenVC = "MatchScreenVC"
    static let sContactVC = "ContactVC"
    
    
}
class IdentifierCollectCell {
    static let sSplashCell = "SplashCell"
    static let sBuildProfileCell = "BuildProfileCell"
    static let sChatUserCell = "ChatUserCell"
    static let scollectionTableHomeCell = "collectionTableHomeCell"
    static let sPlanCell = "PlanCell"
    static let sImageHomeCell = "ImageHomeCell"
}
class IdentifierTableCell {
    static let sChatCell = "ChatCell"
    static let stableHomeCell1 = "tableHomeCell1"
    static let stableHomeCollectionCell = "tableHomeCollectionCell"
    static let stableHomeCell3 = "tableHomeCell3"
    static let sEditInfoCell = "EditInfoCell"
}
class NotificationProfileBtn {
    static let Profile = "Profile"
}
class alertTitle {
    static let alert_success = "Success"
    static let alert_error = "Error"
    static let alert_warning = "Warning"
    static let alert_alert = "Alert"
    static let alert_message = "Message"
}
class buttonTitle {
    static let btnOk = "OK"
    static let btnCancel = "Cancel"
    static let btnFollow = "FOLLOW"
    static let btnUnfollow = "UNFOLLOW"
}
class identifierNotification {
    static let nHosting = "nHosting"
}

class MessageString {
    
    static let emailorMobile = "Please enter mobile number/Email."
    static let enterMobile = "Please enter mobile number."
    static let enterValidMobile = "Please enter valid mobile number."
    static let enterValidMobile2 = "Please enter 10 digit mobile number"
    static let enterOTP = "Please enter otp."
    static let validOTP = "Please enter valid otp."
    static let sentOTP = "OTP has been sent to your mobile number/Email."
    static let name = "Please enter name."
    static let username = "Please enter username."
    static let first_name = "Please enter first name."
    static let bio = "Please enter first bio."
    static let last_name = "Please enter last name."
    static let email = "Please enter email."
    static let address = "Please enter address."
    static let validEmail = "Please enter valid email."
    static let description = "Please enter description."
    static let oldpassword = "Please enter old password."
    static let password = "Please enter password."
    static let passwordValid = "Password should be a combination of one upper case character, one lowercase character, one special character, one number, and should be at least 8 characters long."
    
    
    static let Email_already_exists = "This email address is already in use. Please use a different email address."
    static let Mobile_already_exists = "This Mobile number is already in use. Please use a different email address."
    static let Location = "Please enter Location."
    
    static let country = "Please select country."
    static let state = "Please select state."
    static let city = "Please select city."
    static let zip = "Please select zip."
    static let street = "Please enter street."
    static let comment = "Please enter your comment."
    static let message = "Please enter message"
    
    
    static let newpassword = "Please enter new password."
    static let confirmpassword = "Please enter confirm password."
    static let matchConfirm = "password don't match with confirm password"
    static let location = "Please enter location."
    static let Nexr_Court_date = "Please select Nexr Court date."
    
    static let addPhoto = "Please attach photo."
    static let songName = "Please enter song name"
    static let artiseName = "Please enter artist name"
    
    static let acceccLogin = "You need to Login/Sign up to access profile."
    static let accessCheckIn = "You need to Login/Sign up for check-in"
    static let terms = "Please check terms and conditions if you want to proceed."
    
    
    static let MeventSave = "Event updated successfully"
    static let MeventCreate = "Event created successfully"
    static let MeventUpdate = "Event update successfully"
    
    static let fLast = "Please enter last name."
    static let birthday = "Please select birthday."
    static let yourself = "Please enter about yourself."
        
    static let addressLocation = "Please enter about yourself."
    static let selectImage = "Please select minimum 1 image."
    static let select_date = "Please select date."
    static let select_Hours = "Please select time."
    static let select_am_pm = "Please select time."
    static let select_timeZone = "Please select timeZone."
    static let validAddress  = "Please enter a valid address."
    

    static let username_already = "This username is already in use"
    
    static let lyrics = "Please enter lyrics."
    static let  music_categoriy  = "Please select music categoriy."
    static let  checkbox  = "Please select checkbox."
    static let  video_select  = "Please select video."
    static let  video_record  = "Please record video."

}

class notificationName {
    static let editProfile = "editProfile"
    static let userListInMessage = "userListInMessage"
}

class userDefaultKey {
    static let twilioToken = "twilioToken"
    static let aboutData = "aboutData"
    static let caseOverData = "caseOverData"
    static let Call_us = "Call_us"
    static let fcmToken = "fcmToken"
    static let twilioidentity = "twilioidentity"
    static let full_name = "full_name"
    static let email = "email"
    static let mobile = "mobile"
    static let state = "state"
    static let city = "city"
    static let street_address = "street_address"
    static let profile_pic = "profile_pic"
    
    static let checkinStatus = "checkinStatus"
}

class appfilte {
    static let arr_Ages = ["35-40","40-45","45-50","50-55","55-60","60-65","65 and over"]
    static let arr_relationsipStatus = ["Divorced","Single","Widowed or Widower","Your relationship status does not matter now"]
    static let arr_kindOfRelationsip = ["Friendship","Someone to go out with occasionally","Serious relationship not leading to marriage","Serious relationship leading to marriage"]
    static let arr_nationality = ["African-American","Caribbean descent","Latino","Caucasian","Other","Nationality does not matter"]
    static let arr_bodyType = ["Slim","Average","A few extra pounds","Heavy","Size does not matter"]
    static let arr_height = ["Under 5ft","5ft – 5ft.5","5ft.6 – 6ft","Over 6ft","Height does not matter"]
    static let arr_jobStatus = ["Employed","Unemployed","Current job status does not matter right now","Employment status does not matter"]
    static let arr_education = ["Master’s degree or higher","College graduate","High school diploma","Educated with no papers","Educated in the school of life","Educational level does not matter"]
    static let arr_housing = ["Own your own home or apartment","Rent a house or apartment","Live with family or friends","Your current living situation does not matter"]
    static let arr_drinking = ["Occasionally","Socially","Never drinks"]
    static let arr_smoking = ["Smokes","Does not smoke","Smoking does not matter"]
    static let arr_relegion = ["Christian","Muslim","Hindu","Your religion does not matter"]
}

class appFilterForUserProfile {
    static let arr_Ages = ["35-40","40-45","45-50","50-55","55-60","60-65","65 and over"]
    static let arr_currentRelationsip = ["Married but separated","Separated","Single","Divorced","Widowed or Widower"]
    static let arr_currentRelationsipE = ["Separated","Single","Divorced","Widowed or Widower"]
    static let arr_kindOfRelationsip = ["Friendship","Someone to go out with occasionally","Serious long term","Serious leading to marriage"]
    static let arr_kindOfRelationsipE = ["Friendship","Someone to go out with occasionally","Serious long term relationship not leading to marriage.","Serious long term relationship leading to marriage."]
    static let arr_nationality = ["African-American","Caribbean descent","Latino","Caucasian","Other"]
    static let arr_relegion = ["Christian","Muslim","Hindu","Other"]
    static let arr_bodyType = ["Slim","Average","A few extra pounds","Heavy"]
    static let arr_height = ["Under 5ft","5ft – 5ft.5","5ft.6 – 6ft","Over 6ft"]
    static let arr_jobStatus = ["I am Employed","I am Unemployed"]
    static let arr_education = ["I have a master’s degree or higher","College graduate","High school diploma","Educated with no papers","Educated in the school of life"]
    static let arr_housing = ["I own my own house or apartment","I rent a house or apartment","I live with family or friends"]
    static let arr_drinking = ["Regularly","Socially","Occasionally","Never"]
    static let arr_smoking = ["Regularly","Occasionally","Non-smoker"]
    
}
