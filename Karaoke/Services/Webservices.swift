//
//  Webservices.swift
//  NewProject
//
//  Created by Preeti dhankar on 30/08/18.
//  Copyright © 2018 Preeti dhankar. All rights reserved.
//
//let conversationsManager = QuickstartConversationsManager()

import Foundation
import SwiftyJSON
import Alamofire


enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}


extension WebServices {
    
    // MARK:- Common POST API
    //=======================
    static func commonPostAPI(parameters: JSONDictionary = [:],
                              endPoint: EndPoint,
                              loader: Bool = true,
                              success : @escaping SuccessResponse,
                              successData : @escaping (Data) -> Void,
                              failure : @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
           success(json)

        }, successData: { (data) in
            successData(data)
        }) { (error) in
            failure(error)
        }
    }
    
    static func commonPostAPI_data(parameters: JSONDictionary = [:],
                                   files : [UploadFileParameter] = [],
                              endPoint: EndPoint,
                              loader: Bool = true,
                              success : @escaping SuccessResponse,
                              successData : @escaping (Data) -> Void,
                              progress : @escaping (Double) -> Void,
                              failure : @escaping FailureResponse) {
        AppNetworking.POSTWithFiles(endPoint: endPoint.path, parameters: parameters, files: files, loader: loader) { (json) in
            success(json)
        } successData: { (data) in
            successData(data)
        } progress: { (data) in
            progress(data)
        } failure: { (error) in
            failure(error)
        } 
    }
    // MARK:- Common POST API
    //=======================
    static func commonPostAPIModel<T: Codable>(model : T.Type?,
                                               parameters: JSONDictionary = [:],
                                               endPoint: EndPoint,
                                               loader: Bool = true,
                                               success : @escaping SuccessResponse,
                                               returnModel : @escaping (T?,Error?) -> Void,
                                               successData : @escaping (Data) -> Void,
                                               failure : @escaping FailureResponse) {
        
        AppNetworking.POST(endPoint: endPoint.path, parameters: parameters, loader: loader, success: { (json) in
           success(json)
        }, successData: { (data) in
            successData(data)
            if model != nil {
                do{
                    let dict = try JSONDecoder().decode(model!, from: data)
                    returnModel(dict,nil)
                }catch{
                    returnModel(nil,error)
                }
            }
        }) { (error) in
            failure(error)
        }
    }

//    // MARK:- LogOut
//    //=====================
//    static func send_otp(mobile_number : String,country_code:String,
//                         success: @escaping SuccessResponse,
//                         successData : @escaping (Data) -> Void,
//                         failure: @escaping FailureResponse) {
//        //http://anothercz.yilstaging.com/api/send_otp
////        mobile_number,country_code
//        let params = [ApiKey.mobile:mobile_number,
//                      ApiKey.country_code:country_code]
//        
//        self.commonPostAPI(parameters: params, endPoint: .send_otp, loader: true, success: { (json) in
//            success(json)
//        }, successData: { (data) in
//        }, failure: failure)
//    }

//     MARK:- set_online_status
//    //=====================
    static func get_video_details(video_id:String,
                                  loader: Bool = true,
                                  success: @escaping SuccessResponse,
                              successData : @escaping (Data) -> Void,
                             failure: @escaping FailureResponse) {
        //        Para:video_id,user_id
        //        URL - http://karaoke.yilstaging.com/api/get_video_details
        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
            let err = NSError(localizedDescription: "invalid user")
            failure(err)
            return
        }
        let params : [String:Any] = [ApiKey.user_id:user_id,
                                     ApiKey.video_id:video_id]
        self.commonPostAPI(parameters: params, endPoint: .get_video_details, loader: loader, success: success, successData: successData, failure: failure)
//        self.commonPostAPI(parameters: params, endPoint: .get_video_details, loader: false, success: { (json) in
//            success(json)
//        }, successData: { (data) in
//            successData(data)
//        }, failure: failure)
        
    }
    // MARK:- get_online_status
//    //=====================
//    static func get_online_status(user_id:String, success: @escaping SuccessResponse,
//                              successData : @escaping (Data) -> Void,
//                             failure: @escaping FailureResponse) {
////        API URL: http://anothercz.yilstaging.com/api/get_online_status
////        Method: Post
////        Parameters: user_id
//
//        let params : [String:Any] = [ApiKey.user_id:user_id]
//        self.commonPostAPI(parameters: params, endPoint: .get_online_status, loader: false, success: { (json) in
//            success(json)
//        }, successData: { (successData) in
//        }, failure: failure)
//    }
    // MARK:- Sign Up
    //===============
//    static func signUp(parameters: JSONDictionary,
//                       success: @escaping SuccessResponseNew,
//                       failure: @escaping FailureResponse) {
//
//        self.commonPostAPI(parameters: parameters, endPoint: .send_otp, success: { (json) in
//
//               let response = json["response"].boolValue
//               let msg = json["message"].stringValue
//                switch response {
//                case true: success(json,msg)
//                default: failure(NSError(code: 200, localizedDescription: msg))
//            }
//
//        }) { (error) -> (Void) in
//            failure(error)
//        }
//    }
    // MARK:- getTokenTwilio
    //===============
//    static func getTokenTwilio()  {
//        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String, user_id != "" else{
//            return
//        }
//        //http://caviar.yesitlabs.xyz/api/get_access_token
//        //http://caviar.yesitlabs.xyz/chat/get_access_token
////        http://anothercz.yilstaging.com/chat/get_access_token
//        let para : JSONDictionary = ["user_id":user_id]
////        let url = WebServices.EndPoint.get_access_token.rawValue
////        let url = "http://anothercz.yilstaging.com/chat/get_access_token"
//        let url = baseUrl_chat + WebServices.EndPoint.get_access_token.rawValue
//
//        AppNetworking.POST(endPoint: url, parameters: para, loader: false, success: { (json) in
//           guard let jspn = json.dictionaryObject else{
//               return
//           }
//           if let token = jspn["token"] as? String {
//               let identity = jspn["identity"] ?? ""
////            let pasteboard = UIPasteboard.general
////            pasteboard.string = token
//               UserDefaults.standard.set(token, forKey: userDefaultKey.twilioToken)
//               UserDefaults.standard.set(identity, forKey: userDefaultKey.twilioidentity)
////               let MessagingClientClass = MessagingManager.self
////               SessionManager.loginWithUsername(username: "username")
////               let MessagingManager = MessagingClientClass.sharedManager()
////               MessagingManager.initializeClientWithToken(token: token)
//
////            let conversationsManager = QuickstartConversationsManager()
//            QuickstartConversationsManager.sharedManager.loginWithAccessToken(token) { (client) in
//
//            }
////            conversationsManager.loginWithAccessToken(token) { (res) in
////            }
//           }
//
//        }, successData: { (data) in
//
//        }) { (error) in
//
//        }
//    }
//    static func getTokenTwilio1(complition: @escaping (String,String) -> Void)  {
//        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String, user_id != "" else{
//            return
//        }
//        //http://caviar.yesitlabs.xyz/api/get_access_token
//        //http://caviar.yesitlabs.xyz/chat/get_access_token
////        http://anothercz.yilstaging.com/chat/get_access_token
//        let para : JSONDictionary = ["user_id":user_id]
////        let url = WebServices.EndPoint.get_access_token.rawValue
////        let url = "http://anothercz.yilstaging.com/chat/get_access_token"
//        let url = baseUrl_chat + WebServices.EndPoint.get_access_token.rawValue
//        
//        AppNetworking.POST(endPoint: url, parameters: para, loader: false, success: { (json) in
//           guard let jspn = json.dictionaryObject else{
//               return
//           }
//           if let token = jspn["token"] as? String {
//               let identity = jspn["identity"] as? String ?? ""
////            let pasteboard = UIPasteboard.general
////            pasteboard.string = token
//               UserDefaults.standard.set(token, forKey: userDefaultKey.twilioToken)
//               UserDefaults.standard.set(identity, forKey: userDefaultKey.twilioidentity)
//            complition(token,identity)
//           }
//        }, successData: { (data) in
//        }) { (error) in
//            complition("","")
//        }
//    }
//    // MARK:- get_content
//    //================================
//    static func get_content(loader : Bool, success: @escaping SuccessResponse,
//                       successData : @escaping (Data) -> Void,
//                       failure: @escaping FailureResponse) {
////http://caviar.yesitlabs.xyz/api/get_content
//
//        self.commonPostAPI(endPoint: .get_content,loader: loader, success: { (json) in
//            success(json)
//        }, successData: { (data) in
//            successData(data)
//        }) { (error) -> (Void) in
//            failure(error)
//        }
//    }
//    // MARK:- get_content
//        //================================
//        static func get_question_answer(loader : Bool, success: @escaping SuccessResponse,
//                           successData : @escaping (Data) -> Void,
//                           failure: @escaping FailureResponse) {
////    http://caviar.yesitlabs.xyz/api/get_question_answer
//
//            self.commonPostAPI(endPoint: .get_question_answer,loader: loader, success: { (json) in
//                success(json)
//            }, successData: { (data) in
//                successData(data)
//            }) { (error) -> (Void) in
//                failure(error)
//            }
//        }
   // MARK:- submit_favorite_unfavorite_event
        //=============
    static func submit_favorite_unfavorite_video(video_id:String,
                                                 favourite_status: favourite_status,
                                                 success: @escaping SuccessResponse,
                                                 successData : @escaping (Data) -> Void,
                                                 failure: @escaping FailureResponse) {
        //        Para:user_id,video_id,favorite_status (1-add,0-remove)
        //        URL - http://karaoke.yilstaging.com/api/favorite_add_remove
        
        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
            let err = NSError(localizedDescription: "invalid user")
            failure(err)
            return
        }
        var para : JSONDictionary = [:]
        para[ApiKey.user_id] = user_id
        para[ApiKey.video_id] = video_id
        para[ApiKey.favorite_status] = favourite_status.rawValue
        
        self.commonPostAPI(parameters: para, endPoint: .favorite_add_remove, loader: false, success: { (json) in
            success(json)
        }, successData: { (data) in
            successData(data)
        }) { (err) -> (Void) in
            failure(err)
        }
        
    }
    
    // MARK:- submit_favorite_unfavorite_event
         //=============
     static func submit_post_likes_unlike_video(video_id:String,
                                                  like: like_status,
                                                  success: @escaping SuccessResponse,
                                                  successData : @escaping (Data) -> Void,
                                                  failure: @escaping FailureResponse) {
//        Para:user_id,video_id, like_status(1-like,0-unlike)
//        URL - http://karaoke.yilstaging.com/api/post_likes_unlike
         
         guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
             let err = NSError(localizedDescription: "invalid user")
             failure(err)
             return
         }
        var para : JSONDictionary = [:]
        para[ApiKey.user_id] = user_id
        para[ApiKey.video_id] = video_id
        para[ApiKey.like_status] = like.rawValue
         
         self.commonPostAPI(parameters: para, endPoint: .post_likes_unlike, loader: false, success: { (json) in
             success(json)
         }, successData: { (data) in
             successData(data)
         }) { (err) -> (Void) in
             failure(err)
         }
         
     }
    
    // MARK:- follow_User
         //=============
     static func follow_unfollow(friend_id:String,
                                                  follow_type: follow_status,
                                                  loader: Bool = true,
                                                  success: @escaping SuccessResponse,
                                                  successData : @escaping (Data) -> Void,
                                                  failure: @escaping FailureResponse) {
//        Para:user_id,friend_id, follow_status(1-follow,0-unfollow)
//        URL - http://karaoke.yilstaging.com/api/follow_unfollow
         
         guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
             let err = NSError(localizedDescription: "invalid user")
             failure(err)
             return
         }
        var para : JSONDictionary = [:]
        para[ApiKey.user_id] = user_id
        para[ApiKey.friend_id] = friend_id
        para[ApiKey.follow_status] = follow_type.rawValue
         
        self.commonPostAPI(parameters: para, endPoint: .follow_unfollow, loader: loader, success: success, successData: successData, failure: failure)
     }
//       // MARK:- set_event_attending
//            //=============
//    static func set_event_attending(user_id:String, event_id:String, reg_type: attending_status,confirmed:String,
//                                    success: @escaping SuccessResponse,
//                                    successData : @escaping (Data) -> Void,
//                                    failure: @escaping FailureResponse) {
//        //http://caviar.yesitlabs.xyz/api/set_event_attending
//        //user_id:69, event_id:3, reg_type:0, confirmed:1
//
////        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
////            let err = NSError(localizedDescription: "invalid user")
////            failure(err)
////            return
////        }
//        var parameters : [String:Any] = [:]
//        parameters[ApiKey.user_id] = user_id
//        parameters[ApiKey.event_id] = event_id
//        parameters[ApiKey.confirmed] = confirmed
//        parameters[ApiKey.reg_type] = reg_type.rawValue
//        self.commonPostAPI(parameters: parameters, endPoint: .set_event_attending, loader: false, success: { (json) in
//            success(json)
//        }, successData: { (data) in
//            successData(data)
//        }) { (err) -> (Void) in
//            failure(err)
//        }
//    }
////     MARK:- get_user_profile
////    =======================
//    static func get_user_profile(loader: Bool, success: @escaping SuccessResponse,
//                               successData : @escaping (Data) -> Void,
//                               failure: @escaping FailureResponse) {
//        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
//            let err = NSError(localizedDescription: "invalid user")
//            failure(err)
//            return
//        }
////        http://karaoke.yilstaging.com/api/get_user_profile
//
//        let parameters : [String:Any] = [ApiKey.user_id:user_id]
//
//        self.commonPostAPI(parameters: parameters, endPoint: .get_user_profile, loader: loader, success: { (json) in
//            success(json)
//        }, successData: { (data) in
//            successData(data)
//        }) { (err) -> (Void) in
//            failure(err)
//        }
//     }

//     MARK:- get_user_profile
//        =======================
    static func get_user_profile(user_id:String? = nil, loader: Bool,
                                 success: @escaping SuccessResponse,
                                 successData : @escaping (Data) -> Void,
                                 failure: @escaping FailureResponse) {
    //        http://karaoke.yilstaging.com/api/get_user_profile
        var parameters : [String:Any] = [:]
        if user_id == nil {
            guard let user_id1 = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
                let err = NSError(localizedDescription: "invalid user")
                failure(err)
                return
            }
            parameters[ApiKey.user_id] = user_id1
        }else{
            parameters[ApiKey.user_id] = user_id
        }
        self.commonPostAPI(parameters: parameters, endPoint: .get_user_profile, loader: loader, success: { (json) in
            success(json)
        }, successData: { (data) in
            successData(data)
        }) { (err) -> (Void) in
            failure(err)
        }
    }
    
    // MARK:- LogOut
    //=====================
    static func logout_delete(status : logOutFunction, success: @escaping SuccessResponse,
                             failure: @escaping FailureResponse) {
        //http://caviar.yesitlabs.xyz/api/logout_delete
        guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
            let err = NSError(localizedDescription: "invalid user")
            failure(err)
            return
        }
        var params : [String:Any] = [:]
        if status == .Logout {
            params = [ApiKey.user_id:user_id,
                      ApiKey.type:"1"]
        }else{
            params  = [ApiKey.user_id:user_id,
                       ApiKey.type:"2"]
        }
        self.commonPostAPI(parameters: params, endPoint: .logout_delete, loader: true, success: { (json) in
            success(json)
        }, successData: { (data) in
        }, failure: failure)
    }

    
//    // MARK:- OTP API
//    //===============
//    static func OTP(success: @escaping SuccessResponseNew,
//                               failure: @escaping FailureResponse,userID:String,VCode:String) {
//        AppNetworking.GET(endPoint: EndPoint.OtpApi.path+userID + "&VCode=" + VCode, success: { (json) in
//            let response = json["response"].boolValue
//              let msg = json["message"].stringValue
//               switch response {
//               case true: success(json,msg)
//               default: failure(NSError(code: 200, localizedDescription: msg))
//            }
//       }) { (error) in
//           failure(error)
//        }
//     }
    
    
    // MARK:- unblock_user
    //=========================

//    static func unblock_user(user_id:String, block_user_id : String,
//                             success: @escaping SuccessResponse,
//                             successData : @escaping (Data) -> Void,
//                             failure: @escaping FailureResponse) {
//        //http://caviar.yesitlabs.xyz/api/unblock_user
//        //user_id, block_user_id
//        let parameters : [String:Any] = [ApiKey.user_id:user_id,
//                                         ApiKey.block_user_id:block_user_id]
//        self.commonPostAPI(parameters: parameters, endPoint: .unblock_user, success: { (json) in
//            success(json)
//        }, successData : { (data) in
//            successData(data)
//        }) { (error) -> (Void) in
//            failure(error)
//        }
//    }
    // MARK:- block_user
       //=========================

//       static func block_user(user_id:String, block_user_id : String,
//                                success: @escaping SuccessResponse,
//                                successData : @escaping (Data) -> Void,
//                                failure: @escaping FailureResponse) {
////        http://anothercz.yilstaging.com/api/block_user
////        user_id:23, block_user_id:34
//
//           let parameters : [String:Any] = [ApiKey.user_id:user_id,
//                                            ApiKey.block_user_id:block_user_id]
//           self.commonPostAPI(parameters: parameters, endPoint: .block_user, success: { (json) in
//               success(json)
//           }, successData : { (data) in
//               successData(data)
//           }) { (error) -> (Void) in
//               failure(error)
//           }
//       }
//
//    // MARK:- verify_profile_send_otp
//          //=========================
//
//          static func verify_profile_send_otp(success: @escaping SuccessResponse,
//                                   successData : @escaping (Data) -> Void,
//                                   failure: @escaping FailureResponse) {
////              http://caviar.yesitlabs.xyz/api/verify_profile_send_otp
//            guard let user_id = UserDefaults.standard.object(forKey: UserKeys.user_id.rawValue) as? String else{
//                let err = NSError(localizedDescription: "invalid user")
//                failure(err)
//                return
//            }
//            let parameters : [String:Any] = [ApiKey.user_id:user_id]
//              self.commonPostAPI(parameters: parameters, endPoint: .verify_profile_send_otp, success: { (json) in
//                  success(json)
//              }, successData : { (data) in
//                  successData(data)
//              }) { (error) -> (Void) in
//                  failure(error)
//              }
//          }
//
//
//    // MARK:- get_chat_profile
//       //=========================
//
//       static func get_user_chat_profile(user_id:String, friend_id : String,
//                                success: @escaping SuccessResponse,
//                                successData : @escaping (Data) -> Void,
//                                failure: @escaping FailureResponse) {
//           //http://caviar.bonzaistaging.com/chat/get_user_chat_profile
//           //parameter - user_id , friend_id
//           let parameters : [String:Any] = [ApiKey.user_id:user_id,
//                                            ApiKey.friend_id:friend_id]
//        AppNetworking.POST(endPoint: WebServices.EndPoint.get_user_chat_profile.rawValue, parameters: parameters, loader: false, success: { (json) in
//           success(json)
//
//        }, successData: { (data) in
//            successData(data)
//        }) { (error) in
//            failure(error)
//        }
//           self.commonPostAPI(parameters: parameters, endPoint: .get_user_chat_profile, success: { (json) in
//               success(json)
//           }, successData : { (data) in
//               successData(data)
//           }) { (error) -> (Void) in
//               failure(error)
//           }
//       }
    
    
//    // MARK:- Get All GetBannerImage
//    //==============================
//
//    static func GetBannerImage(parameters: JSONDictionary,
//                       success: @escaping SuccessResponseNew,
//                       failure: @escaping FailureResponse) {
//
//        self.commonPostAPI(parameters: parameters, endPoint: .GetBannerImage, success: { (json) in
//
//               let response = json["response"].boolValue
//               let msg = json["message"].stringValue
//                switch response {
//                case true: success(json,msg)
//                default: failure(NSError(code: 200, localizedDescription: msg))
//            }
//
//        }) { (error) -> (Void) in
//            failure(error)
//        }
//    }
    
    
//    // MARK:- Get Videos By Category and Veiw All List
//    //==================================================
//
//    static func GetVideosByCategoryId(parameters: JSONDictionary,
//                       success: @escaping SuccessResponseNew,
//                       failure: @escaping FailureResponse) {
//
//        self.commonPostAPI(parameters: parameters, endPoint:.GetVideosByCategoryId, success: { (json) in
//
//               let response = json["response"].boolValue
//               let msg = json["message"].stringValue
//                switch response {
//                case true: success(json,msg)
//                default: failure(NSError(code: 200, localizedDescription: msg))
//            }
//
//        }) { (error) -> (Void) in
//            failure(error)
//        }
//    }
    
    
//    // MARK:- Get Videos Details By VideoType ID
//    //===========================================
//
//    static func GetVideoDetailsByVideoTypeId(parameters: JSONDictionary,
//                       success: @escaping SuccessResponseNew,
//                       failure: @escaping FailureResponse) {
//
//        self.commonPostAPI(parameters: parameters, endPoint: .GetVideoDetailsByVideoTypeId, success: { (json) in
//
//               let response = json["response"].boolValue
//               let msg = json["message"].stringValue
//            let ModelVideoType = json["ModelVideoType"]
//                switch response {
//                case true: success(ModelVideoType,msg)
//                default: failure(NSError(code: 200, localizedDescription: msg))
//            }
//
//        }) { (error) -> (Void) in
//            failure(error)
//        }
//    }
    
//    // MARK:- Forgot Password
//    //=======================
//    static func resetPassword(parameters: JSONDictionary,
//                              success: @escaping SuccessResponseNew,
//                              failure: @escaping FailureResponse) {
//
//              self.commonPostAPI(parameters: parameters, endPoint: .changepassword, success: { (json) in
//
//               let response = json["response"].boolValue
//               let msg = json["message"].stringValue
//                switch response {
//                case true: success(json,msg)
//                default: failure(NSError(code: 200, localizedDescription: msg))
//            }
//
//        }){ (error) in
//            failure(error)
//        }
//
//    }
//
}

enum logOutFunction {
    case Logout
    case Delete
}

enum like_status : String {
    case like = "1"
    case unLike = "0"
}
enum favourite_status : String {
    case favourite = "1"
    case unfavourite = "0"
}

enum follow_status : String {
    case follow = "1"
    case unFollow = "0"
}
enum attending_status : String {
    case decline = "4"
    case accept = "3"
    case yes = "1"
    case no = "2"
}
