//
//  PSingletonClass.swift
//  Kaymo
//
//  Created by YATIN  KALRA on 04/04/19.
//  Copyright © 2019 Dhakar. All rights reserved.
//

import UIKit
import Reachability
import Alamofire

var interNetConnetion = true

class PSingletonClass: NSObject {
    static var instance : PSingletonClass? = nil
    
   static var instance2 : PSingletonClass? = nil
    
    var fcm_token  : String?

//    var last_name : String? //
    //1
    var Mobile_number : String? //
    var country_code : String? //
    var Email : String?
    //2
    var Lat : String? //
    var long : String? //
    //3
    var gendar : String? //
    var I_interested : String? //
    //4
    
    var Image_list : [Data] = [] //
    var first_name : String? //
    var ageStr : String? //
    var current_realtionship : String? //
    var kind_of_realtionship : String?
    var nationality : String? //
    var myReligion : String?
    var i_live : String?
    var myBody_type : String?
    var myHeight : String?
    var myjobStatus : String?
    var myEduction : String?
    var myHousing : String?
    var i_Drink : String?
    var i_smok : String?
    var mySelf : String?
    
    var photo_format_id : String?
    var user_photo : Data?
    
    var Seeking_age : String?
    var Seeking_relationship_someone : String?
    var Seeking_relationship_lookingfor : String?
    var Seeking_nationality : String?
    var Seeking_religion : String?
    var Seeking_lives : String?
    var Seeking_body : String?
    var Seeking_height : String?
    var Seeking_education : String?
    var Seeking_living_situation : String?
    var Seeking_drinks : String?
    var Seeking_smoke : String?
    
    
    class func createSingletonSignUp() -> PSingletonClass? {
        if instance2 == nil {
            instance2 = PSingletonClass()
        }
        return instance2
    }
    
    class func createSingleton() -> PSingletonClass? {
        if instance == nil {
            instance = PSingletonClass()
        }
        return instance
    }
    
   
//    func createSingleton() -> PSingletonClass? {
//        if self.instance == nil {
//            self.instance = PSingletonClass()
//        }
//        return self.instance
//    }
    
    static  let shared = PSingletonClass()  // 2. Shared instance
    
    // 4. Tracks current NetworkStatus (notReachable, reachableViaWiFi, reachableViaWWAN)
    var reachabilityStatus: Reachability.Connection = .unavailable
    
    // 3. Boolean to track network reachability
    var isNetworkAvailable : Bool {
        return reachabilityStatus != .unavailable
    }
    
    // 5. Reachability instance for Network status monitoring
    let reachability = try! Reachability()
    
    @objc func reachabilityChanged22(note: Notification) {
        let reachability = note.object as! Reachability
        
        switch reachability.connection {
        case .wifi:
            interNetConnetion = true
            print("Reachable via WiFi")
            NotificationCenter.default.post(name: Notification.Name("put"), object: nil)
        case .cellular:
            interNetConnetion = true
            print("Reachable via Cellular")
            NotificationCenter.default.post(name: Notification.Name("put"), object: nil)
        case .none:
            interNetConnetion = false
            print("Network not reachable")
            NotificationCenter.default.post(name: Notification.Name("put"), object: "YES")
        case .unavailable:
            interNetConnetion = false
            NotificationCenter.default.post(name: Notification.Name("put"), object: "YES")
            print("Network not unavailable")
            break
        }
    }
    func startMonitoring() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged22(note:)), name: .reachabilityChanged, object: reachability)
        do{
            try reachability.startNotifier()
        }catch{
            print("could not start reachability notifier")
        }
        
    }
    func stopMonitoring(){
        reachability.stopNotifier()
        NotificationCenter.default.removeObserver(self,
                                                  name: .reachabilityChanged,
                                                  object: reachability)
    }
    
    
    func createNameCustom() -> String {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "ddMMyyyyHHmmSS"
        return formatter.string(from: date)
    }
    
    class func profileUpdateAPIHit(completion: @escaping (_ dict:NSDictionary) -> Void) {
        let userID = UserDefaults.standard.object(forKey: "userID") as! String
        let para = ["uid":userID]
//        ServiceClassMethods.AlamoRequest(method: .post, serviceString: AppConstants.LeftMenu, parameters: para, heade: nil) { (dict) in
//            print(dict)
//        }
    }
    class func addFavourite(ViewCon:UIViewController, view:UIView, product_id:String,product_name:String,product_image:UIImage?,product_price:String,product_brand:String,product_imageStr:String,animation:Bool,completion: @escaping (_ dict:NSDictionary) -> Void) {
        
       
    }
    class func removeFavourite(ViewCon:UIViewController, view:UIView, product_id:String,animation:Bool,completion: @escaping (_ dict:NSDictionary) -> Void) {
       
    }
    class func removeWishList(ViewCon:UIViewController, view:UIView, product_id:String,animation:Bool,completion: @escaping (_ dict:NSDictionary) -> Void) {
        
    }
    
    
}
extension UIImage {
    func scaleImageToSize(newSize: CGSize) -> UIImage {
        var scaledImageRect = CGRect.zero
        
        let aspectWidth = newSize.width/size.width
        let aspectheight = newSize.height/size.height
        
        let aspectRatio = max(aspectWidth, aspectheight)
        
        scaledImageRect.size.width = size.width * aspectRatio;
        scaledImageRect.size.height = size.height * aspectRatio;
        scaledImageRect.origin.x = (newSize.width - scaledImageRect.size.width) / 2.0;
        scaledImageRect.origin.y = (newSize.height - scaledImageRect.size.height) / 2.0;
        UIGraphicsBeginImageContext(newSize)
        draw(in: scaledImageRect)
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage!
    }
}

extension UIView {
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.gray.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 1, height: 5)
        layer.shadowRadius = 18
        
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float, offSet: CGSize, radius: CGFloat, scale: Bool) {
        layer.masksToBounds = false
        layer.shadowColor = color.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offSet
        layer.shadowRadius = radius
        
        layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
    func roundredious(cusView:UIView) {
        cusView.layer.cornerRadius = cusView.frame.size.height/2
        cusView.clipsToBounds = true
    }
}

