//
//  AppNetworking.swift
//  NewProject
//
//  Created by Preeti dhankar on 30/08/18.
//  Copyright © 2018 Preeti dhankar. All rights reserved.
//

import Foundation
import SwiftyJSON
import Alamofire
import Photos

typealias JSONDictionary = [String : Any]
typealias JSONDictionaryArray = [JSONDictionary]
typealias SuccessResponse = (_ json : JSON) -> ()
typealias SuccessResponseNew = (_ json : JSON, _ msg : String) -> ()
typealias FailureResponse = (NSError) -> (Void)
typealias ResponseMessage = (_ message : String) -> ()
//typealias UserControllerSuccess = (_ user : LoginModel, _ msg : String) -> ()
typealias DownloadData = (_ data : Data) -> ()
typealias UploadFileParameter = (fileName: String, key: String, data: Data, mimeType: String)
extension Notification.Name {
    static let NotConnectedToInternet = Notification.Name("NotConnectedToInternet")
}

enum AppNetworking {

    static func POST(endPoint : String,
                     parameters : JSONDictionary = [:],
                     headers : HTTPHeaders = [:],
                     loader : Bool = true,
                     success : @escaping (JSON) -> Void,
                     successData : @escaping (Data) -> Void,
                     failure : @escaping (NSError) -> Void) {
        
        request(URLString: endPoint, httpMethod: .post, parameters: parameters, headers: headers, loader: loader, success: success,successData: successData, failure: failure)
    }
    
    static func POSTWithFiles(endPoint : String,
                              parameters : [String : Any] = [:],
                              files : [UploadFileParameter] = [],
                              headers : HTTPHeaders = [:],
                              loader : Bool = true,
                              success : @escaping (JSON) -> Void,
                              successData : @escaping (Data) -> Void,
                              progress : @escaping (Double) -> Void,
                              failure : @escaping (NSError) -> Void) {
        
        upload(URLString: endPoint, httpMethod: .post, parameters: parameters, files: files, headers: headers, loader: loader, success: success, successData: successData, progress: progress, failure: failure)
    }
    
    static func GET(endPoint : String,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    successData : @escaping (Data) -> Void,
                    failure : @escaping (NSError) -> Void) {
        
        makeRequestGet(URLString: endPoint, httpMethod: .get,  encoding: URLEncoding.queryString, headers: headers, loader: loader, success: success, successData: successData,  failure: failure)
    }
    
    static func PUT(endPoint : String,
                    parameters : JSONDictionary = [:],
                    headers : HTTPHeaders = [:],
                    loader : Bool = true,
                    success : @escaping (JSON) -> Void,
                    successData : @escaping (Data) -> Void,
                    failure : @escaping (NSError) -> Void) {
        
        request(URLString: endPoint, httpMethod: .put, parameters: parameters, headers: headers, loader: loader, success: success, successData: successData,  failure: failure)
    }
    
    static func DELETE(endPoint : String,
                       parameters : JSONDictionary = [:],
                       headers : HTTPHeaders = [:],
                       loader : Bool = true,
                       success : @escaping (JSON) -> Void,
                       successData : @escaping (Data) -> Void,
                       failure : @escaping (NSError) -> Void) {
        
        request(URLString: endPoint, httpMethod: .delete, parameters: parameters, headers: headers, loader: loader, success: success, successData: successData,  failure: failure)
    }
    
    static func DOWNLOAD(endPoint : String,
                         fileName : String,
                         parameters : JSONDictionary = [:],
                         headers : HTTPHeaders = [:],
                         mediaType : String,
                         loader : Bool = true,
                         success : @escaping (Bool) -> Void,
                         successData : @escaping (URL?) -> Void,
                         failure : @escaping (NSError) -> Void) {
        
        download(URLString: endPoint,fileName: fileName, httpMethod: .get, parameters: parameters, headers: headers, mediaType: mediaType, loader: loader, success: success, successData: successData,  failure: failure)
    }
    
    private static func download(URLString : String,
                                 fileName : String,
                                 httpMethod : HTTPMethod,
                                 parameters : JSONDictionary = [:],
                                 encoding: URLEncoding = URLEncoding.default,
                                 headers : HTTPHeaders = [:],
                                 mediaType : String,
                                 loader : Bool = true,
                                 success : @escaping (Bool) -> Void,
                                 successData : @escaping (URL?) -> Void,
                                 failure : @escaping (NSError) -> Void) {
        
        
        var fileURL = URL(string: "")
        var nameFile = fileName
        if fileName == "" {
            nameFile = Date.getCurrentDateForName()
        }
        let destination: DownloadRequest.Destination = { _, _  in
            
            let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            
            fileURL = documentsURL.appendingPathComponent( nameFile + mediaType.replace(string: "/", withString: "."))
            return (fileURL!, [.removePreviousFile, .createIntermediateDirectories])
            
        }
        
        if loader { CommonFunctions.showActivityLoader() }

        AF.download(URLString, method: httpMethod, parameters: parameters, encoding: encoding, headers: headers, to: destination).response { (response) in
            
            if loader { CommonFunctions.hideActivityLoader() }

            if response.error != nil {
                print("===================== FAILURE =======================")
                let e = response.error!
                print(e.localizedDescription)
                
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
                failure(e as NSError)
                
            } else {
                print("===================== RESPONSE =======================")
                guard response.error == nil else { return }
                
                switch mediaType {
                    
                case "video/mp4":  CustomPhotoAlbum.shared.saveVideo(videoFileUrl: fileURL!)
                    
                case "application/pdf":
                    break
                    //insantiate webViewVC
                    //webView.loadRequest(URLRequest(url: fileURL!))
                case "image/png":
                    CustomPhotoAlbum.shared.saveImage(imageFileUrl: fileURL!)
                    break
                default:
                    
                    break
                }
                print(fileURL as Any)
                successData(fileURL)
                success(true)
            }
        }
    }
    
    private static func request(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: JSONEncoding = JSONEncoding.default,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                successData : @escaping (Data) -> Void,
                                failure : @escaping (NSError) -> Void) {
        
        if loader { CommonFunctions.showActivityLoader() }
        
        
        makeRequest(URLString: URLString, httpMethod: httpMethod, parameters: parameters, encoding: encoding, headers: headers, loader: loader, success: { (json) in
            let code = json[ApiKey.code].intValue
            if code == ApiCode.tokenExpired {
                request(URLString: URLString, httpMethod: httpMethod, parameters: parameters, headers: headers, loader: loader, success: success, successData: successData,  failure: failure)
            }else{
                if loader { CommonFunctions.hideActivityLoader() }
                success(json)
            }
        }, successData: { (data) in
            successData(data)
        }) { (error) in
            failure(error)
        }
    }
    
    private static func makeRequest(URLString : String,
                                httpMethod : HTTPMethod,
                                parameters : JSONDictionary = [:],
                                encoding: JSONEncoding = JSONEncoding.default,
                                headers : HTTPHeaders = [:],
                                loader : Bool = true,
                                success : @escaping (JSON) -> Void,
                                successData : @escaping (Data) -> Void,
                                failure : @escaping (NSError) -> Void) {
        let updatedHeaders : HTTPHeaders = headers
        
//        if !UserModel.main.token.isEmpty {
//            print(UserModel.main.token)
//          //  updatedHeaders[ApiKey.Authorization] =  "Bearer \(UserModel.main.token)"
//            updatedHeaders[ApiKey.Authorization]  = "2dcf1f2823c4528d5dd3a5d9d234cb32"
//        }
//         updatedHeaders["Content-Type"] = "application/json"
         //updatedHeaders[ApiKey.Authorization]  = "Bearer d7b603b419fb166590793b65f3113d6d"
        
        //d7b603b419fb166590793b65f3113d6d
        //2dcf1f2823c4528d5dd3a5d9d234cb32
        
        AF.request(URLString, method: httpMethod, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseJSON { (response) in
            
            if loader { CommonFunctions.hideActivityLoader() }
            
            print("===================== METHOD =========================")
            print(httpMethod)
            print("===================== ENCODING =======================")
            print(encoding)
            print("===================== URL STRING =====================")
            print(URLString)
            print("===================== HEADERS ========================")
            print(updatedHeaders)
            print("===================== PARAMETERS =====================")
            print(parameters.description)
            
            switch(response.result) {
            case .success(let value):
                print("===================== RESPONSE =======================")
//                print(JSON(value))
                successData(response.data!)
                let json = JSON(value)
                success(json)
                
            case .failure(let e):
                print("===================== FAILURE =======================")
                print(e.localizedDescription)
                if loader { CommonFunctions.hideActivityLoader() }
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    
                NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
              CommonFunctions.showToastWithMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                    failure(e as NSError)
                    
                } else {
                    if e.localizedDescription == "URLSessionTask failed with error: The Internet connection appears to be offline." {
                        NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                      CommonFunctions.showToastWithMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                    }
                    failure(e as NSError)
                }
            }
        }
    }
    
    private static func makeRequestGet(URLString : String,
                                    httpMethod : HTTPMethod,
                                    encoding: URLEncoding = URLEncoding.httpBody,
                                    headers : HTTPHeaders = [:],
                                    loader : Bool = true,
                                    success : @escaping (JSON) -> Void,
                                    successData : @escaping (Data) -> Void,
                                    failure : @escaping (NSError) -> Void) {
            var updatedHeaders : HTTPHeaders = headers
             updatedHeaders["Content-Type"] = "application/json"
        AF.request(URLString, method: httpMethod,encoding: encoding, headers: updatedHeaders).responseJSON { (response) in
                
                if loader { CommonFunctions.hideActivityLoader() }
                
                print("===================== METHOD =========================")
                print(httpMethod)
                print("===================== ENCODING =======================")
                print(encoding)
                print("===================== URL STRING =====================")
                print(URLString)
                print("===================== HEADERS ========================")
                print(updatedHeaders)
                print("===================== PARAMETERS =====================")
               // print(parameters.description)
                
                switch(response.result) {
                case .success(let value):
                    print("===================== RESPONSE =======================")
                    print(JSON(value))
                    successData(response.data!)
                    let json = JSON(value)
                    success(json)
                    
                case .failure(let e):
                    print("===================== FAILURE =======================")
                    print(e.localizedDescription)
                    if loader { CommonFunctions.hideActivityLoader() }
                    if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                        
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                  CommonFunctions.showToastWithMessage(LocalizedString.pleaseCheckInternetConnection.localized)
                        failure(e as NSError)
                        
                    } else {
                        failure(e as NSError)
                    }
                }
            }
        }

    
    private static func upload(URLString : String,
                               httpMethod : HTTPMethod,
                               parameters : JSONDictionary = [:],
                               files : [UploadFileParameter] = [],
                               headers : HTTPHeaders = [:],
                               loader : Bool = true,
                               success : @escaping (JSON) -> Void,
                               successData : @escaping (Data) -> Void,
                               progress : @escaping (Double) -> Void,
                               failure : @escaping (NSError) -> Void) {
        
//        let d = URLConvertible(
        if loader { CommonFunctions.showActivityLoader() }

        AF.upload(multipartFormData: { (multipartFormData) in
            
            files.forEach({ (fileParamObject) in
                
                multipartFormData.append(fileParamObject.data, withName: fileParamObject.key, fileName: fileParamObject.fileName, mimeType: fileParamObject.mimeType)
            })
            
            parameters.forEach({ (paramObject) in
                
                if let data = (paramObject.value as AnyObject).data(using : String.Encoding.utf8.rawValue) {
                    multipartFormData.append(data, withName: paramObject.key)
                }
            })
        }, to: URLString,
           usingThreshold: UInt64.init(),
           method: .post,
           headers: headers,
           interceptor: nil,
           requestModifier: nil).response { (encodingResult) in
//        }, with: url, encodingCompletion: { encodingResult in
            
            do {
                print("===================== METHOD =========================")
                print(httpMethod)
                print("===================== URL STRING =====================")
                print(URLString)
                print("===================== HEADERS ========================")
                print(headers)
                print("===================== PARAMETERS =====================")
                print(parameters)
                print("===================== Image_PARAMETERS =====================")
                print(files)
                print("===================== RESPONSE =======================")
                
                if let err = encodingResult.error{
                    if loader { CommonFunctions.hideActivityLoader() }

                    if (err as NSError).code == NSURLErrorNotConnectedToInternet {
                        NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                    }
                    failure(err as NSError)
                    if let f = encodingResult.data {
                    print("Print Server Error: " + String(data: f, encoding: String.Encoding.utf8)!)
                    }
                    
                    return
                }
                
                if loader { CommonFunctions.hideActivityLoader() }
                guard encodingResult.data != nil else{
                    print("===================== FAILURE data nil =======================")
                    failure(NSError(code: 0, localizedDescription: ""))
                    return
                }
                
                successData(encodingResult.data!)
                let value = try JSON(data: encodingResult.data!)
                print(JSON(value))
                success(value)
            }catch {
                print("===================== FAILURE =======================")
                print(error.localizedDescription)
                
                if (error as NSError).code == NSURLErrorNotConnectedToInternet {
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
                failure(error as NSError)
            }
           }.uploadProgress { (progressData) in
            progress(progressData.fractionCompleted)
           }
    }
    
}
